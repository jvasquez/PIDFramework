#ifndef PID_LepHandler
#define PID_LepHandler

#include <iostream>
#include "TString.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#ifndef __CINT__
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "PIDFramework/PIDUtils.h"
#endif

class LepHandler {

  protected:
    TString m_name;
    xAOD::TEvent *m_event;
    xAOD::TStore *m_store;
    unsigned int m_nElContainer;
    unsigned int m_nMuContainer;

  public:
    void Initialize( xAOD::TEvent* event ) { m_event = event; }

  #ifndef __CINT__
    LepHandler( xAOD::TEvent *event, xAOD::TStore *store );
    ~LepHandler();

    bool comparePt( xAOD::Electron *a, xAOD::Electron *b ); 
    bool comparePt( xAOD::Muon *a,     xAOD::Muon *b ); 
    
    bool passSelection( xAOD::Electron*, double = 10 );
    bool passSelection( xAOD::Muon*, double = 10 );

    xAOD::ElectronContainer getElectronContainer();
    xAOD::ElectronContainer getSelectedElectrons( double = 10 );
    xAOD::ElectronContainer getSelectedElectrons( xAOD::ElectronContainer, double );
    
    xAOD::MuonContainer getMuonContainer();
    xAOD::MuonContainer getSelectedMuons( double = 10 );
    xAOD::MuonContainer getSelectedMuons( xAOD::MuonContainer, double );
    
    const xAOD::TrackParticle * getGSFTrack( xAOD::Electron* );
    const xAOD::TrackParticle * getTrack( xAOD::Electron* );
    const xAOD::TrackParticle * getTrack( xAOD::Muon* );

    int mapEtaToPartition( const double );
    int mapPhiToPhisector( const double );
  #endif

};
#endif // PID_LepHandler
