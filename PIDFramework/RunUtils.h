#ifndef PIDFramework_JobHelper_H
#define PIDFramework_JobHelper_H

#include "PIDFramework/PIDAnalysis.h"

#include <EventLoop/StatusCode.h>
#include "EventLoop/OutputStream.h"
#include <EventLoopGrid/GridDriver.h>
#include <EventLoopGrid/PrunDriver.h>

#include "EventLoop/DirectDriver.h"
#include "EventLoop/Job.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "xAODRootAccess/Init.h"
#include <TSystem.h>
#include <iostream>
#include <string>


//! \brief PID namespace
namespace PID {

  //! \name   A few general helper methods and definitions
  //@{

  void runJob(PIDAnalysis *alg, int argc, char** argv);
  
  //@}
}

#endif
