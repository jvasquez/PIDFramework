#ifndef PIDUtils_H
#define PIDUtils_H

#include <iostream>
#include "TString.h"
#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"

namespace PID {
  
  //! \brief typedef for a vector of doubles (to save some typing)
  typedef std::vector<double>  NumV;
  //! \brief typedef for a vector of ints (to save some typing)
  typedef std::vector<int>     IntV;
  //! \brief typedef for a vector of strings (to save some typing)
  typedef std::vector<TString> StrV;

  //! \brief Converts a text line to a vector of words
  //  \param str input string with words
  //  \param sep separator to define where a word ends or starts
  StrV vectorize(TString str, TString sep=" ");
  
  //! \brief Converts string of separated numbers to vector<double>
  //  \param str input string with numbers
  //  \param sep separator to define where a number ends or starts
  NumV vectorizeNum(TString str, TString sep=" ");
  
  //! \brief method to abort program with error message
  void fatal(TString msg);
  
  //! \brief returns true if a given file or directory exist
  bool fileExist(TString fn);

  static const double GeV(0.001);
  static const double Pi(3.14159265359);
  static const double PiHalf(1.57079632679);

}

 #endif
