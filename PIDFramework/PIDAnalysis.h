#ifndef PIDFramework_PIDAnalysis_H
#define PIDFramework_PIDAnalysis_H

#include "PIDFramework/PIDIncludes.h"
#include "PIDFramework/Config.h"

//CINT is weird and handlers need to be put here.
//#ifndef __CINT__

#include "PIDFramework/HistogramStore.h"
#include "PIDFramework/Likelihood.h"
#include "PIDFramework/LepHandler.h"

//#endif

class PIDAnalysis : public EL::Algorithm
{

  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
private:
  // float cutValue;
  std::set<long long int> runEvtList;
  PID::Config m_config; 

  // the xAOD event reader
  xAOD::TEvent *m_event;  //!
  xAOD::TStore *m_store; //!
  HistogramStore *m_histoStore; //!

#ifndef __CINT__
  Likelihood *m_likelihood; //!
  LepHandler *m_lepHandler; //!
  xAOD::MuonContainer m_muons; //!
#endif

#ifndef __CINT__
  CP::IPileupReweightingTool *m_PRWTool; //!
  GoodRunsListSelectionTool *m_grl; //!
#endif

  // event counts
  int m_eventCounter; //!
  long m_startTime; //!

  bool m_pass_grl;                  //!
  
  int etaBin5; //!
  
  // Occupanicy
  double Occ_Glob;
  double Occ_B_C, Occ_B_A;
  double Occ_ECA_C, Occ_ECA_A;
  double Occ_ECB_C, Occ_ECB_A;
 
  // OccLabels 
  std::vector<TString> OccTypes; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  // this is a standard constructor
  PIDAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  //virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  inline virtual void setConfig(const PID::Config &config);
  inline virtual bool passGRL();

  void SetupGRL();
  void SetupPRWTool();

  // this is needed to distribute the algorithm to the workers
  ClassDef(PIDAnalysis, 1);
  
  //helper functions
  void FillEventInfo(); //!
  void FillMCInfo();    //!
  void FillOccupancyInfo();    //!
  //double getTrackOccupancy( const xAOD::TrackParticle* ); //!
  
  //event info
  bool m_isMC, m_isData;  //!
  bool m_verbose;  //!
  unsigned int m_runNumber;         //! run number
  unsigned long long m_eventNumber; //! event number
  unsigned int m_lbn;               //! lumi block number
  double m_averageIntPerXing;        //! mu
  double m_mu;                       //! mu
  
  //mc info
  unsigned int m_mc_channel_number; //! mc channel number
  double m_mc_gen_weight;            //! generator weight
  double m_mc_pu_weight;             //! pile-up weight
  double m_prwSF;                    //! pile-up data SF
  double w;

  // - number of good objects
  unsigned int m_nPhotons;               //!
  unsigned int m_nJets;                  //!
  unsigned int m_nMuons;                 //!

  bool m_selectionZee;   //!
  bool m_selectionZmm;   //!
  bool m_selectionJPee;  //!
  bool m_selectionJPmm;  //!
  bool isEl;            //!
  bool isMu;            //!
  bool isZB;            //!
  bool isJP;            //!

protected:
  // access to useful pointers
  inline virtual xAOD::TEvent*      event() { return m_event; }
  inline virtual xAOD::TStore*      store() { return m_store; }
  inline virtual const xAOD::EventInfo*  eventInfo(); 
  
  inline virtual HistogramStore*  histoStore() { return m_histoStore; }
  inline virtual PID::Config*         config() { return &m_config; }

  inline virtual Likelihood*  LH() { return m_likelihood; }
  inline virtual LepHandler* lepHandler() { return m_lepHandler; }

  inline bool isMC()   { return m_isMC;   }
  inline bool isData() { return m_isData; }
  
  inline double weight() { return m_mc_gen_weight * m_mc_pu_weight; }
  inline double averageMu();

};

inline 
void PIDAnalysis::setConfig(const PID::Config &config)
{
  m_config = config;
}

inline 
bool PIDAnalysis::passGRL()
{
  if (isData())
    return m_grl->passRunLB(*eventInfo());
  return true;
}

inline
double PIDAnalysis::averageMu()
{
  double mu = eventInfo()->averageInteractionsPerCrossing();
  if (not m_isMC)
    mu = m_PRWTool->getCorrectedMu(*eventInfo())*m_prwSF;
  return mu;
}

inline
const xAOD::EventInfo* PIDAnalysis::eventInfo()
{
  const xAOD::EventInfo *eventinfo = 0;
  if (m_event->retrieve(eventinfo, "EventInfo").isFailure()) {
    PID::fatal("Cannot access EventInfo");
  }
  return eventinfo;
}

#endif
