#ifndef PIDANALYSISEDM_H
#define PIDANALYSISEDM_H

// STL includes
#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <cmath>

// ROOT includes
#include <TSystem.h>
#include <TTreeFormula.h>
#include <TObjString.h>
#include <TObjArray.h>
#include <TMath.h>
#include <TH1.h>
#include <TH1F.h>
#include <TString.h>

// ROOT includes
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>
#include "PathResolver/PathResolver.h"

// xAOD includes
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"

// GRL & PRW
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "AsgTools/ToolHandle.h"



#define CHECK( CONTEXT, EXP )                                    \
  {                                                              \
    if (!EXP.isSuccess())          \
      Fatal( CONTEXT, "Failed to execute: %s. Exiting.", #EXP);  \
  }

#define CP_SYST_CHECK( CONTEXT, EXP )                                                          \
  {                                                                                                    \
  CP::SystematicCode correctionCode = EXP;                                                         \
  if (correctionCode==CP::SystematicCode::Ok)                                                          \
    ;                                                                                                  \
  else                                                                                                 \
    Fatal( CONTEXT, Form("Error code %d when calling %s",(int) correctionCode, #EXP));                 \
  }

#define CP_TOOL_CHECK( CONTEXT, EXP )                                                          \
  {                                                                                                    \
  CP::CorrectionCode correctionCode = EXP;                                                         \
  if (correctionCode==CP::CorrectionCode::Ok)                                                          \
    ;                                                                                                  \
  else if (correctionCode==CP::CorrectionCode::Error)                                          \
    Fatal( CONTEXT, Form("Error when calling %s", #EXP) );                                   \
  else if (correctionCode==CP::CorrectionCode::OutOfValidityRange)                                 \
    Warning( CONTEXT, Form("Out-of-range return code when calling %s", #EXP) );                        \
  else                                                                                                 \
    Warning( CONTEXT, Form("Unknown correction code %d when calling %s",(int) correctionCode, #EXP));  \
  }

#define MySafeDelete(x) { if (x) { delete x; x=0; } }

#endif //PIDANALYSISEDM_H
