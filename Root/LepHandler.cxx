#include <iostream>
#include "TString.h"
#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODCore/ShallowCopy.h"
#include "PIDFramework/PIDUtils.h"
#include "PIDFramework/LepHandler.h"


bool LepHandler::comparePt( xAOD::Electron *a, xAOD::Electron *b ) { return ( a->pt() > b->pt() ); }
//---------------------------------------------------------- 
bool LepHandler::comparePt( xAOD::Muon *a, xAOD::Muon *b ) { return ( a->pt() > b->pt() ); }
//----------------------------------------------------------


LepHandler::LepHandler( xAOD::TEvent *event, xAOD::TStore *store)
: m_event(event)
, m_store(store)
, m_nElContainer(0)
, m_nMuContainer(0)
{ }

//--------------------
// Electrons 
//--------------------

xAOD::ElectronContainer LepHandler::getElectronContainer() {
//---------------------------------------------------------- 
  //Get the raw Electron Container
  const xAOD::ElectronContainer* electrons = 0; 
  if ( m_event->retrieve( electrons, "Electrons" ).isFailure() ) { 
    PID::fatal("Failed to retrieve Electrons container."); 
  }

  //create a shallow copy and convert it to an ElectronContainer
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > shallowCopy = xAOD::shallowCopyContainer( *electrons );
  xAOD::ElectronContainer container(shallowCopy.first->begin(), 
                                    shallowCopy.first->end(),
                                    SG::VIEW_ELEMENTS);

  //add shallow container to TStore for clean up 
  TString name = TString::Format("ShallowElectrons_%d", m_nElContainer);
  if (!m_store->record(shallowCopy.first,  name.Data())) {    PID::fatal("Cannot store Electron container in TStore"); }
  name = TString::Format("ShallowElectronsAux_%d", m_nElContainer);
  if (!m_store->record(shallowCopy.second, name.Data())) { PID::fatal("Cannot store Electron aux container in TStore"); }
  m_nElContainer++;

  //container.sort(comparePt);

  return container;
}

bool LepHandler::passSelection( xAOD::Electron *el, double minPtGeV ) { 
//----------------------------------------------------------
  if ( el->pt() < minPtGeV*1.e3  ||  el->pt() > 1000.e3 ) return false;
  if ( fabs(el->eta()) > 2.7 ) return false;

  //Electron ID
  unsigned int isEMLoose = el->auxdata< unsigned int >("isEMLoose");
  if ( isEMLoose != 0 ) return false; //probably not optimal
  
  //Isolation
  //double ptcone20el = el->auxdata< float >("ptcone20");
  //if ( ptcone20el / el->pt() > 0.10 ) return false;

  return true;
}

xAOD::ElectronContainer LepHandler::getSelectedElectrons( xAOD::ElectronContainer rawContainer, double minPtGeV ) {
//----------------------------------------------------------
  xAOD::ElectronContainer selectedElectrons(SG::VIEW_ELEMENTS);
  for( xAOD::Electron *el : rawContainer ) {
    if( !passSelection(el, minPtGeV) ) continue;
    selectedElectrons.push_back(el);
  }
  return selectedElectrons;
}

xAOD::ElectronContainer LepHandler::getSelectedElectrons( double minPtGeV ) {
//----------------------------------------------------------
  xAOD::ElectronContainer rawContainer = getElectronContainer();
  return getSelectedElectrons( rawContainer, minPtGeV );
}



//--------------------
// Muons
//--------------------  

xAOD::MuonContainer LepHandler::getMuonContainer() {
//----------------------------------------------------------
  //Get the raw Muon Container
  const xAOD::MuonContainer* muons = 0;
  if ( !m_event->retrieve( muons, "Muons" ).isSuccess() ){ // retrieve arguments: container type, container key
    PID::fatal("Failed to retrieve Muons container. Exiting.");
  }

  //create a shallow copy and convert it to a MuonContainer
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > shallowCopy = xAOD::shallowCopyContainer( *muons );
  xAOD::MuonContainer container(shallowCopy.first->begin(), 
                                    shallowCopy.first->end(),
                                    SG::VIEW_ELEMENTS);

  //add shallow container to TStore for clean up 
  TString name = TString::Format("ShallowMuons_%d", m_nElContainer);
  if (!m_store->record(shallowCopy.first,  name.Data())) {    PID::fatal("Cannot store Muons container in TStore"); }
  name = TString::Format("ShallowMuonsAux_%d", m_nElContainer);
  if (!m_store->record(shallowCopy.second, name.Data())) { PID::fatal("Cannot store Muons aux container in TStore"); }
  m_nElContainer++;
  
  return container;
}

bool LepHandler::passSelection( xAOD::Muon *mu, double minPtGeV ) {
//----------------------------------------------------------
  if ( mu->pt() < minPtGeV*1.e3  ||  mu->pt() > 1000.e3 ) return false;
  if ( fabs(mu->eta()) > 2.7 ) return false;
  
  //Muon ID?? 

  //Isolation
  //double ptcone20mu = mu->auxdata< float >("ptcone20");
  //if ( ptcone20mu / mu->pt() > 0.10 ) return false;

  return true;
}

xAOD::MuonContainer LepHandler::getSelectedMuons( xAOD::MuonContainer rawContainer, double minPtGeV ) {
//----------------------------------------------------------
  xAOD::MuonContainer selectedMuons(SG::VIEW_ELEMENTS);
  for ( xAOD::Muon *mu : rawContainer ) {
    if ( !passSelection(mu, minPtGeV) ) continue; 
    selectedMuons.push_back(mu);
  }

  return selectedMuons;
}

xAOD::MuonContainer LepHandler::getSelectedMuons( double minPtGeV ) {
//----------------------------------------------------------
  xAOD::MuonContainer rawContainer = getMuonContainer();
  return getSelectedMuons( rawContainer, minPtGeV );
}



//--------------------
// Tracks
//-------------------- 
const xAOD::TrackParticle * LepHandler::getGSFTrack( xAOD::Electron *el ) {
//----------------------------------------------------------
  //return the GSF track and decorate it with the lepton pt
  const xAOD::TrackParticle *gsfTrack = el->trackParticle();
  float pTrk = gsfTrack->charge() / gsfTrack->auxdata<float>("QoverPLM");
  gsfTrack->auxdecor< double >( "pTrk" ) = pTrk; 
  gsfTrack->auxdecor< double >( "trkpt" ) = pTrk / cosh( gsfTrack->eta() );
  //gsfTrack->auxdecor< double >( "pTrk" ) = gsfTrack->pt() * cosh( gsfTrack->eta() );
  //gsfTrack->auxdecor< double >( "trkpt" ) = gsfTrack->pt();
  gsfTrack->auxdecor< double >( "ptlep" ) = el->pt();
  gsfTrack->auxdecor< int >( "partition" ) = mapEtaToPartition( gsfTrack->eta() );
  gsfTrack->auxdecor< int >( "phisector" ) = mapPhiToPhisector( gsfTrack->phi() );
  return gsfTrack;
}

const xAOD::TrackParticle * LepHandler::getTrack( xAOD::Electron *el ) {
//----------------------------------------------------------
  //return the InDet track and decorate it with the lepton pt
  const xAOD::TrackParticle *gsfTrack = getGSFTrack(el);
  if (gsfTrack == nullptr) return 0;

  const xAOD::TrackParticle *track = xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(gsfTrack);
  if (track == nullptr) {
    std::cerr << "   ERROR :: InDetTrack is null pointer!!!" << std::endl;
    return 0;
  }
  float pTrk = gsfTrack->charge() / gsfTrack->auxdata<float>("QoverPLM");
  track->auxdecor< double >( "pTrk" ) = pTrk; 
  track->auxdecor< double >( "trkpt" ) = pTrk / cosh( gsfTrack->eta() );
  //track->auxdecor< double >( "pTrk" ) = gsfTrack->pt() * cosh( gsfTrack->eta() );
  //track->auxdecor< double >( "trkpt" ) = gsfTrack->pt();
  track->auxdecor< double >( "ptlep" ) = el->pt();
  track->auxdecor< int >( "partition" ) = mapEtaToPartition( track->eta() );
  track->auxdecor< int >( "phisector" ) = mapPhiToPhisector( track->phi() );
  return track;
}

const xAOD::TrackParticle * LepHandler::getTrack( xAOD::Muon *mu ) {
//----------------------------------------------------------
  //return the InDet track and decorate it with the lepton pt
  if (!mu->inDetTrackParticleLink().isValid()) return 0;
  const xAOD::TrackParticle *track = *mu->inDetTrackParticleLink();
  track->auxdecor< double >( "pTrk" ) = track->pt() * cosh( track->eta() );
  track->auxdecor< double >( "trkpt" ) = track->pt();
  track->auxdecor< double >( "ptlep" ) = mu->pt();
  track->auxdecor< int >( "partition" ) = mapEtaToPartition( track->eta() );
  track->auxdecor< int >( "phisector" ) = mapPhiToPhisector( track->phi() );
  return track;
}


//--------------------
// Local Occupancy
//--------------------
int LepHandler::mapEtaToPartition(const double t_eta) {
//----------------------------------------------------------
  int partition(-999);
  double abseta = fabs(t_eta);
  
  // do eta selection
  if ( abseta <= 0.90 ) partition = 0;
  else if ( abseta > 0.90 && abseta <= 1.55 ) partition = 1;
  else if ( abseta > 1.55 ) partition = 2;
  //else if ( abseta > 1.55 && abseta <= 2.00 ) partition = 2;
  //else PID::fatal("Could not define partition");
  
  if (t_eta>0.) partition += 3; // side A
  return partition;
}

int LepHandler::mapPhiToPhisector(const double t_phi) {
//----------------------------------------------------------
  int phisector(-999);

  // shift all phi to positive numbers
  double phi2pi = (t_phi > 0) ? t_phi : t_phi + 2*PID::Pi;

  //find bin
  double dphi = 0; // TBD
  phisector = int ( (phi2pi + dphi)*32./(2*PID::Pi) );
  phisector %= 32; //modulo 32

  return phisector;
}
