#include <PIDFramework/PIDAnalysis.h>
#include "PIDFramework/PIDIncludes.h"

#include "PIDFramework/HistogramStore.h"
#include "PIDFramework/Likelihood.h"
#include "PIDFramework/LepHandler.h"
#include "PIDFramework/PIDUtils.h"

using PID::GeV;
using std::cout;
using std::endl;

// this is needed to distribute the algorithm to the workers
ClassImp(PIDAnalysis)


void PIDAnalysis::FillEventInfo ()
//------------------------------------------------------------------------------
{
  if (m_verbose) cout << endl << "Enter FillEventInfo () " << endl;

  m_runNumber = eventInfo()->runNumber();
  m_lbn = eventInfo()->lumiBlock();
  m_eventNumber = eventInfo()->eventNumber();
  m_averageIntPerXing = averageMu(); // use helper function, it includes data corrections 
  if (m_verbose) {
    cout << "  Run Number = " << m_runNumber << endl;
    cout << "  Lumi. Block Number = " << m_lbn << endl;
    cout << "  Event Number = " << m_eventNumber << endl;
    cout << "  Avg. Interactions per crossing = " << m_averageIntPerXing << endl;
  }
  
  //Fill Histograms of event info
  histoStore()->fillTH1F("h_averageMu", m_averageIntPerXing, w );
}


void PIDAnalysis :: FillMCInfo ()
//------------------------------------------------------------------------------
{
  if (m_verbose) cout << "Enter FillMCInfo () " << endl;
  //m_mc_channel_number = eventInfo()->mcChannelNumber();
  const std::vector< float > weights = eventInfo()->mcEventWeights();
  if( weights.size() > 0 ) m_mc_gen_weight = weights[0];

  // pileup weights
  m_mc_pu_weight = 1.0;
  if (m_PRWTool) {
    //  https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ExtendedPileupReweighting
    CHECK("FillMCInfo()", m_PRWTool->apply( *eventInfo() ));
    //m_mc_pu_weight = eventInfo()->auxdata<float>("PileupWeight"); // Should have decoration
    m_mc_pu_weight = m_PRWTool->getCombinedWeight( *eventInfo() );
    histoStore()->fillTH1F("h_pu_weight", m_mc_pu_weight, w );
  }

  w = weight();

  return;
}

void PIDAnalysis::FillOccupancyInfo ()
//------------------------------------------------------------------------------
{
  static SG::AuxElement::Accessor<float> MyAccessorG("TRTOccGlobal");
  static SG::AuxElement::Accessor<float> MyAccessorBC("TRTOccBarrelC");
  static SG::AuxElement::Accessor<float> MyAccessorBA("TRTOccBarrelA");
  static SG::AuxElement::Accessor<float> MyAccessorECAC("TRTOccEndcapAC");
  static SG::AuxElement::Accessor<float> MyAccessorECBC("TRTOccEndcapBC");
  static SG::AuxElement::Accessor<float> MyAccessorECAA("TRTOccEndcapAA");
  static SG::AuxElement::Accessor<float> MyAccessorECBA("TRTOccEndcapBA");


  if(! (MyAccessorG.isAvailable(*eventInfo())) ){
    Fatal("execute()", "PIDModules: Failed to retrieve Occupancies. Exiting." );
  } else { // If it has global assume the rest exists
    Occ_Glob = MyAccessorG(*eventInfo());
    Occ_B_C = MyAccessorBC(*eventInfo());
    Occ_B_A = MyAccessorBA(*eventInfo());
    Occ_ECA_C = MyAccessorECAC(*eventInfo());
    Occ_ECA_A = MyAccessorECAA(*eventInfo());
    Occ_ECB_C = MyAccessorECBC(*eventInfo());
    Occ_ECB_A = MyAccessorECBA(*eventInfo());
  }

  //Fill occupancy
  histoStore()->fillTH1F("h_OccGlobal", Occ_Glob, w );
  histoStore()->fillTH1F("h_OccBRL_sA", Occ_B_A, w );
  histoStore()->fillTH1F("h_OccBRL_sC", Occ_B_C, w );
  histoStore()->fillTH1F("h_OccECA_sA", Occ_ECA_A, w );
  histoStore()->fillTH1F("h_OccECA_sC", Occ_ECA_C, w );
  histoStore()->fillTH1F("h_OccECB_sA", Occ_ECB_A, w );
  histoStore()->fillTH1F("h_OccECB_sC", Occ_ECB_C, w );
  histoStore()->fillTProfile( "h_avgmu_OG", m_averageIntPerXing, Occ_Glob, w );
}


void PIDAnalysis::SetupGRL()
//------------------------------------------------------------------------------
{
  if (isMC()) return; // Don't waste your time.

  std::vector<std::string> vecGRL;
  for (auto grl: m_config.getStrV("EventHandler.GRL")) {
    std::cout << "GRL = " << grl.Data() << std::endl;
    vecGRL.push_back(PathResolverFindCalibFile(grl.Data()));
  }

  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
  CHECK( "SetupGRL()", m_grl->setProperty("GoodRunsListVec", vecGRL));
  CHECK( "SetupGRL()", m_grl->setProperty("PassThrough", false) ); 

  if (m_grl->initialize().isFailure())
    PID::fatal("Failed to initialize GRL tool");
}


void PIDAnalysis :: SetupPRWTool ()
//------------------------------------------------------------------------------
{
  bool usePRW = m_config.getBool("EventHandler.PRW.UsePRW", true);
  if (!usePRW) return;

  m_prwSF = m_config.getNum("EventHandler.PRW.DataScaleFactor", 0.862069);
  int defChan  = m_config.getNum("EventHandler.PRW.DefaultChannel" , 314000  );
  
  std::vector<std::string> confFiles;
  std::vector<std::string> lcalcFiles;

  for (TString val: m_config.getStrV("EventHandler.PRW.ConfigFiles"))
    confFiles.push_back(val.Data());

  for (TString val: m_config.getStrV("EventHandler.PRW.LumiCalcFiles"))
    lcalcFiles.push_back(val.Data());

  if ( (confFiles.size() < 1) || (lcalcFiles.size() < 1) ) {
    Warning("SetupPRWTool","Sample is MC and no pileup reweighting info is provided, will run anyway...");
    return;
  } else {
    m_PRWTool = new CP::PileupReweightingTool("PRWTool");
    CHECK("SetupPRWTool", m_PRWTool->setProperty(     "ConfigFiles", confFiles ));
    CHECK("SetupPRWTool", m_PRWTool->setProperty(   "LumiCalcFiles", lcalcFiles ));
    CHECK("SetupPRWTool", m_PRWTool->setProperty( "DataScaleFactor", m_prwSF      ));
    CHECK("SetupPRWTool", m_PRWTool->setProperty(  "DefaultChannel", defChan    ));
    if (m_PRWTool->initialize().isFailure())
      PID::fatal("Failed to initialize PRW tool");
  }
}
