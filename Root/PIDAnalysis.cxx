#include "PIDFramework/PIDAnalysis.h"
#include "PIDFramework/PIDIncludes.h"

#include "PIDFramework/HistogramStore.h"
#include "PIDFramework/Likelihood.h"
#include "PIDFramework/LepHandler.h"
#include "PIDFramework/PIDUtils.h"

using std::abs;
using std::cosh;
using std::sqrt;
using PID::GeV;
using std::cout;
using std::endl;


// this is needed to distribute the algorithm to the workers
ClassImp(PIDAnalysis)

PIDAnalysis :: PIDAnalysis () 
//------------------------------------------------------------------------------
: m_event(nullptr)
, m_store(nullptr)
, m_histoStore(nullptr)
, m_likelihood(nullptr)
, m_PRWTool(nullptr)
, m_grl(nullptr)
, m_verbose(false)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}


EL::StatusCode PIDAnalysis :: setupJob (EL::Job& job)
//------------------------------------------------------------------------------
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  job.useXAOD();

  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init( "PIDAnalysis" ).ignore();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode PIDAnalysis :: initialize ()
//------------------------------------------------------------------------------
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  
  
  // read event tree
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  
  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event()->getEntries() ); // print long long int
  
  // initialize counts for number of events
  m_eventCounter = -1;

  m_isMC = eventInfo()->eventType( xAOD::EventInfo::IS_SIMULATION );
  m_isData = !m_isMC;
  
  // -------------------------
  //  Prepare Config File
  // -------------------------
  
  // TEnv uses value from first file it's specified in.
  // If specified, read in additional configuration
  if (m_config.isDefined("Include"))
    for (TString cfg : m_config.getStrV("Include"))
      m_config.addFile(cfg);
  
  // Fill unspecified values from default config, specified here.
  if (!m_config.isDefined("BaseConfig")) {
     PID::fatal("You must specify a base configuration file, option: BaseConfig. Exiting.");
  } else {
     m_config.addFile(m_config.getStr("BaseConfig"));
  }

  /*
  // --------------------------------
  //  Still debating this feature.    // JV
  // --------------------------------
  if (!m_config.isDefined("Selection")) {
    PID::fatal("You must specify a supported selection in configuration file. Exiting.");
  } else {
    TString selType = m_config.getStr("Selection");
    selType.ToLower();
    if ( !((selType == "z_ee") || (selType == "z_mumu") || (selType == "jpsi_ee") || (selType == "jpsi_mumu")) )
      PID::fatal("Selection must be either Z_ee, Z_mumu, JPsi_ee, or JPsi_mumu. Exiting.");
  }
  */
  
  // Print configuration database, if requested
  if (m_config.getBool("PIDAnalysis.PrintConfig", true)) {
    Info("initialize()", "Printing full configuration:");
    m_config.printDB();
    Info("initialize()", " ");
  }
  
  // initialize handlers
  m_likelihood = new Likelihood();
  m_lepHandler = new LepHandler(event(),store());

  SetupGRL();
  SetupPRWTool();
  

  // Should clean this bit up a bit
  TString SampleName = m_config.getStr("SampleName","sample");
  SampleName.ToLower();
  
  m_selectionZee  = false;
  m_selectionZmm  = false;
  m_selectionJPee = false;
  m_selectionJPmm = false;

  if      (SampleName.Contains("zee"))    m_selectionZee  = true;
  else if (SampleName.Contains("zmumu"))  m_selectionZmm  = true;
  else if (SampleName.Contains("jpee"))   m_selectionJPee = true;
  else if (SampleName.Contains("jpmumu")) m_selectionJPmm = true;
  else PID::fatal("Please choose a valid selection. Exiting.");

  isEl = false;
  isMu = false;
  isZB = false;
  isJP = false;

  if (  m_selectionZee || m_selectionJPee ) isEl = true;
  if (  m_selectionZmm || m_selectionJPmm ) isMu = true;
  if (  m_selectionZee || m_selectionZmm  ) isZB = true;
  if ( m_selectionJPee || m_selectionJPmm ) isJP = true;

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode PIDAnalysis :: histInitialize ()
//------------------------------------------------------------------------------
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  m_histoStore = new HistogramStore();

  // Pile-up and TRT occupancies:
  // ----------------------------
  TString evts = ";Events";
  histoStore()->createTH1F("h_averageMu",   30,      0,    30, "; #LT#mu#GT"+evts);
  histoStore()->createTH1F("h_OccGlobal",  101, -0.005, 1.005, "; TRT Global Occupancy"+evts);
  histoStore()->createTH1F("h_OccBRL_sA",  101, -0.005, 1.005, "; TRT Regional Occupancy"+evts);
  histoStore()->createTH1F("h_OccBRL_sC",  101, -0.005, 1.005, "; TRT Regional Occupancy"+evts);
  histoStore()->createTH1F("h_OccECA_sA",  101, -0.005, 1.005, "; TRT Regional Occupancy"+evts);
  histoStore()->createTH1F("h_OccECA_sC",  101, -0.005, 1.005, "; TRT Regional Occupancy"+evts);
  histoStore()->createTH1F("h_OccECB_sA",  101, -0.005, 1.005, "; TRT Regional Occupancy"+evts);
  histoStore()->createTH1F("h_OccECB_sC",  101, -0.005, 1.005, "; TRT Regional Occupancy"+evts);
  histoStore()->createTH1F("h_pu_weight",  500, 0, 50, "; pileup weights"+evts);
  histoStore()->createTProfile("h_avgmu_OG",   50,   -0.5,  49.5, "; #LT#mu#GT; Average TRT Global Occupancy"  );
  //histoStore()->createTProfile(  "h_NPV_OG",   50,   -0.5,  49.5, "; #LT#mu#GT; Average TRT Global Occupancy"  );
  
  // GSF Studies:
  // ------------
  histoStore()->createTH1F("h_EoverP_InDet",  100, 0., 2., "; E^{cluster}_{T}/p^{InDet}_{T}"+evts);
  histoStore()->createTH1F("h_EoverP_GSF",    100, 0., 2., "; E^{cluster}_{T}/p^{GSF}_{T}"+evts);
  histoStore()->createTH1F("h_tEoverP_Calo",  100, 0., 2., "; E^{true}_{T}/E^{cluster}_{T}"+evts);
  histoStore()->createTH1F("h_tEoverP_InDet", 100, 0., 2., "; E^{true}_{T}/p^{InDet}_{T}"+evts);
  histoStore()->createTH1F("h_tEoverP_GSF",   100, 0., 2., "; E^{true}_{T}/p^{GSF}_{T}"+evts);
  histoStore()->createTH1F("h_trueres_Calo",  100, -100, 100, "; E^{true}_{T} - E^{cluster}_{T} [GeV]"+evts); 
  histoStore()->createTH1F("h_trueres_InDet", 100, -100, 100, "; E^{true}_{T} - p^{InDet}_{T} [GeV]"+evts); 
  histoStore()->createTH1F("h_trueres_GSF",   100, -100, 100, "; E^{true}_{T} - p^{GSF}_{T} [GeV]"+evts); 
  histoStore()->createTH1F("h_ftrueres_Calo",  100, -100, 100, "; (E^{true}_{T} - E^{cluster}_{T})/E^{true}_{T}"+evts); 
  histoStore()->createTH1F("h_ftrueres_InDet", 100, -100, 100, "; (E^{true}_{T} - p^{InDet}_{T})/E^{true}_{T}"+evts); 
  histoStore()->createTH1F("h_ftrueres_GSF",   100, -100, 100, "; (E^{true}_{T} - p^{GSF}_{T})/E^{true}_{T}"+evts); 
  histoStore()->createTH1F("h_tfrac_qOverP",  100, 0., 2., "; GSF qOverP/Truth"+evts);

  // Kinematics:
  // -----------
  histoStore()->createTH1F("h_massJPll", 40, 0., 10., "; m_{ll} [GeV]"+evts); 
  histoStore()->createTH1F("h_massZll", 50, 70.0, 120.0, "; m_{ll} [GeV]"+evts);
  histoStore()->createTH1F("h_lep_pt",  60, 0, 120, "; #it{p}_{T}^{lepton} [GeV]"+evts);
  histoStore()->createTH1F("h_lep_pt1", 60, 0, 120, "; #it{p}_{T}^{lepton, lead} [GeV]"+evts);
  histoStore()->createTH1F("h_lep_pt2", 60, 0, 120, "; #it{p}_{T}^{lepton, subl} [GeV]"+evts);
  histoStore()->createTH1F("h_trk_pt",  60, 0, 120, "; #it{p}_{T}^{track} [GeV]"+evts);
  histoStore()->createTH1F("h_trk_pt1", 60, 0, 120, "; #it{p}_{T}^{track, lead} [GeV]"+evts);
  histoStore()->createTH1F("h_trk_pt2", 60, 0, 120, "; #it{p}_{T}^{track, subl} [GeV]"+evts);
  
  histoStore()->createTH2F("h2_ProbElVsEta", 100, 0., 1., 5, -0.5, 4.5, "; pHT_El; Eta Bin"+evts);
  for( int iBin(0); iBin < 5; iBin++ ) {
    TString hPname = TString::Format("h_ProbEl_%detaBin", iBin);
    histoStore()->createTH1F(hPname, 100, 0., 1., "; pHT_el"+evts);
  }

  // Troels LHs: 
  // ------------
  // etaBin, fHT, occupancy (require >= 20 TRT hits)
  histoStore()->createTH3F(  "h3_fHT_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHT; Global Occupancy");
  histoStore()->createTH3F(  "h3_fHT_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHT; Track Occupancy");
  histoStore()->createTH3F("h3_fHTXe_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHTXe; Global Occupancy");
  histoStore()->createTH3F("h3_fHTXe_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHTXe; Track Occupancy");
    
  // etaBin, fHTMB, occupancy (require >= 20 TRT hits)
  histoStore()->createTH3F(  "h3_fHTMB_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHT; Global Occupancy");
  histoStore()->createTH3F(  "h3_fHTMB_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHT; Track Occupancy");
  histoStore()->createTH3F("h3_fHTMBXe_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHTXe; Global Occupancy");
  histoStore()->createTH3F("h3_fHTMBXe_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; fHTXe; Track Occupancy");
    
  // etaBin,  LH, occupancy (require >= 20 TRT hits)
  histoStore()->createTH3F(     "h3_LH_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LH; Global Occupancy");
  histoStore()->createTH3F(     "h3_LH_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LH; Track Occupancy");
  histoStore()->createTH3F(    "h3_LHT_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LHT; Global Occupancy");
  histoStore()->createTH3F(    "h3_LHT_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LHT; Track Occupancy");
  histoStore()->createTH3F(   "h3_LHFO_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LHFO; Global Occupancy");
  histoStore()->createTH3F(   "h3_LHFO_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LHFO; Track Occupancy");

  // Combinations of CFs ON/OFF
     // CF0 =  All OFF
     // CF1 =  All ON
     // CF2 =  SL
     // CF3 =  TW
     // CF4 =  ZR
     // CF5 =  SL * TW
     // CF6 =  SL * ZR
     // CF7 =  TW * ZR
  for( int iCF(0); iCF < 8; iCF++ ){
    TString h3head = TString::Format("h3_LHCF%d", iCF);
    histoStore()->createTH3F(  h3head+"_OG", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LHNew CF Comb; Global Occupancy");
    histoStore()->createTH3F(  h3head+"_OT", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 50, 0.0, 1.0, ";; LHNew CF Comb; Global Occupancy");
    histoStore()->createTH3F(  h3head+"_nTRThist", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; LHNew CF Comb; nTRThit");
  }

  // etaBin,  fHT/fHTMB/LH, nTRThits 
  histoStore()->createTH3F(      "h3_LH_nTRThit", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; LH; nTRThit");
  histoStore()->createTH3F(     "h3_LHT_nTRThit", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; LHT; nTRThit");
  histoStore()->createTH3F(    "h3_LHFO_nTRThit", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; LHFO; nTRThit");
  histoStore()->createTH3F(     "h3_fHT_nTRThit", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; fHT; nTRThit");
  histoStore()->createTH3F(   "h3_fHTMB_nTRThit", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; fHTMB; nTRThit");
  histoStore()->createTH3F(   "h3_fHTXe_nTRThit", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; fHTXe; nTRThit");
  histoStore()->createTH3F( "h3_fHTMBXe_nTRThit", 5, -0.5, 4.5, 200.0, 0.0, 1.0, 61, -0.5, 60.5, ";; fHTMBXe; nTRThit");
  
  
  // Average # of TRT hits as a function of eta:
  // -------------------------------------------
  TString hlb = "; average number of TRT hits"; 
  for( TString nVar: {"nLT","nLTB","nLTEA","nLTEB","nHT","nHTB","nHTEA","nHTEB"} ) {
    histoStore()->createTProfile("h_eta_"+nVar, 125, 0.0, 2.5, "; |#eta|"+hlb );
  }

//Get the average track momentum in each detector region
  for( TString part: {"_BRL", "_ECA", "_ECB"} ) {
    histoStore()->createTProfile("h_avgtrkp"+part, 1, 0.5, 1.5, "; Arbitrary Unit; #LT p_{trk} #GT" );
  }

  // TRT distributions:
  // ------------------
  TString eff = "; High-Threshold Probability";
  TString effAr = "; High-Threshold Probability (Ar)";
  TString effXe = "; High-Threshold Probability (Xe)";
  TString effMBX = "; High-Threshold Probability (MBX)";
  TString effArMBX = "; High-Threshold Probability (MBX, Ar)";
  TString effXeMBX = "; High-Threshold Probability (MBX, Xe)";
  TString frac = "; Fraction of Argon Straws";
  std::vector<std::string> partname;
    partname.push_back("_BRL");
    partname.push_back("_ECA");
    partname.push_back("_ECB");

  int NbinsSL[5] = {73, 96, 64};
  int NbinsZR[5] = {36, 40, 40};
  double ZRmin[5] = {  0.0,  630.0,  630.0};
  double ZRmax[5] = {720.0, 1030.0, 1030.0};
  int NbinsTW = 44; double TWmin = 0.0; double TWmax = 2.2;
  int NbinsOcc = 50; double OccMin = 0.0; double OccMax = 1.0;
 
  // Plot TW distance. 
  histoStore()->createTH1F( "h_lepTW", NbinsTW, TWmin, TWmax ); 

  // Divide gamma range into bins logarithmically:
  const int NgBins     = 50;   // Number of gamma bins total, excl. over- and underflow
  const int NgBinDeca  = 10;   // Number of gamma bins per decade, starting at gMin
  double gMin = 10.0;          // Minimum gamma 1.0 (muon of 3 GeV has gamma=30) 
  std::vector<double> gBins;
  for (int ig=0; ig < NgBins+3; ig++) {
    //gBins[ig] = gMin * pow(10.0, double(ig)/double(NgBinDeca));
    gBins.push_back( gMin * pow(10.0, double(ig)/double(NgBinDeca)) );
  }
  
  // create TProfiles for HT fraction plots.
  TString hname;
  OccTypes = {"", "_LO", "_HO"};
  for( int iOccBin(0); iOccBin < 5; iOccBin++) { OccTypes.push_back( TString::Format("_OB%d",iOccBin) ); }
  
  for(unsigned int ip(0); ip < partname.size(); ip++ ) {
    for( TString OccType: OccTypes ) { 
      hname = "h_fHT"+OccType+partname[ip];
      histoStore()->createTProfile( hname+"_SL",  NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; straw layer"+eff );
      histoStore()->createTProfile( hname+"_ZR",  NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z position [mm]"+eff );
      histoStore()->createTProfile( hname+"_TW",  NbinsTW,  TWmin,  TWmax,  "; Track-to-Wire position [mm]"+eff );
      histoStore()->createTProfile( hname+"_OR",  NbinsOcc, OccMin, OccMax, "; Regional Occupancy"+eff ); // region occupancy 
      histoStore()->createTProfile( hname+"_OG",  NbinsOcc, OccMin, OccMax, "; Global Occupancy"+eff ); //global occupancy
      histoStore()->createTProfile( hname+"_OT",  NbinsOcc, OccMin, OccMax, "; Track Occupancy"+eff ); //global occupancy
      histoStore()->createTProfile( hname+"_GF",  gBins, "; #gamma Factor"+eff );
      
      hname = "h_fHT_Xe"+OccType+partname[ip];
      histoStore()->createTProfile( hname+"_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; straw layer"+effXe );
      histoStore()->createTProfile( hname+"_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z position [mm]"+effXe );
      histoStore()->createTProfile( hname+"_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire position [mm]"+effXe );
      histoStore()->createTProfile( hname+"_OR", NbinsOcc, OccMin, OccMax, "; Regional Occupancy"+effXe ); 
      histoStore()->createTProfile( hname+"_OG", NbinsOcc, OccMin, OccMax, "; Global Occupancy"+effXe );
      histoStore()->createTProfile( hname+"_OT", NbinsOcc, OccMin, OccMax, "; Track Occupancy"+effXe );
      histoStore()->createTProfile( hname+"_GF", gBins, "; #gamma Factor"+effXe );
      
      hname = "h_fHT_Ar"+OccType+partname[ip];
      histoStore()->createTProfile( hname+"_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; straw layer"+effAr );
      histoStore()->createTProfile( hname+"_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z position [mm]"+effAr );
      histoStore()->createTProfile( hname+"_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire position [mm]"+effAr );
      histoStore()->createTProfile( hname+"_OR", NbinsOcc, OccMin, OccMax, "; Regional Occupancy"+effAr ); 
      histoStore()->createTProfile( hname+"_OG", NbinsOcc, OccMin, OccMax, "; Global Occupancy"+effAr );
      histoStore()->createTProfile( hname+"_OT", NbinsOcc, OccMin, OccMax, "; Track Occupancy"+effAr );
      histoStore()->createTProfile( hname+"_GF", gBins, "; #gamma Factor"+effAr );
      
      hname = "h_fHT"+OccType+"_MBX"+partname[ip];
      histoStore()->createTProfile( hname+"_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; straw layer"+effMBX );
      histoStore()->createTProfile( hname+"_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z position [mm]"+effMBX );
      histoStore()->createTProfile( hname+"_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire position [mm]"+effMBX );
      histoStore()->createTProfile( hname+"_OR", NbinsOcc, OccMin, OccMax, "; Regional Occupancy"+effMBX ); 
      histoStore()->createTProfile( hname+"_OG", NbinsOcc, OccMin, OccMax, "; Global Occupancy"+effMBX );
      histoStore()->createTProfile( hname+"_OT", NbinsOcc, OccMin, OccMax, "; Track Occupancy"+effMBX );
      histoStore()->createTProfile( hname+"_GF", gBins, "; #gamma Factor"+effMBX );
      
      hname = "h_fHT_Xe"+OccType+"_MBX"+partname[ip];
      histoStore()->createTProfile( hname+"_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; straw layer"+effXeMBX );
      histoStore()->createTProfile( hname+"_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z position [mm]"+effXeMBX );
      histoStore()->createTProfile( hname+"_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire position [mm]"+effXeMBX );
      histoStore()->createTProfile( hname+"_OR", NbinsOcc, OccMin, OccMax, "; Regional Occupancy"+effXeMBX ); 
      histoStore()->createTProfile( hname+"_OG", NbinsOcc, OccMin, OccMax, "; Global Occupancy"+effXeMBX );
      histoStore()->createTProfile( hname+"_OT", NbinsOcc, OccMin, OccMax, "; Track Occupancy"+effXeMBX );
      histoStore()->createTProfile( hname+"_GF", gBins, "; #gamma Factor"+effXeMBX );
      
      hname = "h_fHT_Ar"+OccType+"_MBX"+partname[ip];
      histoStore()->createTProfile( hname+"_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; straw layer"+effArMBX );
      histoStore()->createTProfile( hname+"_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z position [mm]"+effArMBX );
      histoStore()->createTProfile( hname+"_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire position [mm]"+effArMBX );
      histoStore()->createTProfile( hname+"_OR", NbinsOcc, OccMin, OccMax, "; Regional Occupancy"+effArMBX ); 
      histoStore()->createTProfile( hname+"_OG", NbinsOcc, OccMin, OccMax, "; Global Occupancy"+effArMBX );
      histoStore()->createTProfile( hname+"_OT", NbinsOcc, OccMin, OccMax, "; Track Occupancy"+effArMBX );
      histoStore()->createTProfile( hname+"_GF", gBins, "; #gamma Factor"+effArMBX );
    }

    hname = "h_fAr"+partname[ip];
    histoStore()->createTProfile( hname+"_SL", NbinsSL[ip], -0.5, double(NbinsSL[ip])-0.5, "; straw layer"+frac );
    histoStore()->createTProfile( hname+"_ZR", NbinsZR[ip], ZRmin[ip], ZRmax[ip], "; Z position [mm]"+frac );
    histoStore()->createTProfile( hname+"_TW", NbinsTW, TWmin, TWmax, "; Track-to-Wire position [mm]"+frac );
    histoStore()->createTProfile( hname+"_OR", NbinsOcc, OccMin, OccMax, "; Regional Occupancy"+frac ); 
    histoStore()->createTProfile( hname+"_OG", NbinsOcc, OccMin, OccMax, "; Global Occupancy"+frac );
    histoStore()->createTProfile( hname+"_OT", NbinsOcc, OccMin, OccMax, "; Track Occupancy"+frac );
    histoStore()->createTProfile( hname+"_GF", gBins, "; #gamma Factor"+frac );

    hname = "h"+partname[ip];
    histoStore()->createTH1F( hname+"_l1pt", 50, 0.0, 200.0 );
    histoStore()->createTH1F( hname+"_l2pt", 50, 0.0, 200.0 );
    histoStore()->createTH1F( hname+"_t1pt", 50, 0.0, 200.0 );
    histoStore()->createTH1F( hname+"_t2pt", 50, 0.0, 200.0 );
    histoStore()->createTH1F( hname+"_ntrt", 60, -0.5, 59.5 );

    for( TString gasType: {"", "_Xe", "_Ar"} ) {
      // Global Occupancy Onset Curves
      histoStore()->createTH2F(   "h2_nLT"+gasType+partname[ip]+"_GF_OG", gBins, 50, 0, 1, "; #gamma Factor; Global Occupancy"+eff );
      histoStore()->createTH2F(   "h2_nHT"+gasType+partname[ip]+"_GF_OG", gBins, 50, 0, 1, "; #gamma Factor; Global Occupancy"+eff );
      histoStore()->createTH2F( "h2_nHTMB"+gasType+partname[ip]+"_GF_OG", gBins, 50, 0, 1, "; #gamma Factor; Global Occupancy"+eff );
      
      // Regional Occupancy Onset Curves
      histoStore()->createTH2F(   "h2_nLT"+gasType+partname[ip]+"_GF_OR", gBins, 50, 0, 1, "; #gamma Factor; Regional Occupancy"+eff );
      histoStore()->createTH2F(   "h2_nHT"+gasType+partname[ip]+"_GF_OR", gBins, 50, 0, 1, "; #gamma Factor; Regional Occupancy"+eff );
      histoStore()->createTH2F( "h2_nHTMB"+gasType+partname[ip]+"_GF_OR", gBins, 50, 0, 1, "; #gamma Factor; Regional Occupancy"+eff );
      
      // Track Occupancy Onset Curves
      histoStore()->createTH2F(   "h2_nLT"+gasType+partname[ip]+"_GF_OT", gBins, 50, 0, 1, "; #gamma Factor; Track Occupancy"+eff );
      histoStore()->createTH2F(   "h2_nHT"+gasType+partname[ip]+"_GF_OT", gBins, 50, 0, 1, "; #gamma Factor; Track Occupancy"+eff );
      histoStore()->createTH2F( "h2_nHTMB"+gasType+partname[ip]+"_GF_OT", gBins, 50, 0, 1, "; #gamma Factor; Track Occupancy"+eff );
      
    }
  }
  
  // register all histograms
  for (auto *histo : histoStore()->getListOfHistograms()) {
     wk()->addOutput(histo);
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode PIDAnalysis :: fileExecute ()
//------------------------------------------------------------------------------
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode PIDAnalysis :: changeInput (bool firstFile)
//------------------------------------------------------------------------------
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode PIDAnalysis :: execute ()
//------------------------------------------------------------------------------
{
  // print every 1000 events, so we know where we are:
  m_eventCounter++;
  if( m_eventCounter == 0) m_startTime = time(NULL);
  if( m_eventCounter && (m_eventCounter % 1000 == 0) ) {
    Info("execute()","%i events processed so far  <<<==", m_eventCounter );
    Info("execute()","Processing rate = %f Hz", float(m_eventCounter)/(time(NULL)-m_startTime));
  }

  // Check Event Overlap
  if (m_config.getBool("PIDAnalysis.RemoveEventOverlap", true)) {
    int run = eventInfo()->runNumber();
    int event = eventInfo()->eventNumber();
    long long int tag = 100000000000*run + event;
    if ( runEvtList.count(tag) == 0 ) runEvtList.insert( tag );
    else {
      std::cerr << "EVENT OVERLAP :: Run " << run << " Event " << event << std::endl;
      std::cerr << "EVENT OVERLAP :: Skipping Event" << std::endl;
      return EL::StatusCode::SUCCESS;
    }
  }
 
  // Fill event info
  FillEventInfo();
  bool showDEBUG = false;
  if (showDEBUG) printf( "\n\nRun = %8d   Event = %llu \n", m_runNumber, m_eventNumber);
 
  // Need to ensure these are called before any weigth or mu distrobution
  //  --> Good Idea to have this all done in an inherrited class ---  \\JV
  w = 1.0; 
  if (isMC()) FillMCInfo();

  // Fill occupanicy variables
  FillOccupancyInfo();
  
  //Occupancy[ SideA ][ part ] 
  double Occ[2][3] = { {Occ_B_C, Occ_ECA_C, Occ_ECB_C}, {Occ_B_A, Occ_ECA_A, Occ_ECB_A} };

  // Get the name of the sample 
  //double Xsec_sample = wk()->metaData()->getDouble("nc_xs") * 1000000000000000000.0;
  //std::string SampleName = wk()->metaData()->getString("sample_name");
  
  // if data check if event passes GRL
  if (!passGRL()) return EL::StatusCode::SUCCESS;
 
  //----------------------
  // Lepton Selection
  //----------------------
  int nLeptons = 0, nElectrons = 0, nMuons = 0;
  TLorentzVector Zll, lep[2];
  const xAOD::TrackParticle* trks[2];
  int qlep[2] = {    0,    0};
  double pt_lep[2] = { -999., -999.};
  
  //double minPtGeV(10); // Minimum Lepton Pt (GeV)
  double minPtGeV(20); // Minimum Lepton Pt (GeV)
  if (isJP) minPtGeV = 2.5;

  if (isEl) {
    xAOD::ElectronContainer electrons = lepHandler()->getSelectedElectrons( minPtGeV );
    if ( electrons.size() < 2 ) return EL::StatusCode::SUCCESS;
    nElectrons = electrons.size();
    for( int i(0); i < 2; i++ ) {
      pt_lep[i] = electrons[i]->pt();
      qlep[i] = int(electrons[i]->charge());
      trks[i] = lepHandler()->getTrack(electrons[i]);
      lep[i] = electrons[i]->p4();
    }
  } else if (isMu) {
    xAOD::MuonContainer muons = lepHandler()->getSelectedMuons( minPtGeV );
    if ( muons.size() < 2 ) return EL::StatusCode::SUCCESS;
    nMuons = muons.size();
    for( int i(0); i < 2; i++ ) {
      pt_lep[i] = muons[i]->pt();
      qlep[i] = int(muons[i]->charge());
      trks[i] = lepHandler()->getTrack(muons[i]);
      lep[i] = muons[i]->p4();
    }
  }
  nLeptons = nMuons + nElectrons;

  //----------------------
  // Event Selection
  //----------------------
  bool passZselection = false;
  if ( (nLeptons >= 2) && (qlep[0]*qlep[1] < 0) ) {
    Zll = lep[0]+lep[1];
    if (m_verbose) Info("execute()", "Z(ll) = %6.3f GeV      pz = %6.1f  pt = %6.1f", Zll.M()*GeV, Zll.Pz()*GeV, Zll.Pt()*GeV);
    if (isZB) {
      if ( abs(Zll.M() - 91.e3) < 15.e3 ) passZselection = true;
    } else if (isJP) {
      passZselection = true;
    }
  }
  if (!passZselection) return EL::StatusCode::SUCCESS;


  if (m_verbose) Info("execute()", "Passes Z selection");
  histoStore()->fillTH1F("h_massJPll", Zll.M()*GeV, w );
  histoStore()->fillTH1F("h_massZll", Zll.M()*GeV, w );
  histoStore()->fillTH1F("h_lep_pt", pt_lep[0]*GeV, w ); 
  histoStore()->fillTH1F("h_lep_pt", pt_lep[1]*GeV, w ); 
  histoStore()->fillTH1F("h_lep_pt1", pt_lep[0]*GeV, w ); 
  histoStore()->fillTH1F("h_lep_pt2", pt_lep[1]*GeV, w );
  
  //--------------------
  // Tracks
  //--------------------
  //double rmin =  554.0;
  //double rmax = 1082.0;
  //double zmin =  848.0;  // Detector paper:  827mm
  //double zmax = 2710.0;  // Detector paper: 2744mm
  double pMuonMax = 50.e3;   // Maximum muon momentum (to remain on lower plateau)
  

  for( int it = 0; it < 2; it++ ) {
    const xAOD::TrackParticle *trk = trks[it];
    if (trk == nullptr) continue;
 
    // Setup electron Probabilities
    double LHel(1.),    LHmu(1.);
    double LHel_FO(1.), LHmu_FO(1.);

    // For comparing CFs ON and OFF
    double LHel_prod[8], LHmu_prod[8];
    for( int itype(0); itype < 8; itype++ ) {
      LHel_prod[itype] = 1.0;
      LHmu_prod[itype] = 1.0;
    }
    
    //Make your track selection
    double ptlep = trk->auxdata< double >("ptlep");
    if ( trk->pt() < 0.25*ptlep ) continue;
    if ( trk->pt() < 20.e3 ) continue;
    //double trkpt = trk->auxdata< double >("trkpt"); //uncomment to try GSF tracks
    //if ( trkpt < 0.25*ptlep ) continue; //uncomment to try GSF tracks
    
    int nIBLhits  = trk->auxdata< unsigned char >("numberOfInnermostPixelLayerHits");
    int nBLayhits = trk->auxdata< unsigned char >("numberOfNextToInnermostPixelLayerHits");
    int nPixhits  = trk->auxdata< unsigned char >("numberOfPixelHits");
    int nSCThits  = trk->auxdata< unsigned char >("numberOfSCTHits");
    int nTRThits  = trk->auxdata< unsigned char >("numberOfTRTHits"); 
	  
    if(( nIBLhits+nBLayhits) < 1 ) continue;
	  if(( nPixhits+nSCThits) < 6 ) continue;
	  if ( nTRThits < 5 ) continue;
      
    // Basic quantities of the track:
    double eta = trk->eta();
    double theta = trk->theta();
    double z0 = trk->z0();
    double pt = trk->pt();
    double pTrk = pt * cosh(eta);
    //double pt = trkpt; //uncomment to try GSF tracks
    //double pTrk = trk->auxdata< double >("pTrk"); //uncomment to try GSF tracks
    if (showDEBUG) {
    printf( "---------------------------------------------------------------------------------------\n");
    printf( "  Got track:   pT: %6.1f   eta: %6.5f   phi: %6.5f \n", pt, eta, trk->phi() );
    printf( "---------------------------------------------------------------------------------------\n");
    }

    double trackOcc = -999;
    // Dumb Fix until all samples are consistent. 
    //if (m_selectionZee && isMC()) trackOcc = trk->auxdata< float >("TRTLocalOccupancy");
    if (isMC()) trackOcc = trk->auxdata< float >("TRTLocalOccupancy");
    else trackOcc = trk->auxdata< float >("TRTTrackOccupancy");

    histoStore()->fillTH1F("h_trk_pt", pt*GeV, w );
    if (it == 0)      histoStore()->fillTH1F("h_trk_pt1", pt*GeV, w );
    else if (it == 1) histoStore()->fillTH1F("h_trk_pt2", pt*GeV, w );

    // Calculate gamma factor:
    static const double m_el = 0.511;
    static const double m_mu = 105.6; //139.57; // Use pion mass to compare to athena tool
    static const double m_pi = 139.6;
    
    double gamma = -999;
    if      (isEl) gamma = pTrk/m_el; //sqrt(pTrk*pTrk + m_el*m_el) / m_el;
    else if (isMu) gamma = pTrk/m_mu; //sqrt(pTrk*pTrk + m_mu*m_mu) / m_mu;  //JV
    else           gamma = pTrk/m_pi; //sqrt(pTrk*pTrk + m_pi*m_pi) / m_pi;
    // NOTE: Validate this conditional is working properly 
	
	  etaBin5 = -999;
    if      (fabs(trk->eta()) < 0.625) etaBin5 = 0;
    else if (fabs(trk->eta()) < 1.070) etaBin5 = 1;
    else if (fabs(trk->eta()) < 1.304) etaBin5 = 2;
    else if (fabs(trk->eta()) < 1.752) etaBin5 = 3;
    else if (fabs(trk->eta()) < 2.000) etaBin5 = 4;

    //-----------------
    // TRT Track Hits
    //-----------------
    int nArHits = 0;
    int nLTB  = 0, nHTB  = 0, nHTMBB  = 0;
    int nLTEA = 0, nHTEA = 0, nHTMBEA = 0;
    int nLTEB = 0, nHTEB = 0, nHTMBEB = 0;
    
    //Xe only counts
    int nLTBXe  = 0, nHTBXe  = 0, nHTMBBXe  = 0;
    int nLTEAXe = 0, nHTEAXe = 0, nHTMBEAXe = 0;
    int nLTEBXe = 0, nHTEBXe = 0, nHTMBEBXe = 0;
    
    int nhits = 0;
    
    typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
    static const char* measurementNames = "msosLink";   //Note the prefix could change
    //static const char* measurementNames = "IDDET1_msosLink";   //Note the prefix could change

    // Check if there are MSOS attached
    if ( !trk->isAvailable< MeasurementsOnTrack >( measurementNames ) ) {
	    Error("execute()","No MSOS on track!!!");
	    continue;
    }
    const MeasurementsOnTrack& measurementsOnTrack = trk->auxdata< MeasurementsOnTrack >( measurementNames );

    // loop over hits on tracks
    static TString partnames[3] = {"_BRL", "_ECA", "_ECB"};
    for ( auto msos_itr : measurementsOnTrack ) {
      if ( ! msos_itr.isValid() ) continue;
	    const xAOD::TrackStateValidation *msos = *msos_itr; 
	    if ( msos->detType() != 3  || msos->type() != 0 ) continue;   // require it is a TRT hit!
	    
      if ( !msos->trackMeasurementValidationLink().isValid() ) continue; 
	    const xAOD::TrackMeasurementValidation *TRTDriftCircle =  *(msos->trackMeasurementValidationLink());
	    
	    // --------------------------
      // Get information about hit:
	    // --------------------------
	    int module = TRTDriftCircle->auxdata<int>("layer");
      
      int part = 0;    // 0: Barrel, 1: EndcapA, 2: EndcapB
	    if (abs(TRTDriftCircle->auxdata<int>("bec")) == 2) part = (module < 6) ? 1 : 2;
      bool SideA = (TRTDriftCircle->auxdata<int>("bec") > 0) ? true : false;
      TString pname = partnames[part];
      
      //int hit_partition = part;
      //if (SideA) hit_partition += 3;
      //int hit_phisector = TRTDriftCircle->auxdata<int>("phi_module");
      
      int strawlayer = TRTDriftCircle->auxdata<int>("strawlayer");
	    int SL = -999;
	    double ZR = -999;
	    double TW = -999;
	  
	    bool isHT    =  (int(TRTDriftCircle->auxdata<char>("highThreshold")) > 0) ? true : false;
	    //bool isHTMB  = ((TRTDriftCircle->auxdata<unsigned int>("bitPattern")&int(pow(2,17))) > 0) ? true : false;
	    bool isHTMB  = ((TRTDriftCircle->auxdata<unsigned int>("bitPattern") & 131072) > 0) ? true : false;
	    bool isArgon = int(TRTDriftCircle->auxdata<char>("isArgon"));
      if (isArgon) nArHits++;
	  
	    // BARREL:
	    //int SLforPositionCalc;
	    if (part == 0) {
	      if (module == 0)       SL = strawlayer;
	      else if (module == 1)  SL = 19 + strawlayer;
	      else                   SL = 19 + 24 + strawlayer;
	      //SLforPositionCalc =  SL;
	      nLTB++;
        if(isHT) nHTB++;
        if (isHTMB) nHTMBB++;
        if(!isArgon) {
          nLTBXe++;
          if(isHT) nHTBXe++;
          if(isHTMB) nHTMBBXe++;
        }
	    // ENDCAP:
	    } else {
	      if (module < 6) {
	        SL = 16*module + strawlayer;
	        //SLforPositionCalc =  SL;
	        nLTEA++; 
          if (isHT) nHTEA++;
          if (isHTMB) nHTMBEA++;
          if(!isArgon) {
            nLTEAXe++;
            if(isHT) nHTEAXe++;
            if(isHTMB) nHTMBEAXe++;
          }
	      } else {
	        SL =  8*(module-6) + strawlayer;
	        //SLforPositionCalc =  6*16 + SL;
	        nLTEB++; 
          if (isHT) nHTEB++;
          if (isHTMB) nHTMBEB++;
          if(!isArgon) {
            nLTEBXe++;
            if(isHT) nHTEBXe++;
            if(isHTMB) nHTMBEBXe++;
          }
	      }
      }
	    
      if (part == 0) {
	      // Get approximate z-position of hit:
	      double Xstraw = TRTDriftCircle->auxdata< float >("globalX");
	      double Ystraw = TRTDriftCircle->auxdata< float >("globalY");
	      double pos_r = sqrt(Xstraw*Xstraw + Ystraw*Ystraw);
	      double pos_z = pos_r * tan(PID::PiHalf - theta) + z0;
	      ZR = fabs(pos_z);   // Pass to more global variable!
	      if (ZR > 719.99) ZR = 719.99;      // No zpos > 720mm 
	    } else {
	      // Get approximate r-position of hit:
	      double pos_z = TRTDriftCircle->auxdata< float >("globalZ");
	      double pos_r = (pos_z - z0) / tan(PID::PiHalf - theta);
	      ZR = fabs(pos_r);   // Pass to more global variable!
	      if (ZR <  640.01) ZR =  640.01;  // No rpos <  640mm
	      if (ZR > 1139.99) ZR = 1139.99;  // No rpos > 1140mm
	    }
	  
      // Get the Track-to-Wire distance:
	    float DClocalX   = TRTDriftCircle->auxdata< float >("localX"); // This is the drift circle estimated by straw!
	    float MSOSlocalX = msos->localX();                             // This is the Track-to-Wire distance!
     

      double GF = gamma;
	    TW = fabs(MSOSlocalX);
      if (TW >= 2.2) TW = 2.175; 
      histoStore()->fillTH1F("h_lepTW", TW, w); 
	  
      if (m_verbose) 
	      Info("execute()", " TRThit: part=%i  SL=%2d  ZR=%6.1f  TW=%5.3f (%5.3f)  \
            isArgon=%1d  isHT=%1d  isHTMB=%2d", part, SL, ZR, TW, DClocalX, isArgon, isHT, isHTMB );

      // nHit
      nhits++;
      if (showDEBUG) printf( " Hit: %1d  TrtPart: %i  GasType: %1d  SL: %2d  ZRpos: %6.3f  \
          TWdist: %5.6f  Occ_Local: %5.6f  HTMB: %1d \n",  nhits,   part,  isArgon,  SL,   ZR,  TW,  trackOcc,   isHTMB); 
	  
      // Fill histograms for each hit:
      float OG = Occ_Glob;
      float OR = Occ[SideA][part];
      float OT = trackOcc;
      //printf("  OG = %4.3f  OR = %4.3f  OT = %4.3f" , OG, OR, OT);
      
      bool isLowOcc = (OT < 0.20);

      static TString sGas[3] = {"","_Ar","_Xe"};
      bool isGas[3] = { true,  isArgon,  (!isArgon) };
      bool isOcc[3] = { true, isLowOcc, (!isLowOcc) };
      
      // ------------------------------------------------------------------------------------
      // Calculate contribution to likelihood:
      // ------------------------------------------------------------------------------------
      
      double pHTel_noCF = LH()->pHTvP_Xe(part, pTrk, m_el, OT); 
      double pHTmu_noCF = LH()->pHTvP_Xe(part, pTrk, m_mu, OT); 
      
      double pHTel_FO_noCF = LH()->pHTvP_Xe(part, pTrk, m_el, 0.35); 
      double pHTmu_FO_noCF = LH()->pHTvP_Xe(part, pTrk, m_mu, 0.35);

      if (isArgon) {
        pHTel_noCF    = LH()->pHTvP_Ar(part, pTrk, m_el, OT);
        pHTmu_noCF    = LH()->pHTvP_Ar(part, pTrk, m_mu, OT);
        
        pHTel_FO_noCF = LH()->pHTvP_Ar(part, pTrk, m_el, 0.35); //Fixed Occupancy 
        pHTmu_FO_noCF = LH()->pHTvP_Ar(part, pTrk, m_mu, 0.35);
      }
      
      double CorrEl  = LH()->Corr_el_SL(part, double(SL));
             CorrEl *= LH()->Corr_el_ZR(part, ZR);
             CorrEl *= LH()->Corr_el_TW(part, TW);
      
      double CorrMu  = LH()->Corr_mu_SL(part, double(SL)); 
             CorrMu *= LH()->Corr_mu_ZR(part, ZR);
             CorrMu *= LH()->Corr_mu_TW(part, TW);
     
      // MC Based Correction Factors, others are taken from data12
      //double CorrElMC  = LH()->Corr_elMC_SL(part, double(SL));
      //       CorrElMC *= LH()->Corr_elMC_ZR(part, ZR);
      //       CorrElMC *= LH()->Corr_elMC_TW(part, TW);
      
      //double CorrMuMC  = LH()->Corr_muMC_SL(part, double(SL)); 
      //       CorrMuMC *= LH()->Corr_muMC_ZR(part, ZR);
      //       CorrMuMC *= LH()->Corr_muMC_TW(part, TW);
      
      double pHTel = pHTel_noCF;      // Data Corrections
      double pHTmu = pHTmu_noCF;
      
      //double pHTel_MC = pHTel_noCF; // MC Corrections
      //double pHTmu_MC = pHTmu_noCF;
      
      double pHTel_FO = pHTel_FO_noCF; // FO = Fixed Occupancy
      double pHTmu_FO = pHTmu_FO_noCF;

      //double pHTel_FOMC = pHTvP_FOMC_noCF; // MC Corrections & Fixed Occupancy
      //double pHTel_FOMC = pHTvP_FOMC_noCF;
      
      double CFCombEl[8] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };
      double CFCombMu[8] = { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };

      // *******************
      //   ATTENTION!!! 
      // *******************
      // I think the PID tool actually uses Xe corrections on Argon, use this?
      if (!isArgon) {
        pHTel *= CorrEl;
        pHTmu *= CorrMu;

        //pHTel_MC *= CorrElMC;
        //pHTmu_MC *= CorrMuMC;
        
        pHTel_FO *= CorrEl;
        pHTmu_FO *= CorrMu;

        // CF Combinations ON/OFF
        double mcCF_el_SL = LH()->Corr_elMC_SL(part, double(SL));
        double mcCF_el_TW = LH()->Corr_elMC_TW(part, TW);
        double mcCF_el_ZR = LH()->Corr_elMC_ZR(part, ZR);
        
        double mcCF_mu_SL = LH()->Corr_muMC_SL(part, double(SL)); 
        double mcCF_mu_TW = LH()->Corr_muMC_TW(part, TW); 
        double mcCF_mu_ZR = LH()->Corr_muMC_ZR(part, ZR);
        
        // El CFs
        CFCombEl[0] = 1.0;   // All CFs off 
        CFCombEl[1] = mcCF_el_SL * mcCF_el_TW * mcCF_el_ZR;
        CFCombEl[2] = mcCF_el_SL;
        CFCombEl[3] = mcCF_el_TW;
        CFCombEl[4] = mcCF_el_ZR;
        CFCombEl[5] = mcCF_el_SL * mcCF_el_TW;
        CFCombEl[6] = mcCF_el_SL * mcCF_el_ZR;
        CFCombEl[7] = mcCF_el_TW * mcCF_el_ZR;
        
        // Mu CFs
        CFCombMu[0] = 1.0;   // All CFs off 
        CFCombMu[1] = mcCF_mu_SL * mcCF_mu_TW * mcCF_mu_ZR;
        CFCombMu[2] = mcCF_mu_SL;
        CFCombMu[3] = mcCF_mu_TW;
        CFCombMu[4] = mcCF_mu_ZR;
        CFCombMu[5] = mcCF_mu_SL * mcCF_mu_TW;
        CFCombMu[6] = mcCF_mu_SL * mcCF_mu_ZR;
        CFCombMu[7] = mcCF_mu_TW * mcCF_mu_ZR;
      }
     
      // ---------------------
      // Fill Likelihoods:
      // ---------------------
      // index 0 : Normal Likelihood with Data CFs
      //       1 : Fixed Occupancy with Data CFs
      //   Missing?
      //     - LHs using only MC CFs
      //     - LHs using only Xe hits?
      //     - Permutations of everything
      
      // index 0 : Standard LH w/ Data CFs
      if (isHTMB) { LHel *=     pHTel; LHmu *=     pHTmu;}
      else        { LHel *= 1.0-pHTel; LHmu *= 1.0-pHTmu;}
      
      // index 1 : Fixed Occupancy (0.35) LH w/ Data CFs
      if (isHTMB) { LHel_FO *=     pHTel_FO; LHmu_FO *=     pHTmu_FO;}
      else        { LHel_FO *= 1.0-pHTel_FO; LHmu_FO *= 1.0-pHTmu_FO;}
      
      // Turn CFs on and off
      for( int iCF(0); iCF < 8; iCF++ ) {
        if (isHTMB) { LHel_prod[iCF] *=     pHTel_noCF*CFCombEl[iCF]; LHmu_prod[iCF] *=     pHTmu_noCF*CFCombMu[iCF];}
        else        { LHel_prod[iCF] *= 1.0-pHTel_noCF*CFCombEl[iCF]; LHmu_prod[iCF] *= 1.0-pHTmu_noCF*CFCombMu[iCF];}
      }

      
      // --------------------------------
      // Plot probability HT profiles 
      // --------------------------------
      TString hname;
      for( int ig: {0,1,2} ) {
        if (!isGas[ig]) continue;
        for ( int io: {0,1,2} ) {
          if (!isOcc[io]) continue;
          //hname = "h_fHT"+sGas[ig]+sOcc[io]+pname;
          hname = "h_fHT"+sGas[ig]+OccTypes[io]+pname;
          histoStore()->fillTProfile( hname+"_GF", GF, isHT, w );
          if( pTrk < pMuonMax ) {
            histoStore()->fillTProfile( hname+"_SL", SL, isHT, w );
            histoStore()->fillTProfile( hname+"_ZR", ZR, isHT, w );
            histoStore()->fillTProfile( hname+"_TW", TW, isHT, w );
            histoStore()->fillTProfile( hname+"_OG", OG, isHT, w );
            histoStore()->fillTProfile( hname+"_OT", OT, isHT, w );
          }
        
          //hname = "h_fHT"+sGas[ig]+sOcc[io]+"_MBX"+pname;
          hname = "h_fHT"+sGas[ig]+OccTypes[io]+"_MBX"+pname;
          histoStore()->fillTProfile( hname+"_GF", GF, isHTMB, w );
          if ( pTrk < pMuonMax ) {
            histoStore()->fillTProfile( hname+"_SL", SL, isHTMB, w );
            histoStore()->fillTProfile( hname+"_ZR", ZR, isHTMB, w );
            histoStore()->fillTProfile( hname+"_TW", TW, isHTMB, w );
            histoStore()->fillTProfile( hname+"_OG", OG, isHTMB, w );
            histoStore()->fillTProfile( hname+"_OR", OR, isHTMB, w );
            histoStore()->fillTProfile( hname+"_OT", OT, isHTMB, w );
          }
        }

        // Fill 2D histograms
        histoStore()->fillTH2F( "h2_nLT"+sGas[ig]+pname+"_GF_OG", GF, OG, w );
        histoStore()->fillTH2F( "h2_nLT"+sGas[ig]+pname+"_GF_OR", GF, OR, w );
        histoStore()->fillTH2F( "h2_nLT"+sGas[ig]+pname+"_GF_OT", GF, OT, w );
        if (isHT) {
          histoStore()->fillTH2F( "h2_nHT"+sGas[ig]+pname+"_GF_OG", GF, OG, w );
          histoStore()->fillTH2F( "h2_nHT"+sGas[ig]+pname+"_GF_OR", GF, OR, w );
          histoStore()->fillTH2F( "h2_nHT"+sGas[ig]+pname+"_GF_OT", GF, OT, w );
        }
        if (isHTMB) {
          histoStore()->fillTH2F( "h2_nHTMB"+sGas[ig]+pname+"_GF_OG", GF, OG, w );
          histoStore()->fillTH2F( "h2_nHTMB"+sGas[ig]+pname+"_GF_OR", GF, OR, w );
          histoStore()->fillTH2F( "h2_nHTMB"+sGas[ig]+pname+"_GF_OT", GF, OT, w );
        }

        // Fill in distributions (weighted by nHits)
        if (it == 0) {
          histoStore()->fillTH1F( "h"+pname+"_l1pt", ptlep*0.001, w );
          histoStore()->fillTH1F( "h"+pname+"_t1pt", pTrk *0.001, w );
        } else {
          histoStore()->fillTH1F( "h"+pname+"_l2pt", ptlep*0.001, w );
          histoStore()->fillTH1F( "h"+pname+"_t2pt", pTrk *0.001, w );
        }
        
        // Fill in Bins of Occupancy
        int iOB = int(Occ_Glob/0.10);
        if (iOB > 4) iOB = 4;
        hname = "h_fHT"+sGas[ig]+OccTypes[iOB+3]+pname;
        histoStore()->fillTProfile( hname+"_GF", GF, isHT, w );
        if( pTrk < pMuonMax ) {  
          histoStore()->fillTProfile( "h_avgtrkp"+pname, 1, pTrk, w ); // Get <p_trk> in each region
          histoStore()->fillTProfile( hname+"_SL", SL, isHT, w );
          histoStore()->fillTProfile( hname+"_ZR", ZR, isHT, w );
          histoStore()->fillTProfile( hname+"_TW", TW, isHT, w );
          histoStore()->fillTProfile( hname+"_OG", OG, isHT, w );
          histoStore()->fillTProfile( hname+"_OR", OR, isHT, w );
          histoStore()->fillTProfile( hname+"_OT", OT, isHT, w );
        }
        
        hname = "h_fHT"+sGas[ig]+OccTypes[iOB+3]+"_MBX"+pname;
        histoStore()->fillTProfile( hname+"_GF", GF, isHTMB, w );
        if ( pTrk < pMuonMax ) {
          histoStore()->fillTProfile( hname+"_SL", SL, isHTMB, w );
          histoStore()->fillTProfile( hname+"_ZR", ZR, isHTMB, w );
          histoStore()->fillTProfile( hname+"_TW", TW, isHTMB, w );
          histoStore()->fillTProfile( hname+"_OG", OG, isHTMB, w );
          histoStore()->fillTProfile( hname+"_OR", OR, isHTMB, w );
          histoStore()->fillTProfile( hname+"_OT", OT, isHTMB, w );
        }

      }
      
      hname = "h_fAr"+pname;
      histoStore()->fillTProfile( hname+"_GF", GF, isArgon, w );
      histoStore()->fillTProfile( hname+"_SL", SL, isArgon, w );
      histoStore()->fillTProfile( hname+"_ZR", ZR, isArgon, w );
      histoStore()->fillTProfile( hname+"_TW", TW, isArgon, w );
      histoStore()->fillTProfile( hname+"_OG", OG, isArgon, w );
      histoStore()->fillTProfile( hname+"_OR", OR, isArgon, w );
      histoStore()->fillTProfile( hname+"_OT", OT, isArgon, w );
		
    } //End loop over hits

    // -----------------------------------
    // Fill 'per track' histograms here.
    // -----------------------------------
    if (etaBin5%2 == 0){
      TString prtname = "_BRL";
      if (etaBin5 == 2) prtname = "_ECA";
      if (etaBin5 == 4) prtname = "_ECB";
      histoStore()->fillTH1F( "h"+prtname+"_ntrt", nTRThits, w );
    }
    
    int nLT = ( nLTB + nLTEA + nLTEB );
    histoStore()->fillTProfile(   "h_eta_nLT", fabs(eta), nLT, w );
    histoStore()->fillTProfile(  "h_eta_nLTB", fabs(eta), nLTB, w );
    histoStore()->fillTProfile( "h_eta_nLTEA", fabs(eta), nLTEA, w );
    histoStore()->fillTProfile( "h_eta_nLTEB", fabs(eta), nLTEB, w );
    
    int nHT = ( nHTB + nHTEA + nHTEB );
    histoStore()->fillTProfile(   "h_eta_nHT", fabs(eta), nHT, w );
    histoStore()->fillTProfile(  "h_eta_nHTB", fabs(eta), nHTB, w );
    histoStore()->fillTProfile( "h_eta_nHTEA", fabs(eta), nHTEA, w );
    histoStore()->fillTProfile( "h_eta_nHTEB", fabs(eta), nHTEB, w );
 
    int nLTXe   = ( nLTBXe   + nLTEAXe   + nLTEBXe );
    int nHTXe   = ( nHTBXe   + nHTEAXe   + nHTEBXe );
    int nHTMB   = ( nHTMBB   + nHTMBEA   + nHTMBEB );
    int nHTMBXe = ( nHTMBBXe + nHTMBEAXe + nHTMBEBXe );
    double fHT     = double(nHT)/nLT;
    double fHTXe   = double(nHTXe)/nLTXe;
    double fHTMB   = double(nHTMB)/nLT;
    double fHTMBXe = double(nHTMBXe)/nLTXe;

    // Build electron probabilities  
    double   LH = LHel / ( LHel + LHmu );
    double LHFO = LHel_FO / ( LHel_FO + LHmu_FO );
    
    // Build electron probability with CFs ON / OFF in all permutations 
    double LHCF[8];
    for( int iCF(0); iCF < 8; iCF++ ) {
      LHCF[iCF] = LHel_prod[iCF]/(LHel_prod[iCF]+LHmu_prod[iCF]);
    }
   
    // NOTE!! -- Should be able to ~recreate the LHT with variables here // JV
    double LHT     = trk->auxdata< float >("eProbabilityHT");
    if (showDEBUG) printf("   tool = %6.4f     rootcore = %6.4f \n", LHT, LH ); // need to use MC CFs

    //remove tracks with pT > 50 from performance plots
    if ( pTrk >= pMuonMax ) continue;

    histoStore()->fillTH2F("h2_ProbElVsEta", LH, etaBin5, w );

    //for occupancy plots require nTRThits >= 20 for comparison to past
    histoStore()->fillTH3F(      "h3_LH_nTRThit", etaBin5,      LH, nTRThits, w );
    histoStore()->fillTH3F(     "h3_LHT_nTRThit", etaBin5,     LHT, nTRThits, w );
    histoStore()->fillTH3F(    "h3_LHFO_nTRThit", etaBin5,    LHFO, nTRThits, w );
    histoStore()->fillTH3F(     "h3_fHT_nTRThit", etaBin5,     fHT, nTRThits, w );
    histoStore()->fillTH3F(   "h3_fHTXe_nTRThit", etaBin5,   fHTXe, nTRThits, w );
    histoStore()->fillTH3F(   "h3_fHTMB_nTRThit", etaBin5,   fHTMB, nTRThits, w );
    histoStore()->fillTH3F( "h3_fHTMBXe_nTRThit", etaBin5, fHTMBXe, nTRThits, w );
    
    for( int iCF(0); iCF < 8; iCF++ ) {
      TString h3head = TString::Format("h3_LHCF%d", iCF);
      histoStore()->fillTH3F( h3head+"_nTRThist", etaBin5, LHCF[iCF], nTRThits, w );
    }
    
    if (nTRThits >= 20) {
      // Fraction HT
      histoStore()->fillTH3F(    "h3_fHT_OG", etaBin5,   fHT, Occ_Glob, w );
      histoStore()->fillTH3F(    "h3_fHT_OT", etaBin5,   fHT, trackOcc, w );
      histoStore()->fillTH3F(  "h3_fHTXe_OG", etaBin5, fHTXe, Occ_Glob, w );
      histoStore()->fillTH3F(  "h3_fHTXe_OT", etaBin5, fHTXe, trackOcc, w );
      // Fraction HT (Middle BX) 
      histoStore()->fillTH3F(  "h3_fHTMB_OG", etaBin5,   fHTMB, Occ_Glob, w );
      histoStore()->fillTH3F(  "h3_fHTMB_OT", etaBin5,   fHTMB, trackOcc, w );
      histoStore()->fillTH3F("h3_fHTMBXe_OG", etaBin5, fHTMBXe, Occ_Glob, w );
      histoStore()->fillTH3F("h3_fHTMBXe_OT", etaBin5, fHTMBXe, trackOcc, w );
      // Likelihood Method
      histoStore()->fillTH3F(    "h3_LH_OG", etaBin5,     LH, Occ_Glob, w );
      histoStore()->fillTH3F(    "h3_LH_OT", etaBin5,     LH, trackOcc, w );
      histoStore()->fillTH3F(   "h3_LHT_OG", etaBin5,    LHT, Occ_Glob, w );
      histoStore()->fillTH3F(   "h3_LHT_OT", etaBin5,    LHT, trackOcc, w );
      histoStore()->fillTH3F(  "h3_LHFO_OG", etaBin5,   LHFO, Occ_Glob, w );
      histoStore()->fillTH3F(  "h3_LHFO_OT", etaBin5,   LHFO, trackOcc, w );
      
      // CF Combinations ON/OFF
      for( int iCF(0); iCF < 8; iCF++ ) {
        TString h3head = TString::Format("h3_LHCF%d", iCF);
        histoStore()->fillTH3F( h3head+"_OG", etaBin5, LHCF[iCF], Occ_Glob, w );
        histoStore()->fillTH3F( h3head+"_OT", etaBin5, LHCF[iCF], trackOcc, w );
      }
    }

    if (etaBin5 >= 0) {
      TString LHhname = TString::Format("h_ProbEl_%detaBin", etaBin5);
      histoStore()->fillTH1F( LHhname, LH, w );
    }
    
  }//end loop on tracks

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode PIDAnalysis :: postExecute ()
//------------------------------------------------------------------------------
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode PIDAnalysis :: finalize ()
//------------------------------------------------------------------------------
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  MySafeDelete(m_grl);
  MySafeDelete(m_PRWTool);
  MySafeDelete(m_histoStore);
  MySafeDelete(m_likelihood);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode PIDAnalysis :: histFinalize ()
//------------------------------------------------------------------------------
{
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  Info("","Completed running over %d events", m_eventCounter);
  return EL::StatusCode::SUCCESS;
}

