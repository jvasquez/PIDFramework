#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>
#include <TGraphAsymmErrors.h>

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

double sqr(double a) {return a*a;}

double func_onset(double *x, double *par) {
  if (x[0] < 0.0) return 0.0;

  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par[4])/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par[4])/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par[1] - par[4])/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par[1]);
  
  if (log10(x[0])      < par[0]) return pHT_dEdx;
  else if (log10(x[0]) > par[1]) return pHT_HG;
  else                           return pHT_TR;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.025 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  h->GetYaxis()->SetTitleOffset(1.4);
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->SetBinContent( 0, 0 );
  h->SetBinContent( nbins, 0);
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHistoNorm2D(TH2 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetZaxis()->SetTitle("Fraction of Events / Bin");
  int nbinsx = h->GetNbinsX();
  int nbinsy = h->GetNbinsY();
  h->Scale( 1. / h->Integral( 0, (nbinsx+1), 0, (nbinsy+1) ) );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, float useMax=-999) 
{
  return drawEffHisto(getHisto(f,hn),opt,col,useMax);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHistoNorm2D(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm2D(getHisto2D(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

TGaxis *getElecAxis( double ypos=-0.055 ) {
  double mEl = 0.000511;
  TGaxis *axis2 = new TGaxis(5000.0, ypos, 598000.0, ypos, 5000.0*mEl, 598000.0*mEl, 50510, "G");
  axis2->SetTitle("       Electron momentum [GeV] ");
  axis2->SetLabelFont(42);
  axis2->SetTitleFont(42);
  axis2->SetLabelSize(0.045);
  axis2->SetTitleSize(0.035);
  //axis2->SetLabelOffset(0.00005);
  axis2->SetTitleOffset(1.2);
  axis2->SetLabelOffset(-0.008);
  axis2->CenterTitle();
  return axis2;
}

TGaxis *getMuonAxis( double ypos=-0.055 ) {
  double mMu = 0.10599;
  TGaxis *axis1 = new TGaxis(35.0, ypos, 4000.0, ypos, 35.0*mMu, 4000.0*mMu, 50510, "G");
  axis1->SetTitle("Muon momentum [GeV]    ");
  axis1->SetLabelFont(42);
  axis1->SetTitleFont(42);
  axis1->SetLabelSize(0.045);
  axis1->SetTitleSize(0.035);
  axis1->SetTitleOffset(1.2);
  axis1->SetLabelOffset(-0.008);
  axis1->CenterTitle();
  return axis1;
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTypeLabels(TString head, int col=kOrange+1) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  TString binType = "All HT bits";
  if (head.Contains("_MBX")) binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.92,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.92, strawType, col);
    drawText( 0.14, 0.87,   binType, col);
  }
}

  
void drawMajorLabels(TString hname, int col=kBlack) 
{
    drawText(0.685,0.92,"#bf{#it{ATLAS}} Work In Progress",col);
    TString local = "";
    if (hname.Contains("BRL")) local = "Barrel";
    if (hname.Contains("ECA")) local = "Endcap Type A";
    if (hname.Contains("ECB")) local = "Endcap Type B";
   
    TString side = ", Side A+C";
    if (hname.Contains("_sA")) side = ", Side A";
    if (hname.Contains("_sC")) side = ", Side C";
    if (local != "") local += side;
    drawText(0.685,0.87,local,col);
}


double integrate(TFile *f, TString hn )
{ 
  TH1 *h =  getHisto(f,hn); 
  double sum = h->Integral(); 
  return sum; 
} 


///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString  inFN("outputZee/hist-Zee.root");
  TString inFN2("outputZmm/hist-Zmumu.root"), pdf("plots.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile")  inFN=argv[++i];
    if (arg=="--inputFile2") inFN2=argv[++i];
    if (arg=="-in")  inFN=argv[++i];
    if (arg=="-in1")  inFN=argv[++i];
    if (arg=="-in2") inFN2=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }

  printf("\n  Running plotRecoEff\n");
  printf("  input file Z(ee): %s\n",inFN.Data());
  printf("  input file Z(mm): %s\n",inFN.Data());
  TFile *f = openFile(inFN);   // Z->ee
  TFile *f2 = openFile(inFN2); // Z->mm
  TFile *fc = openFile("CorrFactors.root"); // Correction Factors

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.02); can->SetRightMargin(0.04);
  can->Print(pdf+"[");

  // ----------------------------------
  // Fit Turn On Curve
  // ----------------------------------
  can->SetGridy(1);
  can->SetGridx(1);
  
  can->SetLogx(1);
  //can->SetBottomMargin(0.18);
  
  double UseMax = 0.40;
  //static const double m_el = 0.511;
  //static const double m_mu = 105.6;
  //static const double m_pi = 139.6;
  
  TH1 *h_el[3][5], *h_mu[3][5];
  TH1 *h_avgel[3], *h_avgmu[3];
  
  TF1 *f_el[3][5], *f_mu[3][5];
  TF1 *f_avgel[3], *f_avgmu[3];
  
  TF1 *f_avgocc[3][5];
  
  int cols[5] = { kViolet-5, kBlue, kGreen+2, kOrange+1, kRed };

  TString head = "h_fHT_Xe";
  TString parts[3] = {"_BRL","_ECA","_ECB"};
  TString occs[5] = { "_OB0", "_OB1", "_OB2", "_OB3", "_OB4" };
    
  static TString fitfn = "[0] + [1]*(log10(x) - [2]);";

  for ( TString bit: { "_MBX" } ) {
    for ( int ip(0); ip < 3; ip++ ) {
      TString part = parts[ip];

      h_avgel[ip] = drawEffHisto(  f, head+bit+part+"_GF", "", kBlack, UseMax );
      h_avgmu[ip] = drawEffHisto( f2, head+bit+part+"_GF", "", kBlack, UseMax );
        
      // define fit functions
      f_avgel[ip] = new TF1( "f_avgel"+part, fitfn,  20.e3, 100.e3 );
      f_avgel[ip]->SetLineColor(kBlack);
      f_avgel[ip]->SetParLimits( 1, 0., 1. );

      f_avgmu[ip] = new TF1( "f_avgmu"+part, fitfn,  100.0, 500.0  );
      f_avgmu[ip]->SetLineColor(kBlack);
      f_avgmu[ip]->SetParLimits( 1, 0., 1. );

      // get fit to inclusive onset curve
      h_avgel[ip]->Fit( f_avgel[ip], "QMR" );
      h_avgmu[ip]->Fit( f_avgmu[ip], "QMR" );

      // find average occupancy
      TH1 *h_avgocc = drawEffHisto(  f, head+bit+part+"_OG", "", kBlack, UseMax );
      double avgocc = h_avgocc->GetMean();
      std::cout << "Average Occupancy = " << avgocc << std::endl;

      // loop over occupancy bins
      for (int iOB(0); iOB < 5; iOB++ ) {
        TString occbin = occs[iOB];
        
        // find average occupancy of bin and correction factor
        TH1 *h_occ = drawEffHisto(  f, head+occbin+bit+part+"_OG", "", cols[iOB], UseMax );
        double binavgocc = h_occ->GetMean();

        //get hist from fc and then get bin content corresponding to avg.
        TH1 *h_CFel = drawHisto( fc, "el_cf_Xe"+bit+part+"_OG", "", kBlack ); 
        TH1 *h_CFmu = drawHisto( fc, "mu_cf_Xe"+bit+part+"_OG", "", kBlack );
        int CFbin = h_CFel->FindBin( binavgocc );
        double CFel = h_CFel->GetBinContent( CFbin );
        double CFmu = h_CFmu->GetBinContent( CFbin );

        std::cout << "   " << "Avg Occupancy (Bin " << iOB << ") = " << binavgocc << std::endl;
        std::cout << "      " << "CF(el) = " << CFel << std::endl;
        std::cout << "      " << "CF(mu) = " << CFmu << std::endl;
        
        // get histograms & fit them
        h_el[ip][iOB] = drawEffHisto(  f, head+occbin+bit+part+"_GF", "", cols[iOB], UseMax );
        h_mu[ip][iOB] = drawEffHisto( f2, head+occbin+bit+part+"_GF", "", cols[iOB], UseMax );
        
        // define predictions
        f_el[ip][iOB] = new TF1( "f_el"+occbin+bit+part, fitfn,  20.e3, 100.e3 );
        f_mu[ip][iOB] = new TF1( "f_mu"+occbin+bit+part, fitfn,  100.0, 500.0  );

        f_el[ip][iOB]->SetLineColor( cols[iOB] );
        f_mu[ip][iOB]->SetLineColor( cols[iOB] );

        f_el[ip][iOB]->SetParameter( 2, f_avgel[ip]->GetParameter(2) );
        f_el[ip][iOB]->SetParameter( 1, f_avgel[ip]->GetParameter(1)*CFel );
        f_el[ip][iOB]->SetParameter( 0, f_avgel[ip]->GetParameter(0)*CFel );

        f_mu[ip][iOB]->SetParameter( 2, f_avgmu[ip]->GetParameter(2) );
        f_mu[ip][iOB]->SetParameter( 1, f_avgmu[ip]->GetParameter(1)*CFmu );
        f_mu[ip][iOB]->SetParameter( 0, f_avgmu[ip]->GetParameter(0)*CFmu );
      }

      // draw only the averages 
      h_avgel[ip]->Draw();
      h_avgmu[ip]->Draw("same");
      
      drawMajorLabels(part+"_GF",kBlack); 
      drawTypeLabels( "h_fHT_Xe_MBX" );
      
      can->Print(pdf);

      // draw the predictions
      h_el[ip][0]->Draw();
      for( int iOB(0); iOB < 5; iOB++ ) {
        h_el[ip][iOB]->Draw("same");
        h_mu[ip][iOB]->Draw("same");
        f_el[ip][iOB]->Draw("same");
        f_mu[ip][iOB]->Draw("same");
      }
      
      drawMajorLabels(part+"_GF",kBlack); 
      drawTypeLabels( "h_fHT_Xe_MBX" );
      
      can->Print(pdf);
    }
  }
  
  can->SetGridy(0);
  can->SetGridx(0);

  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f->Close();


}


