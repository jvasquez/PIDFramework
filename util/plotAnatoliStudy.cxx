#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>
#include <TGraphErrors.h>

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

double sqr(double a) {return a*a;}

double func_onset(double *x, double *par) {
  if (x[0] < 0.0) return 0.0;

  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par[4])/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par[4])/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par[1] - par[4])/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par[1]);
  
  if (log10(x[0])      < par[0]) return pHT_dEdx;
  else if (log10(x[0]) > par[1]) return pHT_HG;
  else                           return pHT_TR;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.015 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  h->GetYaxis()->SetTitleOffset(1.4);
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->SetBinContent( 0, 0 );
  h->SetBinContent( nbins, 0);
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHistoNorm2D(TH2 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetZaxis()->SetTitle("Fraction of Events / Bin");
  int nbinsx = h->GetNbinsX();
  int nbinsy = h->GetNbinsY();
  h->Scale( 1. / h->Integral( 0, (nbinsx+1), 0, (nbinsy+1) ) );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, float useMax=-999) 
{
  return drawEffHisto(getHisto(f,hn),opt,col,useMax);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHistoNorm2D(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm2D(getHisto2D(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

TF1 *getLinearFit( TH1 *h, int col=kMagenta+1, int sty=3 ) {
  TF1 *fn = new TF1("fn","pol0",10,500);
  h->Fit("fn","Q0");
  fn->SetLineColor(col);
  fn->SetLineStyle(sty);
  return fn;
}


TH1 *drawOnset1D( TH1 *h1, TH1 *h2, TString opt, int col=kBlue, float useMax=-999 ) {
  static int nFn(-1); nFn++;
  static const double SystError = 0.005;    // Don't trust very small errors! (0.005 gives reasonable Chi2 values)
  
  static const int NgBins     = 50;   // Number of gamma bins total, excl. over- and underflow
  static const int NgBinDeca  = 10;   // Number of gamma bins per decade, starting at gMin
  double gMin = 10.0;          // Minimum gamma 1.0 (muon of 3 GeV has gamma=30)
  Double_t gBins[NgBins+3];
  for (int ig=0; ig < NgBins+3; ig++) {
    gBins[ig] = gMin * pow(10.0, double(ig)/double(NgBinDeca)) ;
  }
  TString hname = TString::Format("h_pHT_%d", nFn);
  TH1D *h_onset = new TH1D( hname, "; #gamma Factor; Probablility High Threshold", NgBins+1, gBins ); 
  
  for ( int ibin(0); ibin < h_onset->GetNbinsX()+2; ibin++ ) {
    double pHTel = h1->GetBinContent(ibin);
    double pHTmu = h2->GetBinContent(ibin);
    if ( h1->GetBinLowEdge(ibin) > 300000 ) continue; // get rid of high gamma weirdness 

    int Denom = 1;
    if( (pHTel>0.0) && (pHTmu>0.0)) Denom = 2;
    double pHT = (pHTel+pHTmu)/Denom;
    
    double errel = h1->GetBinError(ibin);
    double errmu = h2->GetBinError(ibin);
    double error = sqrt( sqr(errel) + sqr(errmu) )/Denom;
           error = sqrt( sqr(error) + sqr(SystError) );

    if (pHT <= 0.0) continue;
    h_onset->SetBinContent( ibin, pHT );
    h_onset->SetBinError( ibin, error );
  }
  return drawEffHisto( h_onset, opt, col, useMax );
}

TH1 *drawOnset1D( TFile *f1, TFile *f2, TString hn, TString opt, int col=kBlue, float useMax=-999 ) {
  return drawOnset1D( getHisto(f1,hn), getHisto(f2,hn), opt, col, useMax);
}

TF1 *getOnsetFit( TH1 *h, int igas, int ipart, int col=kMagenta+1, int sty=3) {
  static int nFn(-1); nFn++;
  TString fname = TString::Format("f1_pHT_%d", nFn);
  TF1 *fn = new TF1( fname, func_onset, 50, 1000000, 6 );
  
  fn->SetLineWidth(1);
  fn->SetLineColor(col);
  fn->SetLineStyle(sty);
  
  double fit_pars[2][3][6] = {
    // Xenon
     { {1.0, 4.0, 0.07, 0.14, 3.1, 0.27},    //BRL
       {1.0, 4.1, 0.08, 0.14, 3.1, 0.12},    //ECA
       {1.0, 3.6, 0.09, 0.19, 3.1, 0.14} },  //ECB
    // Argon
     { {1.0, 3.3, 0.09, 0.03, 2.9, 0.15},    //BRL
       {1.0, 3.4, 0.08, 0.04, 3.0, 0.13},    //ECA
       {0.0, 0.0, 0.00, 0.00, 0.0, 0.00} }   //ECB
  };

  fn->SetNpx(10000);
  for( int ipar(0); ipar < 6; ipar++ ) { fn->SetParameter( ipar, fit_pars[igas][ipart][ipar] ); }
  fn->FixParameter( 0, 1.0 );
  fn->SetParLimits( 1, 3.3, 6.0 );

  h->Fit( fname, "RM0" );
  return fn;
}

TGraph *drawpHTPlane( TH1 *h1, TH1 *h2, TString opt, int col=kBlue) {
  TGraph *tg = new TGraph();
  tg->SetTitle(";pHT(#mu); pHT(e)");
  tg->SetMarkerColor(col);
  tg->SetMarkerStyle(11);
  tg->SetMarkerSize(0.5);

  //TF1 *fn1 = getLinearFit(h1);
  //TF1 *fn2 = getLinearFit(h2);
      
  int ipt(0);
  int Nbins = h1->GetNbinsX()+1;
  for( int ibin(1); ibin < Nbins; ibin++ ) {
    //double occ = h1->GetBinCenter(ibin);
    //double pHT_el = fn1->Eval(occ);
    //double pHT_mu = fn2->Eval(occ);
    //double pHT_el_bin = h1->GetBinContent(ibin);
    //double pHT_mu_bin = h2->GetBinContent(ibin);
    //if ( (pHT_el_bin <= 0) || (pHT_mu_bin <= 0) ) continue;
    double pHT_el = h1->GetBinContent(ibin);
    double pHT_mu = h2->GetBinContent(ibin);
    if ( (pHT_el <= 0) || (pHT_mu <= 0) ) continue;
    tg->SetPoint( ipt, pHT_mu, pHT_el);
    ipt++;
  }

  tg->GetXaxis()->SetLimits(0.,0.3);    // along X
  tg->GetHistogram()->SetMaximum(0.50); // along 
  tg->GetHistogram()->SetMinimum(0.15); // Y 
  tg->Draw(opt);
  return tg;
}

TGaxis *getElecAxis( double ypos=-0.055 ) {
  double mEl = 0.000511;
  TGaxis *axis2 = new TGaxis(5000.0, ypos, 598000.0, ypos, 5000.0*mEl, 598000.0*mEl, 50510, "G");
  axis2->SetTitle("       Electron momentum [GeV] ");
  axis2->SetLabelFont(42);
  axis2->SetTitleFont(42);
  axis2->SetLabelSize(0.045);
  axis2->SetTitleSize(0.035);
  //axis2->SetLabelOffset(0.00005);
  axis2->SetTitleOffset(1.2);
  axis2->SetLabelOffset(-0.008);
  axis2->CenterTitle();
  return axis2;
}

TGaxis *getMuonAxis( double ypos=-0.055 ) {
  double mMu = 0.10599;
  TGaxis *axis1 = new TGaxis(35.0, ypos, 4000.0, ypos, 35.0*mMu, 4000.0*mMu, 50510, "G");
  axis1->SetTitle("Muon momentum [GeV]    ");
  axis1->SetLabelFont(42);
  axis1->SetTitleFont(42);
  axis1->SetLabelSize(0.045);
  axis1->SetTitleSize(0.035);
  axis1->SetTitleOffset(1.2);
  axis1->SetLabelOffset(-0.008);
  axis1->CenterTitle();
  return axis1;
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTypeLabels(TString head, int col=kOrange+1) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  TString binType = "All HT bits";
  if (head.Contains("_MBX")) binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.92,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.92, strawType, col);
    drawText( 0.14, 0.87,   binType, col);
  }
}

  
void drawMajorLabels(TString hname, int col=kBlack) 
{
    drawText(0.685,0.92,"#bf{#it{ATLAS}} Work In Progress",col);
    TString local = "";
    if (hname.Contains("BRL")) local = "Barrel";
    if (hname.Contains("ECA")) local = "Endcap Type A";
    if (hname.Contains("ECB")) local = "Endcap Type B";
   
    TString side = ", Side A+C";
    if (hname.Contains("_sA")) side = ", Side A";
    if (hname.Contains("_sC")) side = ", Side C";
    if (local != "") local += side;
    drawText(0.685,0.87,local,col);
}

void drawMajorLabels2(TString hname, int col=kBlack) 
{
    drawText(0.685,0.22,"#bf{#it{ATLAS}} Work In Progress",col);
    TString local = "";
    if (hname.Contains("BRL")) local = "Barrel";
    if (hname.Contains("ECA")) local = "Endcap Type A";
    if (hname.Contains("ECB")) local = "Endcap Type B";
   
    TString side = ", Side A+C";
    if (hname.Contains("_sA")) side = ", Side A";
    if (hname.Contains("_sC")) side = ", Side C";
    if (local != "") local += side;
    drawText(0.685,0.17,local,col);
}

void drawTypeLabels2(TString head, int col=kOrange+1) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  TString binType = "All HT bits";
  if (head.Contains("_MBX")) binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.82,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.27, strawType, col);
    drawText( 0.14, 0.22,   binType, col);
  }
}

double integrate(TFile *f, TString hn )
{ 
  TH1 *h =  getHisto(f,hn); 
  double sum = h->Integral(); 
  return sum; 
} 


///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString  inFN("outputZee_500k/hist-Zee.root");
  TString inFN2("outputZmm_500k/hist-Zmumu.root"), pdf("plots.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile")  inFN=argv[++i];
    if (arg=="--inputFile2") inFN2=argv[++i];
    if (arg=="-in")  inFN=argv[++i];
    if (arg=="-in1")  inFN=argv[++i];
    if (arg=="-in2") inFN2=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }

  printf("\n  Running plotRecoEff\n");
  printf("  input file Z(ee): %s\n",inFN.Data());
  printf("  input file Z(mm): %s\n",inFN.Data());
  TFile *f = openFile(inFN);   // Z->ee
  TFile *f2 = openFile(inFN2); // Z->mm

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.02); can->SetRightMargin(0.04);
  can->Print(pdf+"[");

  // ----------------------------------
  // Fit Turn On Curve
  // ----------------------------------
  can->SetGridy(1);
  can->SetGridx(1);
  int cols[5] = { kViolet-5, kBlue, kGreen+2, kOrange+1, kRed };
  TString parts[3] = {"_BRL", "_ECA", "_ECB"};
  
  static const double m_el = 0.511;
  static const double m_mu = 105.6;
  //static const double m_pi = 139.6;
  
  static const int NgBins     = 50;   // Number of gamma bins total, excl. over- and underflow
  static const int NgBinDeca  = 10;   // Number of gamma bins per decade, starting at gMin
  double gMin = 10.0;          // Minimum gamma 1.0 (muon of 3 GeV has gamma=30)
  Double_t gBins[NgBins+3];
  for (int ig=0; ig < NgBins+3; ig++) {
    gBins[ig] = gMin * pow(10.0, double(ig)/double(NgBinDeca)) ;
  }
  TH1D *h_onset[3][5];
  
  for( int ipart(0); ipart < 3; ipart++ ) {
    TString part = parts[ipart];

    TH1 *h_onset, *h_elpred[5];
    TF1 *fit[5];
    for( int iOB(0); iOB < 5; iOB++ ){
      TString opt = "SAME";
      if (iOB==0) opt = "";

      TString hname = TString::Format( "h_fHT_Xe_OB%d", iOB );
              hname += part+"_GF";
      //h_onset[ipart][iOB] = new TH1D( hname, "; #gamma Factor; Probablility High Threshold", NgBins+1, gBins ); 
      h_onset = drawOnset1D( f, f2, hname, opt, cols[iOB], 0.55);
      fit[iOB] = getOnsetFit( h_onset, 0, ipart, cols[iOB] ); // Xenon == 0
      //fit[iOB]->Draw("SAME");
     
      h_elpred[iOB] = (TH1*)h_onset->Clone();
      h_elpred[iOB]->Reset("M");
      h_elpred[iOB]->SetLineColor(cols[iOB]);
      h_elpred[iOB]->SetMarkerColor(cols[iOB]);
      h_elpred[iOB]->SetMarkerSize(1.4);
      h_elpred[iOB]->SetMarkerStyle(24);

      double   pHTm0 = fit[0]->GetParameter(2);
      double pHTmOcc = fit[iOB]->GetParameter(2);
      for( int ibin(1); ibin < 50; ibin++ ) {
        double gam = h_onset->GetBinCenter(ibin);
        if (gam < 500.0) continue;
        if (gam > 2.0e5) break;
        
        double   pHTe0 = fit[0]->Eval(gam);
        double pHTeOcc = pHTe0 + ( 1 - pHTe0 )*( pHTmOcc - pHTm0 );
        h_elpred[iOB]->Fill( gam, pHTeOcc );
      }
      h_elpred[iOB]->Draw("HISTP SAME");

    }

    // predict points for electron plateau at Occupancy (30-40%)
    /*TH1 *h_elpred = (TH1*)h_onset->Clone();
    h_elpred->Reset("M");
    h_elpred->SetLineColor(kBlack);
    h_elpred->SetMarkerColor(kBlack);
    h_elpred->SetMarkerSize(1.4);
    //h_elpred->SetMarkerStyle(24);
    h_elpred->SetMarkerStyle(29);

    double   pHTm0 = fit[0]->GetParameter(2);
    double pHTmOcc = fit[4]->GetParameter(2);
    for( int ibin(1); ibin < 50; ibin++ ) {
      double gam = h_onset->GetBinCenter(ibin);
      if (gam < 500.0) continue;
      if (gam > 2.0e5) break;
      
      double   pHTe0 = fit[0]->Eval(gam);
      double pHTeOcc = pHTe0 + ( 1 - pHTe0 )*( pHTmOcc - pHTm0 );
      h_elpred->Fill( gam, pHTeOcc );
    }*/
    
    can->SetLogx(1);
    //h_elpred->Draw("HISTP SAME");
    drawMajorLabels(part+"_Xe_MBX",kBlack); 
    drawTypeLabels( "_Xe_MBX"+part, kBlack );
    drawText( 0.14, 0.82, "Occupancy 40%-50%", cols[4]);
    drawText( 0.14, 0.77, "Occupancy 30%-40%", cols[3]);
    drawText( 0.14, 0.72, "Occupancy 20%-30%", cols[2]);
    drawText( 0.14, 0.67, "Occupancy 10%-20%", cols[1]);
    drawText( 0.14, 0.62, "Occupancy  0%-10%", cols[0]);
    can->Print(pdf);

    //Loop through fit parameters and plot dependence
    for( int ipar(1); ipar < 6; ipar++ ) {
      TGraphErrors *tg = new TGraphErrors();
      for( int iocc(0); iocc < 5; iocc++ ) {
        double val = fit[iocc]->GetParameter(ipar);
        double occ = 0.05 + (0.10*iocc);
        double valerr = fit[iocc]->GetParError(ipar);
        tg->SetPoint(iocc, occ, val);
        tg->SetPointError(iocc, 0.0, valerr );
      }
      TString title = TString::Format("; Track Occupancy; %s", fit[0]->GetParName(ipar) );
      tg->SetTitle(title);
      tg->SetMarkerStyle(20);

      can->SetLogx(0);
      tg->SetMinimum(0.0); 
      tg->GetXaxis()->SetRangeUser(0.5, 0.98);
      tg->Draw("APE");

      TString fitfn = "pol0";
      if (ipar == 2) fitfn = "pol2";
      if (ipar == 3) fitfn = "pol2";

      TString parname;
      TF1 *fn = new TF1("fn",fitfn,0,1);
      tg->Fit("fn","RM");

      fitfn = "Fit Function : "+fitfn;
      drawMajorLabels2(part+"_Xe_MBX",kBlack); 
      drawTypeLabels2( "_Xe_MBX"+part, kBlack );
      drawText( 0.14, 0.17, fitfn, kBlack);
      
      //can->Print(pdf);

      
    }
  
  }


  can->SetGridy(0);
  can->SetGridx(0);
  
  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f->Close();


}


