#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TStyle.h>
#include <TGaxis.h>
#include <TGraphAsymmErrors.h>
#include "TH3F.h"

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.035 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  h->GetYaxis()->SetTitleOffset(1.4);
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->SetBinContent( 0, 0 );
  h->SetBinContent( nbins, 0);
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHistoNorm2D(TH2 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetZaxis()->SetTitle("Fraction of Events / Bin");
  int nbinsx = h->GetNbinsX();
  int nbinsy = h->GetNbinsY();
  h->Scale( 1. / h->Integral( 0, (nbinsx+1), 0, (nbinsy+1) ) );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, float useMax=-999) 
{
  return drawEffHisto(getHisto(f,hn),opt,col,useMax);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHistoNorm2D(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm2D(getHisto2D(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

TF1 *getLinearFit( TH1 *h, int col=kMagenta+1, int sty=3 ) {
  TF1 *fn = new TF1("fn","pol3",0,1);
  h->Fit("fn","Q0");
  fn->SetLineColor(col);
  fn->SetLineStyle(sty);
  return fn;
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTypeLabels(TString head, int col=kBlack) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  //TString binType = "All HT bits";
  //if (head.Contains("_MBX")) binType = "Middle HT bit";
  TString binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.92,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.92, strawType, col);
    drawText( 0.14, 0.87,   binType, col);
  }
}

  
void drawMajorLabels(TString hname, int iOccBin=-1, int col=kBlack) 
{
  drawText(0.55,0.92,"#bf{#it{ATLAS}} Work In Progress",col);
  TString local = "";
  if (hname.Contains("BRL")) local = "Barrel";
  if (hname.Contains("ECA")) local = "Endcap Type A";
  if (hname.Contains("ECB")) local = "Endcap Type B";
      
  TString occLabel = "All Occupancy";
  if (iOccBin == 0) occLabel = "Occupancy  0% - 20%";
  if (iOccBin == 1) occLabel = "Occupancy 20% - 40%";
  if (iOccBin == 2) occLabel = "Occupancy #geq 40%";
   
  //TString side = ", Side A+C";
  //if (hname.Contains("_sA")) side = ", Side A";
  //if (hname.Contains("_sC")) side = ", Side C";
  //if (local != "") local += side;
  drawText(0.55,0.87,local,col);
  drawText(0.55,0.82,occLabel,col);
}

void drawFuncLabel(TString func, int col=kBlack)
{
  TString desc = "MC15 Data";
  if (func == "f1") desc = "MC15 Fit";
  if (func == "f2") desc = "Data12 + MC15 Fit";
  drawText(0.14, 0.82, desc, col);
}

void drawLabels() {
  drawText( 0.14, 0.92, "Fraction HTMB", kBlue  );
  drawText( 0.14, 0.87,   "RootCore LH", kBlack );
  drawText( 0.14, 0.82,     "Athena LH", kRed   );
}

TGraph *drawROCcurve( TFile *f1, TFile *f2, TString hn, int etaBin5, int zmin=0, int zmax=-1, TString opt="AL", int col=kBlack)
{
  TH3 *h1 = (TH3*)f1->Get(hn); 
  TH3 *h2 = (TH3*)f2->Get(hn);
  int nBinsY = h1->GetYaxis()->GetNbins()+1;
  if (zmax < 0) zmax = h1->GetZaxis()->GetNbins()+1;

  TGraph *tg = new TGraph();
  double sum1 = h1->Integral(etaBin5,etaBin5, 0, nBinsY, zmin, zmax);
  double sum2 = h2->Integral(etaBin5,etaBin5, 0, nBinsY, zmin, zmax);
  
  for( int ibin(0); ibin < nBinsY; ibin++ ) {
    double effEl = h1->Integral(etaBin5,etaBin5, ibin, nBinsY, zmin, zmax)/sum1;
    double effMu = h2->Integral(etaBin5,etaBin5, ibin, nBinsY, zmin, zmax)/sum2;
    tg->SetPoint( ibin, effEl, effMu );
    //double LHcut = h1->GetGetBinLowEdge(etaBin5, etaBin5, ibin); 
    //if ((effEl > 0.89) && (effEl < 0.91)) printf(" %6.4f    %6.4f   %6.4f", LHcut, effEl, effMu );
  }
 
  tg->SetTitle("; Electron Efficiency; Muon Efficiency");
  tg->SetMinimum(0.0); tg->SetMaximum(0.48);
  tg->GetXaxis()->SetRangeUser(0.5, 0.98);
  //tg->GetXaxis()->SetNdivisions( 512, kFALSE );
  
  tg->SetLineColor(col);
  tg->SetMarkerColor(col);

  tg->Draw(opt);
  return tg;
}

///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString  inFN("outputZee_500k/hist-Zee.root");
  TString inFN2("outputZmm_500k/hist-Zmumu.root"), pdf("plot_2DSurf.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile")  inFN=argv[++i];
    if (arg=="--inputFile2") inFN2=argv[++i];
    if (arg=="-in")  inFN=argv[++i];
    if (arg=="-in1")  inFN=argv[++i];
    if (arg=="-in2") inFN2=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }
  //gStyle->SetNdivisions(100,"X"); 

  printf("\n  Running plotRecoEff\n");
  printf("  input file Z(ee): %s\n", inFN.Data());
  printf("  input file Z(mm): %s\n",inFN2.Data());
  TFile *f1 = openFile(inFN);   // Z->ee
  TFile *f2 = openFile(inFN2); // Z->mm

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.02); can->SetRightMargin(0.04);
  can->Print(pdf+"[");
  can->SetGridy(1);
  can->SetGridx(1);

  //can->SetRightMargin(0.12); can->SetLogx(1);
  for( TString part: {"_BRL", "_ECA", "_ECB"} ) {
    for( int iOccBin(-1); iOccBin < 3; iOccBin++ ) {
      int etaBin5 = 1;
      if (part == "_ECA") etaBin5 = 3;
      if (part == "_ECB") etaBin5 = 5;

      //iOccBin = -1 is for all occupancies.
      double OccBMin(0), OccBMax(50);
      if( iOccBin >= 0) {
        OccBMin = iOccBin*10 + 1;
        OccBMax = OccBMin + 10;
        if (iOccBin == 2) OccBMax = 50;
      }

      //comparing all
      drawROCcurve( f1, f2, "h3_LHNew_CF0_OT",  etaBin5,  OccBMin, OccBMax, "APL",  kRed  );
      drawROCcurve( f1, f2, "h3_LHNew_CF1_OT",  etaBin5,  OccBMin, OccBMax,  "PL",  kOrange+7 );
      drawROCcurve( f1, f2, "h3_LHNew_CF2_OT",  etaBin5,  OccBMin, OccBMax,  "PL",  kOrange+1 );
      drawROCcurve( f1, f2, "h3_LHNew_CF3_OT",  etaBin5,  OccBMin, OccBMax,  "PL",  kYellow+1 );
      drawROCcurve( f1, f2, "h3_LHNew_CF4_OT",  etaBin5,  OccBMin, OccBMax,  "PL",  kGreen+2 );
      drawROCcurve( f1, f2, "h3_LHNew_CF5_OT",  etaBin5,  OccBMin, OccBMax,  "PL",  kBlue+1 );
      drawROCcurve( f1, f2, "h3_LHNew_CF6_OT",  etaBin5,  OccBMin, OccBMax,  "PL",  kCyan+1 );
      drawROCcurve( f1, f2, "h3_LHNew_CF7_OT",  etaBin5,  OccBMin, OccBMax,  "PL",  kViolet );

      //drawROCcurve( f1, f2,     "h3_LH_OT", etaBin5,  OccBMin, OccBMax,  "APL",  kRed  );
      //drawROCcurve( f1, f2,    "h3_LHT_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kCyan+1  );
      //drawROCcurve( f1, f2,   "h3_LHMC_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kOrange+8  );
      //drawROCcurve( f1, f2,   "h3_LHXe_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kRed+2  );
      //drawROCcurve( f1, f2,  "h3_fHTMB_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kBlack   );
      //drawROCcurve( f1, f2,    "h3_fHT_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kOrange   );
      //drawROCcurve( f1, f2,   "h3_LHFO_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kMagenta  );
      //drawROCcurve( f1, f2,   "h3_LH1D_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kBlue  );
      //drawROCcurve( f1, f2,   "h3_LHNO_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kGreen+1  );
      //drawROCcurve( f1, f2,  "h3_LHNew_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kViolet+1  );
      //drawROCcurve( f1, f2,  "h3_LHBad_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kGray+1  );
      //drawROCcurve( f1, f2,  "h3_LHBad_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kRed-1  );
      //drawROCcurve( f1, f2,  "h3_LHNew_noCF_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kBlue+2  );
      //drawROCcurve( f1, f2,  "h3_LHBad_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kRed-1  );
      //drawROCcurve( f1, f2,  "h3_LHMy_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kViolet+1  );
      
      drawMajorLabels(part,iOccBin);
      drawText( 0.14, 0.92, "No Corrections", kRed  );    
      drawText( 0.14, 0.87, "All ON", kOrange+7  );
      drawText( 0.14, 0.82, "SL ON", kOrange+1  );
      drawText( 0.14, 0.77, "TW ON", kYellow+1  );
      drawText( 0.14, 0.72, "ZR ON", kGreen+2  );
      drawText( 0.14, 0.67, "SL+TW ON", kBlue+1  );
      drawText( 0.14, 0.62, "SL+ZR ON", kCyan+1  );
      drawText( 0.14, 0.57, "TW+ZR ON", kViolet  );
      
      
      //drawText( 0.14, 0.92, "Fraction HTMB", kBlack  );
      //drawText( 0.14, 0.87, "Fraction HT", kOrange  );
      //drawText( 0.14, 0.82, "PID LH (2D, Fixed Occ = 0.1)", kMagenta   );
      //drawText( 0.14, 0.77, "PID LH (1D, No Occ Corr )", kGreen+1 );
      //drawText( 0.14, 0.72, "PID LH (1D, Occ. CF)", kBlue );
      //drawText( 0.14, 0.67, "PID LH (2D, Athena)", kCyan+1 );
      //drawText( 0.14, 0.62, "PID LH (2D, Rootcore)", kRed );
      //drawText( 0.14, 0.57, "PID LH (2D, Xenon Only)", kRed+2 );
      //drawText( 0.14, 0.52, "PID LH (2D, MC parameters)", kOrange+8 ); 
      //drawText( 0.14, 0.47, "PID LH (2D, New)", kViolet+1 );
      //drawText( 0.14, 0.42, "PID LH (2D, Bad)", kGray+1 );
     
      //data+MC vs MC fits
      /*drawROCcurve( f1, f2,     "h3_LH_OT", etaBin5,  OccBMin, OccBMax,  "APL",  kBlue  );
      drawROCcurve( f1, f2,  "h3_fHTMB_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kBlack   );
      drawROCcurve( f1, f2,   "h3_LHMC_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kRed  );
      drawMajorLabels(part,iOccBin);
      drawText( 0.14, 0.92, "Fraction HTMB", kBlack  );
      drawText( 0.14, 0.82, "PID LH (Fit A)", kRed );
      drawText( 0.14, 0.87, "PID LH (Fit B)", kBlue   );*/
      
      //rootcore vs. athena
      /*drawText( 0.14, 0.92, "Fraction HTMB", kBlack  );
      drawROCcurve( f1, f2,     "h3_LH_OT", etaBin5,  OccBMin, OccBMax,  "APL",  kBlue  );
      drawROCcurve( f1, f2,  "h3_fHTMB_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kBlack   );
      drawROCcurve( f1, f2,    "h3_LHT_OT", etaBin5,  OccBMin, OccBMax,   "PL",  kRed  );
      drawMajorLabels(part,iOccBin);
      drawText( 0.14, 0.92, "Fraction HTMB", kBlack  );
      drawText( 0.14, 0.87, "PID LH (Athena)", kRed   );
      drawText( 0.14, 0.82, "PID LH (Rootcore)", kBlue );*/
      
      //drawLabels();
      can->Print(pdf);
    }
  }

  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f1->Close();
  f2->Close();


}


