#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>
#include <TGraphAsymmErrors.h>

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.015 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  h->GetYaxis()->SetTitleOffset(1.4);
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->SetBinContent( 0, 0 );
  h->SetBinContent( nbins, 0);
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHistoNorm2D(TH2 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetZaxis()->SetTitle("Fraction of Events / Bin");
  int nbinsx = h->GetNbinsX();
  int nbinsy = h->GetNbinsY();
  h->Scale( 1. / h->Integral( 0, (nbinsx+1), 0, (nbinsy+1) ) );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, float useMax=-999) 
{
  return drawEffHisto(getHisto(f,hn),opt,col,useMax);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHistoNorm2D(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm2D(getHisto2D(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

TF1 *getLinearFit( TH1 *h, int col=kMagenta+1, int sty=3 ) {
  TF1 *fn = new TF1("fn","pol3",0,1);
  h->Fit("fn","Q0");
  fn->SetLineColor(col);
  fn->SetLineStyle(sty);
  return fn;
}

TGaxis *getElecAxis( double ypos=-0.055 ) {
  double mEl = 0.000511;
  TGaxis *axis2 = new TGaxis(5000.0, ypos, 598000.0, ypos, 5000.0*mEl, 598000.0*mEl, 50510, "G");
  axis2->SetTitle("       Electron momentum [GeV] ");
  axis2->SetLabelFont(42);
  axis2->SetTitleFont(42);
  axis2->SetLabelSize(0.045);
  axis2->SetTitleSize(0.035);
  //axis2->SetLabelOffset(0.00005);
  axis2->SetTitleOffset(1.2);
  axis2->SetLabelOffset(-0.008);
  axis2->CenterTitle();
  return axis2;
}

TGaxis *getMuonAxis( double ypos=-0.055 ) {
  double mMu = 0.10599;
  TGaxis *axis1 = new TGaxis(35.0, ypos, 4000.0, ypos, 35.0*mMu, 4000.0*mMu, 50510, "G");
  axis1->SetTitle("Muon momentum [GeV]    ");
  axis1->SetLabelFont(42);
  axis1->SetTitleFont(42);
  axis1->SetLabelSize(0.045);
  axis1->SetTitleSize(0.035);
  axis1->SetTitleOffset(1.2);
  axis1->SetLabelOffset(-0.008);
  axis1->CenterTitle();
  return axis1;
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTypeLabels(TString head, int col=kBlack) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  //TString binType = "All HT bits";
  //if (head.Contains("_MBX")) binType = "Middle HT bit";
  TString binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.92,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.92, strawType, col);
    drawText( 0.14, 0.87,   binType, col);
  }
}

  
void drawMajorLabels(TString hname, int col=kBlack) 
{
    drawText(0.60,0.92,"#bf{#it{ATLAS}} Work In Progress",col);
    TString local = "";
    if (hname.Contains("BRL")) local = "Barrel";
    if (hname.Contains("ECA")) local = "Endcap Type A";
    if (hname.Contains("ECB")) local = "Endcap Type B";
   
    TString side = ", Side A+C";
    if (hname.Contains("_sA")) side = ", Side A";
    if (hname.Contains("_sC")) side = ", Side C";
    if (local != "") local += side;
    drawText(0.60,0.87,local,col);
}

TH2 *get2DpHThist( TFile *f, TFile *f2, TString hname )
{
  TH2 *h2_nHT_ee = getHisto2D(  f, "h2_nHTMB"+hname ); 
  TH2 *h2_nLT_ee = getHisto2D(  f,   "h2_nLT"+hname );
  TH2 *h2_nHT_mm = getHisto2D( f2, "h2_nHTMB"+hname ); 
  TH2 *h2_nLT_mm = getHisto2D( f2,   "h2_nLT"+hname );

  TH2 *h2_nHT = (TH2*)h2_nHT_ee->Clone();
  TH2 *h2_nLT = (TH2*)h2_nLT_ee->Clone();
  h2_nHT->Add( h2_nHT_mm );
  h2_nLT->Add( h2_nLT_mm );

  TH2 *h2_pHT = (TH2*)h2_nHT->Clone();
  h2_pHT->Divide( h2_nLT );
  h2_pHT->SetMaximum(0.32);

  return h2_pHT;
}

TH2 *draw2DpHT( TFile *f, TFile *f2, TString hn, TString opt="colz" )
{
  return drawHisto2D( get2DpHThist(f,f2,hn), opt );
}

TH1 *getSliceX( TH2 *h2, int ymin=0, int ymax=-1 )
{
  int NbinsX = h2->GetNbinsX();
  if (ymax < 0) ymax = h2->GetNbinsY()+1;
  
  TH1 *h = (TH1*)h2->ProjectionX( "" );
  h->Reset("ICESM");
  //h->Sumw2(1);
  
  for( int xbin(0); xbin <= NbinsX+1; xbin++ ) {
    double avg(0), err(0);
    for( int ybin = ymin; ybin <= ymax; ybin++ ) { 
      avg += h2->GetBinContent( xbin, ybin ); 
      double dx = h2->GetBinError( xbin, ybin );
      err += (dx*dx);
    }
    avg /= (ymax-ymin);
    err = sqrt(err)/(ymax-ymin);
    h->SetBinContent(xbin, avg);
    h->SetBinError(xbin, err);
    //std::cout << "avg = " << avg << std::endl;
  }

  return h;
}

TH1 *getSliceY( TH2 *h2, int xmin=0, int xmax=-1 )
{
  int NbinsY = h2->GetNbinsY();
  if (xmax < 0) xmax = h2->GetNbinsX()+1;
  
  TH1 *h = (TH1*)h2->ProjectionY( "" );
  h->Reset("ICESM");
  //h->Sumw2(1);
  
  for( int ybin(0); ybin <= NbinsY+1; ybin++ ) {
    double avg(0), err(0);
    for( int xbin = xmin; xbin <= xmax; xbin++ ) { 
      avg += h2->GetBinContent( xbin, ybin ); 
      double dx = h2->GetBinError( xbin, ybin );
      err += (dx*dx);
    }
    avg /= (xmax-xmin);
    err = sqrt(err)/(xmax-xmin);
    h->SetBinContent(ybin, avg);
    h->SetBinError(ybin, err);
    //std::cout << "avg = " << avg << std::endl;
  }

  return h;
}

TH1 *drawSliceX( TH2 *h2, TString opt, int col=kBlack, int ymin=0, int ymax=-1, double useMax=-999 )
{
  return drawEffHisto( getSliceX(h2,ymin,ymax), opt, col, useMax);
}

TH1 *drawSliceY( TH2 *h2, TString opt, int col=kBlack, int xmin=0, int xmax=-1, double useMax=-999 )
{
  return drawEffHisto( getSliceY(h2,xmin,xmax), opt, col, useMax);
}

double integrate(TFile *f, TString hn )
{ 
  TH1 *h =  getHisto(f,hn); 
  double sum = h->Integral(); 
  return sum; 
} 


///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString  inFN("outputZee_500k/hist-Zee.root");
  TString inFN2("outputZmm_500k/hist-Zmumu.root"), pdf("plots.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile")  inFN=argv[++i];
    if (arg=="--inputFile2") inFN2=argv[++i];
    if (arg=="-in")  inFN=argv[++i];
    if (arg=="-in1")  inFN=argv[++i];
    if (arg=="-in2") inFN2=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }

  printf("\n  Running plotRecoEff\n");
  printf("  input file Z(ee): %s\n", inFN.Data());
  printf("  input file Z(mm): %s\n",inFN2.Data());
  TFile *f1 = openFile(inFN);   // Z->ee
  TFile *f2 = openFile(inFN2); // Z->mm

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.02); can->SetRightMargin(0.04);
  can->Print(pdf+"[");

  can->SetRightMargin(0.12); can->SetLogx(1);
  for( TString gasType: {"_Xe", "_Ar"} ) {
    for( TString part: {"_BRL", "_ECA", "_ECB"} ) {
      if ((gasType == "_Ar") && (part == "_ECB")) continue;

      // Draw 2D onset
      can->SetRightMargin(0.12); can->SetLogx(1);
      TString hn = gasType+part+"_GF_OT";
      TH2 *h2_pHT = draw2DpHT( f1, f2, hn );

      drawMajorLabels( hn );
      drawTypeLabels( hn );
      can->Print(pdf);

      //int cols[10] = {kMagenta+2, kMagenta, kBlue+1, kBlue, kAzure+1, kGreen+2, kGreen, kOrange-2, kOrange+6, kRed};
      int cols[6] = {kMagenta+2, kBlue+1, kGreen+2, kOrange-2, kOrange+6, kRed};
      
      //Draw projections (along X)
      can->SetRightMargin(0.04); can->SetLogx(1);
      TH1 *h_projX[6];
      for( int i=0; i < 6; i++ ) {
        TH1 *htemp = drawSliceX( h2_pHT, "", cols[i], 1+i*5, 5+i*5, 0.45);
        h_projX[i] = (TH1*)htemp->Clone();
      }

      for( int i=0; i < 6; i++ ) {
        if (i%2 != 0) continue;
        TString opt = ( (i==0) ? "" : "same" );
        h_projX[i]->Draw(opt);
      }
      
      drawMajorLabels( hn );
      drawTypeLabels( hn );
      can->Print(pdf);

      //Draw projections (along Y)
      can->SetRightMargin(0.04); can->SetLogx(0);
      
      /*TH1 *h_projY[6];
      for( int i=0; i < 6; i++ ) {
        TH1 *htemp = drawSliceY( h2_pHT, "", cols[i], 1+i*5, 5+i*5, 0.45);
        h_projY[i] = (TH1*)htemp->Clone();
      }
      drawMajorLabels( hn );
      drawTypeLabels( hn );
      can->Print(pdf);*/

    }
  }

  can->SetGridy(1);
  can->SetGridx(1);

  can->SetGridy(0);
  can->SetGridx(0);
  
  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f1->Close();
  f2->Close();


}


