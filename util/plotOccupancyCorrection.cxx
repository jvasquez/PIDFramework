#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>
#include <TGraphAsymmErrors.h>

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

double sqr(double a) {return a*a;}

double func_onset(double *x, double *par) {
  if (x[0] < 0.0) return 0.0;

  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par[4])/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par[4])/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par[1] - par[4])/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par[1]);
  
  if (log10(x[0])      < par[0]) return pHT_dEdx;
  else if (log10(x[0]) > par[1]) return pHT_HG;
  else                           return pHT_TR;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.025 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  h->GetYaxis()->SetTitleOffset(1.4);
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->SetBinContent( 0, 0 );
  h->SetBinContent( nbins, 0);
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHistoNorm2D(TH2 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetZaxis()->SetTitle("Fraction of Events / Bin");
  int nbinsx = h->GetNbinsX();
  int nbinsy = h->GetNbinsY();
  h->Scale( 1. / h->Integral( 0, (nbinsx+1), 0, (nbinsy+1) ) );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, float useMax=-999) 
{
  return drawEffHisto(getHisto(f,hn),opt,col,useMax);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHistoNorm2D(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm2D(getHisto2D(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

TF1 *getLinearFit( TH1 *h, int col=kMagenta+1, int sty=3 ) {
  TF1 *fn = new TF1("fn","pol3",0,1);
  h->Fit("fn","Q0");
  fn->SetLineColor(col);
  fn->SetLineStyle(sty);
  return fn;
}

TGraph *drawpHTPlane( TH1 *h1, TH1 *h2, TString opt, int col=kBlue) {
  TGraph *tg = new TGraph();
  tg->SetTitle(";pHT(#mu); pHT(e)");
  tg->SetMarkerColor(col);
  tg->SetMarkerStyle(11);
  tg->SetMarkerSize(0.5);

  //TF1 *fn1 = getLinearFit(h1);
  //TF1 *fn2 = getLinearFit(h2);
      
  int ipt(0);
  int Nbins = h1->GetNbinsX()+1;
  for( int ibin(1); ibin < Nbins; ibin++ ) {
    //double occ = h1->GetBinCenter(ibin);
    //double pHT_el = fn1->Eval(occ);
    //double pHT_mu = fn2->Eval(occ);
    //double pHT_el_bin = h1->GetBinContent(ibin);
    //double pHT_mu_bin = h2->GetBinContent(ibin);
    //if ( (pHT_el_bin <= 0) || (pHT_mu_bin <= 0) ) continue;
    double pHT_el = h1->GetBinContent(ibin);
    double pHT_mu = h2->GetBinContent(ibin);
    if ( (pHT_el <= 0) || (pHT_mu <= 0) ) continue;
    tg->SetPoint( ipt, pHT_mu, pHT_el);
    ipt++;
  }

  tg->GetXaxis()->SetLimits(0.,0.3);    // along X
  tg->GetHistogram()->SetMaximum(0.50); // along 
  tg->GetHistogram()->SetMinimum(0.15); // Y 
  tg->Draw(opt);
  return tg;
}

TGaxis *getElecAxis( double ypos=-0.055 ) {
  double mEl = 0.000511;
  TGaxis *axis2 = new TGaxis(5000.0, ypos, 598000.0, ypos, 5000.0*mEl, 598000.0*mEl, 50510, "G");
  axis2->SetTitle("       Electron momentum [GeV] ");
  axis2->SetLabelFont(42);
  axis2->SetTitleFont(42);
  axis2->SetLabelSize(0.045);
  axis2->SetTitleSize(0.035);
  //axis2->SetLabelOffset(0.00005);
  axis2->SetTitleOffset(1.2);
  axis2->SetLabelOffset(-0.008);
  axis2->CenterTitle();
  return axis2;
}

TGaxis *getMuonAxis( double ypos=-0.055 ) {
  double mMu = 0.10599;
  TGaxis *axis1 = new TGaxis(35.0, ypos, 4000.0, ypos, 35.0*mMu, 4000.0*mMu, 50510, "G");
  axis1->SetTitle("Muon momentum [GeV]    ");
  axis1->SetLabelFont(42);
  axis1->SetTitleFont(42);
  axis1->SetLabelSize(0.045);
  axis1->SetTitleSize(0.035);
  axis1->SetTitleOffset(1.2);
  axis1->SetLabelOffset(-0.008);
  axis1->CenterTitle();
  return axis1;
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTypeLabels(TString head, int col=kOrange+1) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  TString binType = "All HT bits";
  if (head.Contains("_MBX")) binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.92,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.92, strawType, col);
    drawText( 0.14, 0.87,   binType, col);
  }
}

  
void drawMajorLabels(TString hname, int col=kBlack) 
{
    drawText(0.685,0.92,"#bf{#it{ATLAS}} Work In Progress",col);
    TString local = "";
    if (hname.Contains("BRL")) local = "Barrel";
    if (hname.Contains("ECA")) local = "Endcap Type A";
    if (hname.Contains("ECB")) local = "Endcap Type B";
   
    TString side = ", Side A+C";
    if (hname.Contains("_sA")) side = ", Side A";
    if (hname.Contains("_sC")) side = ", Side C";
    if (local != "") local += side;
    drawText(0.685,0.87,local,col);
}


double integrate(TFile *f, TString hn )
{ 
  TH1 *h =  getHisto(f,hn); 
  double sum = h->Integral(); 
  return sum; 
} 


///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString  inFN("outputZee/hist-Zee.root");
  TString inFN2("outputZmm/hist-Zmumu.root"), pdf("plots.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile")  inFN=argv[++i];
    if (arg=="--inputFile2") inFN2=argv[++i];
    if (arg=="-in")  inFN=argv[++i];
    if (arg=="-in1")  inFN=argv[++i];
    if (arg=="-in2") inFN2=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }

  printf("\n  Running plotRecoEff\n");
  printf("  input file Z(ee): %s\n",inFN.Data());
  printf("  input file Z(mm): %s\n",inFN.Data());
  TFile *f = openFile(inFN);   // Z->ee
  TFile *f2 = openFile(inFN2); // Z->mm

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.02); can->SetRightMargin(0.04);
  can->Print(pdf+"[");

  // ----------------------------------
  // Plot global variables
  // ----------------------------------
  //for( TString hists: {"h_averageMu", "h_OccGlobal", "h_avgmu_OG", "h_massZll"} ) {
  /*for( TString hists: {"h_averageMu", "h_OccGlobal", "h_massZll"} ) {
    drawHistoNorm( f, hists     ,"",kRed);
    drawHistoNorm(f2, hists ,"same",kBlue); 
    drawMajorLabels("",kBlack);
    drawText(0.55,0.92,"Z#rightarrowee MC", kRed);
    drawText(0.55,0.87,"Z#rightarrow#mu#mu MC", kBlue);
    can->Print(pdf);
  }

  for (TString hists: {"h_avgmu_OG"} ) {
    drawHisto( f, hists     ,"",kRed);
    drawHisto(f2, hists ,"same",kBlue); 
    drawMajorLabels("",kBlack);
    drawText(0.55,0.92,"Z#rightarrowee MC", kRed);
    drawText(0.55,0.87,"Z#rightarrow#mu#mu MC", kBlue);
    can->Print(pdf);
  }

  
  // ----------------------------------
  // Plot local variables
  // ----------------------------------
  for( TString hists: {"h_OccBRL", "h_OccECA", "h_OccECB"} ) {
    for( TString side: {"_sA", "_sC"} ) {
      drawHistoNorm( f, hists+side ,    "",kRed);
      drawHistoNorm(f2, hists+side ,"same",kBlue);
      drawMajorLabels(hists+side,kBlack);
      drawText(0.55,0.92,"Z#rightarrowee MC", kRed);
      drawText(0.55,0.87,"Z#rightarrow#mu#mu MC", kBlue);
      can->Print(pdf);
    }
  }*/
  
  // ----------------------------------
  // Fit Turn On Curve
  // ----------------------------------
  can->SetGridy(1);
  can->SetGridx(1);
  
  static const double m_el = 0.511;
  static const double m_mu = 105.6;
  //static const double m_pi = 139.6;

  TH1 *h_el0[3], *h_el1[3], *h_el3[3];
  TH1 *h_mu0[3], *h_mu1[3], *h_mu3[3];
  TH1 *h_elpred[3];
  TH1 *h_elavg[3], *h_muavg[3];

  TF1 *f_el0[3], *f_el1[3], *f_el3[3];
  TF1 *f_mu0[3], *f_mu1[3], *f_mu3[3];
  TF1 *f_elavg[3], *f_muavg[3];

  double corr_mu0[3] = { 0.60, 0.68, 1.00};
  double corr_el0[3] = { 0.90, 0.92, 1.00};
  
  double corr_mu1[3] = { 0.74, 0.79, 1.00};
  double corr_el1[3] = { 0.92, 0.95, 1.00};
  
  double corr_mu3[3] = { 1.01, 1.01, 1.00};
  double corr_el3[3] = { 1.04, 1.05, 1.00};
  
  int cols[5] = { kViolet-5, kBlue, kGreen+2, kOrange+1, kRed };

  TString parts[3] = {"_BRL","_ECA","_ECB"};
  for ( TString bit: { "_MBX" } ) {
    for ( int ip(0); ip < 3; ip++ ) {
      TString part = parts[ip];

      can->SetLogx(1);
      can->SetBottomMargin(0.18);

      h_el0[ip] = drawEffHisto(  f, "h_fHT_Xe_OB0"+bit+part+"_GF", "", cols[0] );
      h_el1[ip] = drawEffHisto(  f, "h_fHT_Xe_OB1"+bit+part+"_GF", "", cols[1] );
      h_el3[ip] = drawEffHisto(  f, "h_fHT_Xe_OB3"+bit+part+"_GF", "", cols[3] );
      
      h_mu0[ip] = drawEffHisto( f2, "h_fHT_Xe_OB0"+bit+part+"_GF", "", cols[0] );
      h_mu1[ip] = drawEffHisto( f2, "h_fHT_Xe_OB1"+bit+part+"_GF", "", cols[1] );
      h_mu3[ip] = drawEffHisto( f2, "h_fHT_Xe_OB3"+bit+part+"_GF", "", cols[3] );

      h_elavg[ip] = drawEffHisto(  f, "h_fHT_Xe"+bit+part+"_GF", "", kBlack );
      h_muavg[ip] = drawEffHisto( f2, "h_fHT_Xe"+bit+part+"_GF", "", kBlack );

      h_elpred[ip] = (TH1*)h_mu3[ip]->Clone();
      h_elpred[ip]->Reset("M");
      h_elpred[ip]->SetLineColor(kBlack);
      h_elpred[ip]->SetMarkerColor(kBlack);
      h_elpred[ip]->SetMarkerSize(1.4);
      h_elpred[ip]->SetMarkerStyle(29);

      TString fitfn = "[0] + [1]*(log10(x) - [2]);";
      f_el0[ip] = new TF1( "f_el0"+part, fitfn, 20.e3, 100.e3 );
      f_el1[ip] = new TF1( "f_el1"+part, fitfn, 20.e3, 100.e3 );
      f_el3[ip] = new TF1( "f_el0"+part, fitfn, 20.e3, 100.e3 );
      
      f_mu0[ip] = new TF1( "f_mu0"+part, fitfn, 100.0, 500.0 );
      f_mu1[ip] = new TF1( "f_mu1"+part, fitfn, 100.0, 500.0 );
      f_mu3[ip] = new TF1( "f_mu3"+part, fitfn, 100.0, 500.0 );

      f_elavg[ip] = new TF1( "f_elavg"+part, fitfn, 20.e3, 100.e3 );
      f_muavg[ip] = new TF1( "f_muavg"+part, fitfn, 100.0, 500.0 );
      
      f_el0[ip]->SetLineColor( cols[0] );
      f_mu0[ip]->SetLineColor( cols[0] );

      f_el1[ip]->SetLineColor( cols[1] );
      f_mu1[ip]->SetLineColor( cols[1] );
      
      f_el3[ip]->SetLineColor( cols[3] );
      f_mu3[ip]->SetLineColor( cols[3] );
      
      f_el0[ip]->SetParLimits( 1, 0., 1.);
      f_el1[ip]->SetParLimits( 1, 0., 1.);
      f_el3[ip]->SetParLimits( 1, 0., 1.);
       
      f_mu0[ip]->SetParLimits( 1, 0., 1.);
      f_mu1[ip]->SetParLimits( 1, 0., 1.);
      f_mu3[ip]->SetParLimits( 1, 0., 1.);

      /*h_el0[ip]->Fit( f_el0[ip], "QMR" );
      h_el3[ip]->Fit( f_el3[ip], "QMR" );

      h_mu0[ip]->Fit( f_mu0[ip], "QMR" );
      h_mu3[ip]->Fit( f_mu3[ip], "QMR" );*/
      
      h_elavg[ip]->Fit( f_elavg[ip], "QMR" ); 
      h_muavg[ip]->Fit( f_muavg[ip], "QMR" );
      
      // predict points for electron plateau at Occupancy (30-40%)
      for( int ibin(1); ibin < 50; ibin++ ) {
        double gam_mu = h_mu0[ip]->GetBinCenter(ibin);
        double gam_el = gam_mu * ( m_mu / m_el );
        if (gam_mu < 100.0) continue;
        if (gam_mu > 500.0) break;
        
        int elbin = h_mu0[ip]->FindBin(gam_el);

        double   pHTe0 = f_el0[ip]->Eval(gam_el);
        double   pHTm0 = f_mu0[ip]->Eval(gam_mu);
        double pHTmOcc = f_mu3[ip]->Eval(gam_mu);
        double pHTeOcc = pHTe0 + ( 1 - pHTe0 )*( pHTmOcc - pHTm0 );

        h_elpred[ip]->Fill( gam_el, pHTeOcc );
        //h_elpred[ip]->SetBinContent(elbin, pHTeOcc);
        //cout << elbin << "  " << pHTeOcc << endl;
        //cout << ibin << "  " << gam_mu << "  " << gam_el << "  " << elbin << "  " << pHTeOcc << endl;
      }
      
      /*TH1 *el35 = drawEffHisto( f, "h_fHT_Xe_OB3"+bit+part+"_GF", "", kGreen+2);
      mu35[ip]->SetMaximum(0.4);
      mu35[ip]->Draw("PE");
      el35->Draw("same");
      onset->Draw("same");
      mu35[ip]->Draw("PEsame");*/

      f_mu0[ip]->SetParameter( 0, f_muavg[ip]->GetParameter(0)*corr_mu0[ip] );
      f_mu0[ip]->SetParameter( 1, f_muavg[ip]->GetParameter(1)*corr_mu0[ip] );
      f_mu0[ip]->SetParameter( 2, f_muavg[ip]->GetParameter(2) );
      
      f_el0[ip]->SetParameter( 0, f_elavg[ip]->GetParameter(0)*corr_el0[ip] );
      f_el0[ip]->SetParameter( 1, f_elavg[ip]->GetParameter(1)*corr_el0[ip] );
      f_el0[ip]->SetParameter( 2, f_elavg[ip]->GetParameter(2) );
      
      f_mu1[ip]->SetParameter( 0, f_muavg[ip]->GetParameter(0)*corr_mu1[ip] );
      f_mu1[ip]->SetParameter( 1, f_muavg[ip]->GetParameter(1)*corr_mu1[ip] );
      f_mu1[ip]->SetParameter( 2, f_muavg[ip]->GetParameter(2) );
      
      f_el1[ip]->SetParameter( 0, f_elavg[ip]->GetParameter(0)*corr_el1[ip] );
      f_el1[ip]->SetParameter( 1, f_elavg[ip]->GetParameter(1)*corr_el1[ip] );
      f_el1[ip]->SetParameter( 2, f_elavg[ip]->GetParameter(2) );
      
      f_mu3[ip]->SetParameter( 0, f_muavg[ip]->GetParameter(0)*corr_mu3[ip] );
      f_mu3[ip]->SetParameter( 1, f_muavg[ip]->GetParameter(1)*corr_mu3[ip] );
      f_mu3[ip]->SetParameter( 2, f_muavg[ip]->GetParameter(2) );
      
      f_el3[ip]->SetParameter( 0, f_elavg[ip]->GetParameter(0)*corr_el3[ip] );
      f_el3[ip]->SetParameter( 1, f_elavg[ip]->GetParameter(1)*corr_el3[ip] );
      f_el3[ip]->SetParameter( 2, f_elavg[ip]->GetParameter(2) );
      
      //can->Print(pdf);
      
      h_elavg[ip]->Draw("");
      h_muavg[ip]->Draw("same");
     
      h_el0[ip]->Draw("same");
      h_mu0[ip]->Draw("same");
      f_mu0[ip]->Draw("same");
      f_el0[ip]->Draw("same");
      
      h_el1[ip]->Draw("same");
      h_mu1[ip]->Draw("same");
      f_mu1[ip]->Draw("same");
      f_el1[ip]->Draw("same");
      
      //h_el3[ip]->Draw("same");
      //h_mu3[ip]->Draw("same");
      //f_mu3[ip]->Draw("same");
      //f_el3[ip]->Draw("same");
      drawMajorLabels(part+"_GF",kBlack); 
      drawTypeLabels( "h_fHT_Xe_MBX" );
      
      can->Print(pdf);

    }
  }
  
  can->SetGridy(0);
  can->SetGridx(0);


  // ----------------------------------
  // Plot Efficiencies 
  // ----------------------------------
  double UseMax = 0.40;
  can->SetGridy(1);
  can->SetGridx(1);
  

  
  //plot High Occ vs. Low Occ
  for( TString hist: {"_GF","_OR","_OG","_SL","_ZR","_TW"}  ) {
    for( TString part: {"_BRL","_ECA","_ECB"} ) {
      for( TString head: {"_Xe"} ) {
        for( TString bit: { "_MBX" } ) {
          if (hist == "_GF") { 
            can->SetLogx(1);
            can->SetBottomMargin(0.1);
            //can->SetBottomMargin(0.18);
          } else { 
            can->SetLogx(0);
            can->SetBottomMargin(0.1);
          }

          TGaxis *axis1 = getMuonAxis();
          TGaxis *axis2 = getElecAxis();
       
          //plot highest occupancy first
          TH1 *h1 = drawEffHisto( f2, "h_fHT"+head+"_OB4"+bit+part+hist,     "", cols[4],   UseMax);
          if (hist == "_GF") {
            h1->SetTitleOffset(-0.6,"x");
            h1->GetXaxis()->SetTitle("#gamma Factor    ");
            h1->GetXaxis()->SetTitleSize(0.06);
            h1->GetXaxis()->SetLabelSize(0.05);
            h1->GetXaxis()->SetLabelOffset(-0.008);
          }
          h1->Draw("");
          for(int iOB(0); iOB < 5; iOB++ ) {
            //if ((iOB%2) != 0) continue;
            TString sOB = TString::Format("_OB%d",iOB);
            //if (hist != "_GF") drawEffHisto( f, "h_fHT"+head+sOB+bit+part+hist, "same", cols[iOB], UseMax);
            drawEffHisto( f, "h_fHT"+head+sOB+bit+part+hist, "same", cols[iOB], UseMax);
            drawEffHisto(f2, "h_fHT"+head+sOB+bit+part+hist, "same", cols[iOB], UseMax);
          }
          if (false && (hist == "_GF")) {
            drawEffHisto( f, "h_fHT"+head+"_OB3"+bit+part+hist, "same", cols[3], UseMax);
            drawEffHisto( f, "h_fHT"+head+"_OB0"+bit+part+hist, "same", cols[0], UseMax);
          }

          int iprt = -1;
          if (part == "_BRL") iprt = 0; 
          if (part == "_ECA") iprt = 1; 
          if (part == "_ECB") iprt = 2;
          h_elpred[iprt]->Draw("PEsame");
          //mu35[iprt]->Draw("same");

          drawMajorLabels(part+hist,kBlack); 
          drawTypeLabels( "h_fHT"+head+bit );
        
          /*drawText(0.30,0.92,"Z#rightarrowee MC Global Occ #geq 35%", kRed);
          drawText(0.30,0.87,"Z#rightarrow#mu#mu MC Global Occ #geq 35%", kBlue);
          drawText(0.61,0.92,"< 20%", kRed+2);
          drawText(0.61,0.87,"< 20%", kBlue+2);*/
          
          if (hist == "_GF") {
            axis1->Draw();
            axis2->Draw();
          }
          can->Print(pdf);
        }
      }
    }
  }

  can->SetGridy(0);
  can->SetGridx(0);
  
  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f->Close();


}


