#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>
#include <TGraphAsymmErrors.h>

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.035 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  h->GetYaxis()->SetTitleOffset(1.4);
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->SetBinContent( 0, 0 );
  h->SetBinContent( nbins, 0);
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHistoNorm2D(TH2 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetZaxis()->SetTitle("Fraction of Events / Bin");
  int nbinsx = h->GetNbinsX();
  int nbinsy = h->GetNbinsY();
  h->Scale( 1. / h->Integral( 0, (nbinsx+1), 0, (nbinsy+1) ) );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, float useMax=-999) 
{
  return drawEffHisto(getHisto(f,hn),opt,col,useMax);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHistoNorm2D(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm2D(getHisto2D(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

TF1 *getLinearFit( TH1 *h, int col=kMagenta+1, int sty=3 ) {
  TF1 *fn = new TF1("fn","pol3",0,1);
  h->Fit("fn","Q0");
  fn->SetLineColor(col);
  fn->SetLineStyle(sty);
  return fn;
}

TGaxis *getElecAxis( double ypos=-0.055 ) {
  double mEl = 0.000511;
  TGaxis *axis2 = new TGaxis(5000.0, ypos, 598000.0, ypos, 5000.0*mEl, 598000.0*mEl, 50510, "G");
  axis2->SetTitle("       Electron momentum [GeV] ");
  axis2->SetLabelFont(42);
  axis2->SetTitleFont(42);
  axis2->SetLabelSize(0.045);
  axis2->SetTitleSize(0.035);
  //axis2->SetLabelOffset(0.00005);
  axis2->SetTitleOffset(1.2);
  axis2->SetLabelOffset(-0.008);
  axis2->CenterTitle();
  return axis2;
}

TGaxis *getMuonAxis( double ypos=-0.055 ) {
  double mMu = 0.10599;
  TGaxis *axis1 = new TGaxis(35.0, ypos, 4000.0, ypos, 35.0*mMu, 4000.0*mMu, 50510, "G");
  axis1->SetTitle("Muon momentum [GeV]    ");
  axis1->SetLabelFont(42);
  axis1->SetTitleFont(42);
  axis1->SetLabelSize(0.045);
  axis1->SetTitleSize(0.035);
  axis1->SetTitleOffset(1.2);
  axis1->SetLabelOffset(-0.008);
  axis1->CenterTitle();
  return axis1;
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTypeLabels(TString head, int col=kBlack) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  //TString binType = "All HT bits";
  //if (head.Contains("_MBX")) binType = "Middle HT bit";
  TString binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.92,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.92, strawType, col);
    drawText( 0.14, 0.87,   binType, col);
  }
}

  
void drawMajorLabels(TString hname, int col=kBlack) 
{
  drawText(0.60,0.92,"#bf{#it{ATLAS}} Work In Progress",col);
  TString local = "";
  if (hname.Contains("BRL")) local = "Barrel";
  if (hname.Contains("ECA")) local = "Endcap Type A";
  if (hname.Contains("ECB")) local = "Endcap Type B";
   
  TString side = ", Side A+C";
  if (hname.Contains("_sA")) side = ", Side A";
  if (hname.Contains("_sC")) side = ", Side C";
  if (local != "") local += side;
  drawText(0.60,0.87,local,col);
}

void drawFuncLabel(TString func, int col=kBlack)
{
  TString desc = "MC15 Data";
  if (func == "f1") desc = "MC15 Fit";
  if (func == "f2") desc = "Data12 + MC15 Fit";
  drawText(0.14, 0.82, desc, col);
}

void drawOccLabels() {
  //int cols[6] = {kMagenta+2, kBlue+1, kGreen+2, kOrange-2, kOrange+6, kRed};
  drawTextSmall( 0.14, 0.82,  "50-60% Track Occ", kRed );
  drawTextSmall( 0.14, 0.79,  "40-50% Track Occ", kOrange+6 );
  drawTextSmall( 0.14, 0.76,  "30-40% Track Occ", kOrange-2 );
  drawTextSmall( 0.14, 0.73,  "20-30% Track Occ", kGreen+2 );
  drawTextSmall( 0.14, 0.70,  "10-20% Track Occ", kBlue+1 );
  drawTextSmall( 0.14, 0.67, "  0-10% Track Occ", kMagenta+2 );
}

void drawOccLabels2() {
  drawTextSmall( 0.44, 0.94,  "40-50% Track Occ", kOrange+6 );
  drawTextSmall( 0.44, 0.90,  "20-30% Track Occ", kGreen+2 );
  drawTextSmall( 0.44, 0.86, "  0-10% Track Occ", kMagenta+2 );
}

void drawOccLabels3() {
  drawTextSmall( 0.44, 0.94,  "50-60% Track Occ", kRed );
  drawTextSmall( 0.44, 0.90,  "30-40% Track Occ", kOrange-2 );
  drawTextSmall( 0.44, 0.86,  "10-20% Track Occ", kBlue+1 );
}

void drawGamLabels() {
  drawTextSmall( 0.44, 0.93, "Electron Plateau", kRed );
  drawTextSmall( 0.44, 0.88, "Muon Plateau", kBlue );
}

TH2 *get2DpHThist( TFile *f, TFile *f2, TString hname )
{
  TH2 *h2_nHT_ee = getHisto2D(  f, "h2_nHTMB"+hname ); 
  TH2 *h2_nLT_ee = getHisto2D(  f,   "h2_nLT"+hname );
  TH2 *h2_nHT_mm = getHisto2D( f2, "h2_nHTMB"+hname ); 
  TH2 *h2_nLT_mm = getHisto2D( f2,   "h2_nLT"+hname );

  TH2 *h2_nHT = (TH2*)h2_nHT_ee->Clone();
  TH2 *h2_nLT = (TH2*)h2_nLT_ee->Clone();
  h2_nHT->Add( h2_nHT_mm );
  h2_nLT->Add( h2_nLT_mm );

  TH2 *h2_pHT = (TH2*)h2_nHT->Clone();
  h2_pHT->Divide( h2_nLT );
  h2_pHT->SetMaximum(0.32);

  return h2_pHT;
}

TH2 *draw2DpHT( TFile *f, TFile *f2, TString hn, TString opt="colz" )
{
  return drawHisto2D( get2DpHThist(f,f2,hn), opt );
}

TH1 *getSliceX( TH2 *h2, int ymin=0, int ymax=-1 )
{
  int NbinsX = h2->GetNbinsX();
  if (ymax < 0) ymax = h2->GetNbinsY()+1;
  
  TH1 *h = (TH1*)h2->ProjectionX( "" );
  h->Reset("ICESM");
  //h->Sumw2(1);
  
  for( int xbin(0); xbin <= NbinsX+1; xbin++ ) {
    int nvals(0);
    double avg(0), err(0);
    for( int ybin = ymin; ybin <= ymax; ybin++ ) { 
      double val = h2->GetBinContent( xbin, ybin ); 
      double dx  = h2->GetBinError( xbin, ybin );
      if( val <= 0 ) continue;
      nvals++;
      avg += val;
      err += (dx*dx);
    }
    if (nvals <= 0) continue;
    avg /= nvals;
    err = sqrt(err)/nvals;
    h->SetBinContent(xbin, avg);
    h->SetBinError(xbin, err);
    //std::cout << "avg = " << avg << std::endl;
  }

  return h;
}

TH1 *getSliceY( TH2 *h2, int xmin=0, int xmax=-1 )
{
  int NbinsY = h2->GetNbinsY();
  if (xmax < 0) xmax = h2->GetNbinsX()+1;
  
  TH1 *h = (TH1*)h2->ProjectionY( "" );
  h->Reset("ICESM");
  //h->Sumw2(1);
  
  for( int ybin(0); ybin <= NbinsY+1; ybin++ ) {
    int nvals(0);
    double avg(0), err(0);
    for( int xbin = xmin; xbin <= xmax; xbin++ ) { 
      //avg += h2->GetBinContent( xbin, ybin ); 
      double val = h2->GetBinContent( xbin, ybin );
      double dx = h2->GetBinError( xbin, ybin );
      if( val <= 0 ) continue;
      nvals++;
      avg += val;
      err += (dx*dx);
    }
    if (nvals <= 0) continue;
    avg /= nvals;
    err = sqrt(err)/nvals;
    h->SetBinContent(ybin, avg);
    h->SetBinError(ybin, err);
    //std::cout << "avg = " << avg << std::endl;
  }

  //std::cout << "integral = " << h->Integral(0,NbinsY+1) << std::endl;;

  return h;
}

TH1 *drawSliceX( TH2 *h2, TString opt, int col=kBlack, int ymin=0, int ymax=-1, double useMax=-999 )
{
  return drawEffHisto( getSliceX(h2,ymin,ymax), opt, col, useMax);
}

TH1 *drawSliceY( TH2 *h2, TString opt, int col=kBlack, int xmin=0, int xmax=-1, double useMax=-999 )
{
  return drawEffHisto( getSliceY(h2,xmin,xmax), opt, col, useMax);
}

double integrate(TFile *f, TString hn )
{ 
  TH1 *h =  getHisto(f,hn); 
  double sum = h->Integral(); 
  return sum; 
}

double sqr( double a ) { return a*a; }

double func_OnsetOcc4(double *x, double *par) {
// ------------------------------------------------------------------- //
  if (x[0] < 0.0) return 0.0;

  // The onset function describes the gamma dependency at occupancy = 0:
  // Parameters:
  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  // -------------------------------------------------------------------
  // TR onset part (main part):
  double par2 = par[2] +  par[8]*x[1] +  par[9]*sqr(x[1]);
  double par3 = par[3] + par[10]*x[1] + par[11]*sqr(x[1]);
  double par4 = par[4] + par[7]*x[1];
  double exp_term = exp(-(log10(x[0]) - par4)/par[5]);
  double pHT_TR   = par2 + par3/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par2 + par3/(1.0 + exp_term0);
  double beta0 = par3 / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double par1 = par[1] + par[6]*x[1];
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par2 + par3/(1.0 + exp_term1);
  double beta1 = par3 / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par1);

  double pHT = pHT_TR;
  if      (log10(x[0]) < par[0]) pHT = pHT_dEdx;
  else if (log10(x[0]) > par1  ) pHT = pHT_HG;

  return pHT;
}
  
double pHTvsP( int iGas, int TRTpart, double gamma, double occ ) {
  // The onset function describes the gamma dependency at occupancy = 0:
  // Parameters:
  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  // -------------------------------------------------------------------
  // LH Type:
  //   0: Xe    
  //   1: Ar    
  //   2: Xe (from MC15)
  // -------------------------------------------------------------------
  // TR onset part (main part):

  /*double par2D_Xe[3][10] = {
    { 1.0000, 3.7955, 0.0424, 0.1373, 3.2247, 0.2400, 0.9713,  0.3026, 0.0000, -0.4244 },  // Barrel
    { 1.0000, 4.0962, 0.0546, 0.1711, 3.3826, 0.2235, 1.0968, -0.0853, 0.0000, -0.1858 },  // Endcap A
    { 1.0000, 3.6137, 0.0556, 0.1843, 3.1762, 0.1824, 0.6981,  0.5137, 0.0000, -0.1021 }}; // Endcap B
  
  double par2D_Ar[3][10] = {
    { 1.0000, 3.0142, 0.0406, 0.0224, 2.8225, 0.2544, 1.8543,  2.1235, 0.0000, -0.7855 },  // Barrel
    { 1.0000, 3.0788, 0.0480, 0.0157, 2.9294, 0.1297, 1.6632,  0.8530, 0.0000, -0.1925 },  // Endcap A
    { 1.0000, 3.0788, 0.0480, 0.0157, 2.9294, 0.1297, 1.6632,  0.8530, 0.0000, -0.1925 }}; // Endcap B

  double par2D_MC[3][10] = {
    { 1.0000, 3.7630, 0.0444, 0.1089, 3.2937, 0.2544, 0.9713,  0.3026, 0.0000, -0.6833 },  // Barrel
    { 1.0000, 3.5293, 0.0618, 0.1048, 3.1558, 0.1297, 1.0968, -0.0853, 0.0000, -0.2285 },  // Endcap A
    { 1.0000, 3.4769, 0.0695, 0.1375, 3.1296, 0.1318, 0.6981,  0.5137, 0.0000, -0.1430 }}; // Endcap B*/

  double par2D_Xe[3][12] = {
    { 1.0000, 2.9567, 0.0816, 0.1007, 2.8033, 0.1234, 0.7694, 0.5648, 0.7108, 0.2117, 0.2191,-0.2489},  // Barrel 
    { 1.0000, 3.3137, 0.1228, 0.1876, 3.0432, 0.1208, 1.0104,-0.2250, 0.5940, 0.0965, 0.2290,-0.2056},  // Endcap A
    { 1.0000, 3.3635, 0.1041, 0.1849, 3.0738, 0.1269, 0.5050, 0.3054, 0.2960, 0.0077, 0.4660,-0.2741}}; // Endcap B
  
  double *pars = par2D_Xe[TRTpart];
  //if (iGas == 1) pars = par2D_Ar[TRTpart]; 
  //if (iGas == 2) pars = par2D_Xe[TRTpart]; 
  
  double par4 = pars[4] + pars[9]*occ;
  double exp_term = exp(-(log10(gamma) - par4)/pars[5]);
  double pHT_TR   = pars[2] + pars[3]/(1.0 + exp_term);
  
  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(pars[0] - par4)/pars[5]);
  double alpha0 = pars[2] + pars[3]/(1.0 + exp_term0);
  double beta0 = pars[3] / sqr(1.0 + exp_term0) * exp_term0 / pars[5];
  double pHT_dEdx = alpha0 + beta0*(log10(gamma) - pars[0]);
 
  // High-gamma part (linear at high gamma):
  double par1 = pars[1] + pars[8]*occ;
  double exp_term1 = exp(-(par1 - par4)/pars[5]);
  double alpha1 = pars[2] + pars[3]/(1.0 + exp_term1);
  double beta1 = pars[3] / sqr(1.0 + exp_term1) * exp_term1 / pars[5];
  double pHT_HG   = alpha1 + beta1*(log10(gamma) - par1);
  
  double pHT_OccZero = pHT_TR;
  if      (log10(gamma) < pars[0]) pHT_OccZero = pHT_dEdx;
  else if (log10(gamma) > par1  )  pHT_OccZero = pHT_HG;
 
  // The occupancy dependency is included through the Anatoli-Petersen formula:
  // --------------------------------------------------------------------------
  double occ_factor = pars[10] + pars[6]*(occ-pars[11]) + pars[7]*sqr(occ-pars[11]);
  double pHT = pHT_OccZero * occ_factor; 
  //double occ_factor = 1.0 + pars[6]*(occ/1.0) + pars[7]*sqr(occ/1.0);     // Quadratic, and unit at occ=0
  //double pHT = pHT_OccZero + (1.0 - pHT_OccZero + pars[2]) * (pHT_OccZero*occ_factor - pHT_OccZero);

  return pHT;
}


///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString  inFN("outputZee_500k/hist-Zee.root");
  TString inFN2("outputZmm_500k/hist-Zmumu.root"), pdf("plot_2DSurf.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile")  inFN=argv[++i];
    if (arg=="--inputFile2") inFN2=argv[++i];
    if (arg=="-in")  inFN=argv[++i];
    if (arg=="-in1")  inFN=argv[++i];
    if (arg=="-in2") inFN2=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }

  printf("\n  Running plotRecoEff\n");
  printf("  input file Z(ee): %s\n", inFN.Data());
  printf("  input file Z(mm): %s\n",inFN2.Data());
  TFile *f1 = openFile(inFN);   // Z->ee
  TFile *f2 = openFile(inFN2); // Z->mm

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.02); can->SetRightMargin(0.04);
  can->Print(pdf+"[");

  can->SetRightMargin(0.12); can->SetLogx(1);
  //for( TString gasType: {"_Xe", "_Ar"} ) {
  for( TString gasType: {"_Xe"} ) {
    for( TString part: {"_BRL", "_ECA", "_ECB"} ) {
      if ((gasType == "_Ar") && (part == "_ECB")) continue;

      int ipart = 0;
      if (part == "_ECA") ipart = 1;
      if (part == "_ECB") ipart = 2;

      int igas = 0;
      if (gasType == "_Ar") igas = 1;
      
      // Draw 2D onset
      can->SetRightMargin(0.12); can->SetLogx(1);
      TString hn = gasType+part+"_GF_OT";
      TH2 *h2_pHT = draw2DpHT( f1, f2, hn );
      drawMajorLabels( hn );
      drawTypeLabels( hn );
      drawFuncLabel("");
      can->Print(pdf);

      
      // Draw From Fit for comparison.
      TH2 *h2_fn = (TH2*)h2_pHT->Clone();
      //h2_fn->Reset("ICES");
      for( int i(1), nx(h2_fn->GetNbinsX()); i <= nx; i++) {
        for( int j(1), ny(h2_fn->GetNbinsY()); j <= ny; j++) {
          double gamma = h2_fn->GetXaxis()->GetBinCenter(i);
          double occ   = h2_fn->GetYaxis()->GetBinCenter(j);
          double pHT   = pHTvsP( igas, ipart, gamma, occ );
          if ( h2_fn->GetBinContent(i,j) == 0 ) continue;
          h2_fn->SetBinContent( i, j, pHT );
          h2_fn->SetBinError(   i, j, 0.0 );
        }
      }
      h2_fn->Draw("colz");
      drawMajorLabels( hn );
      drawTypeLabels( hn );
      drawFuncLabel("f1");
      can->Print(pdf);

      //Draw projections (along X)
      int cols[6] = {kMagenta+2, kBlue+1, kGreen+2, kOrange-2, kOrange+6, kRed};
      can->SetRightMargin(0.04); can->SetLogx(1);
      TH1 *h_projX_MC[6];  TH1 *h_projX_fm[6];  TH1 *h_projX_fd[6];
      for( int i=0; i < 6; i++ ) {
        TH1 *htempMC = drawSliceX( h2_pHT, "", cols[i], 1+i*5, 5+i*5, 0.45);
        h_projX_MC[i] = (TH1*)htempMC->Clone();
        TH1 *htempfm = drawSliceX(  h2_fn, "", cols[i], 1+i*5, 5+i*5, 0.45);
        h_projX_fm[i] = (TH1*)htempfm->Clone();
        
        h_projX_MC[i]->SetMarkerStyle(2);
        h_projX_fm[i]->SetMarkerStyle(24);
        h_projX_MC[i]->SetMarkerColor(cols[i]);
        h_projX_fm[i]->SetMarkerColor(cols[i]);
     
        for( int ibin(0), nbins(h_projX_MC[i]->GetNbinsX()); ibin < nbins+1; ibin++ ) {
          if ( h_projX_MC[i]->GetBinContent(ibin) != 0 ) continue;
          h_projX_fm[i]->SetBinContent(ibin, 0);
        }
        
      }

      //Get Y slices
      // Muons vs Electrons
      int NSlicesY = 2;
      TH1 *h_projY_MC[2], *h_projY_fm[2], *h_projY_fd[2];
      int minbin[2] = {  4, 27 };
      int maxbin[2] = { 17, 40 };
      int cols2[2] = { kBlue, kRed };
      
      for( int i=0; i < NSlicesY; i++ ) {
        TH1 *htempMC = drawSliceY( h2_pHT, "", cols2[i], minbin[i], maxbin[i], 0.45);
        h_projY_MC[i] = (TH1*)htempMC->Clone();
        TH1 *htempfm = drawSliceY(  h2_fn, "", cols2[i], minbin[i], maxbin[i], 0.45);
        h_projY_fm[i] = (TH1*)htempfm->Clone();
        
        h_projY_MC[i]->SetMarkerStyle(2);
        h_projY_fm[i]->SetMarkerStyle(24);
        
        h_projY_MC[i]->SetMarkerColor(cols2[i]);
        h_projY_fm[i]->SetMarkerColor(cols2[i]);

        for( int ibin(0), nbins(h_projY_MC[i]->GetNbinsX()); ibin < nbins+1; ibin++ ) {
          if ( h_projY_MC[i]->GetBinContent(ibin) != 0 ) continue;
          h_projY_fm[i]->SetBinContent(ibin, 0);
        }
      }


      //Create Legend #1
      TLegend* leg  = new TLegend(0.29, 0.70, 0.49, 0.86);
      TLegend* leg2 = new TLegend(0.27, 0.81, 0.47, 0.97);
      //TLegend* leg = new TLegend(0.125, 0.51, 0.325, 0.67);
      leg->SetFillColor(0);   leg->SetLineColor(0);
      leg2->SetFillColor(0); leg2->SetLineColor(0);
      TH1 *h_MC = (TH1*)h_projX_MC[0]->Clone();
      TH1 *h_fm = (TH1*)h_projX_fm[0]->Clone();
      h_MC->SetMarkerColor(kBlack);
      h_fm->SetMarkerColor(kBlack);
      leg->AddEntry( h_MC, "MC15 Data", "p");
      leg->AddEntry( h_fm, "MC15 Fit", "p");
      leg2->AddEntry(h_MC, "MC15 Data", "p");
      leg2->AddEntry(h_fm, "MC15 Fit", "p");

      //Draw MC vs MC fits 
      for( int i=0; i < 6; i++ ) {
        if (i%2 != 0) continue;
        TString opt = ( (i==0) ? "PE" : "PEsame" );
        drawEffHisto( h_projX_MC[i],      opt, cols[i], 0.45 );
        drawEffHisto( h_projX_fm[i], "PEsame", cols[i], 0.45 );
      }
      leg2->Draw();
      drawMajorLabels( hn );
      drawTypeLabels( hn );
      drawOccLabels2();
      can->Print(pdf);
      
      //Draw MC vs MC fits 
      for( int i=0; i < 6; i++ ) {
        if (i%2 == 0) continue;
        TString opt = ( (i==1) ? "PE" : "PEsame" );
        drawEffHisto( h_projX_MC[i],      opt, cols[i], 0.45 );
        drawEffHisto( h_projX_fm[i], "PEsame", cols[i], 0.45 );
      }
      leg2->Draw();
      drawMajorLabels( hn );
      drawTypeLabels( hn );
      drawOccLabels3();
      can->Print(pdf);
      

      //Draw MC vs MC projections (along Y)
      can->SetRightMargin(0.04); can->SetLogx(0);
      for( int i=0; i < NSlicesY; i++ ) {
        TString opt = ( (i==0) ? "PE" : "PEsame" );
        drawEffHisto( h_projY_MC[i],      opt, cols2[i], 0.45);
        drawEffHisto( h_projY_fm[i], "PEsame", cols2[i], 0.45);
      }
      leg2->Draw();
      drawMajorLabels( hn );
      drawTypeLabels( hn );
      drawGamLabels();
      can->Print(pdf);
    
    }
  }

  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f1->Close();
  f2->Close();


}


