#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>
#include <TGraphAsymmErrors.h>

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}

double sqr(double a) {return a*a;}

double func_onset(double *x, double *par) {
  if (x[0] < 0.0) return 0.0;

  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par[4])/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par[4])/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par[1] - par[4])/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par[1]);
  
  if (log10(x[0])      < par[0]) return pHT_dEdx;
  else if (log10(x[0]) > par[1]) return pHT_HG;
  else                           return pHT_TR;
}

TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, float useMax=-999) 
{
  h->SetLineColor(col); //h->SetLineWidth(lw); 
  h->SetStats(0);
  
  //hide away the worst points
  for( int ibin(1); ibin < h->GetNbinsX()+1; ibin++) { 
    if ( h->GetBinError(ibin) > 0.025 ) {
      h->SetBinError(ibin, 0.0);
      h->SetBinContent(ibin, 0.0); //or -1.0? 
    }
  }

  if (useMax == -999) useMax = h->GetMaximum()*1.2; 
  h->GetYaxis()->SetRangeUser(0,useMax);
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  h->GetYaxis()->SetTitleOffset(1.4);
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->SetBinContent( 0, 0 );
  h->SetBinContent( nbins, 0);
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHistoNorm2D(TH2 *h, TString opt, int col=kBlue, int lw=1) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetZaxis()->SetTitle("Fraction of Events / Bin");
  int nbinsx = h->GetNbinsX();
  int nbinsy = h->GetNbinsY();
  h->Scale( 1. / h->Integral( 0, (nbinsx+1), 0, (nbinsy+1) ) );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, float useMax=-999) 
{
  return drawEffHisto(getHisto(f,hn),opt,col,useMax);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHistoNorm2D(TFile *f, TString hn, TString opt, int col=kBlue, int lw=1) 
{
  return drawHistoNorm2D(getHisto2D(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

TF1 *getLinearFit( TH1 *h, int col=kMagenta+1, int sty=3 ) {
  TF1 *fn = new TF1("fn","pol3",0,1);
  h->Fit("fn","Q0");
  fn->SetLineColor(col);
  fn->SetLineStyle(sty);
  return fn;
}

TGraph *drawpHTPlane( TH1 *h1, TH1 *h2, TString opt, int col=kBlue) {
  TGraph *tg = new TGraph();
  tg->SetTitle(";pHT(#mu); pHT(e)");
  tg->SetMarkerColor(col);
  tg->SetMarkerStyle(11);
  tg->SetMarkerSize(0.5);

  //TF1 *fn1 = getLinearFit(h1);
  //TF1 *fn2 = getLinearFit(h2);
      
  int ipt(0);
  int Nbins = h1->GetNbinsX()+1;
  for( int ibin(1); ibin < Nbins; ibin++ ) {
    //double occ = h1->GetBinCenter(ibin);
    //double pHT_el = fn1->Eval(occ);
    //double pHT_mu = fn2->Eval(occ);
    //double pHT_el_bin = h1->GetBinContent(ibin);
    //double pHT_mu_bin = h2->GetBinContent(ibin);
    //if ( (pHT_el_bin <= 0) || (pHT_mu_bin <= 0) ) continue;
    double pHT_el = h1->GetBinContent(ibin);
    double pHT_mu = h2->GetBinContent(ibin);
    if ( (pHT_el <= 0) || (pHT_mu <= 0) ) continue;
    tg->SetPoint( ipt, pHT_mu, pHT_el);
    ipt++;
  }

  tg->GetXaxis()->SetLimits(0.,0.3);    // along X
  tg->GetHistogram()->SetMaximum(0.50); // along 
  tg->GetHistogram()->SetMinimum(0.15); // Y 
  tg->Draw(opt);
  return tg;
}

TGaxis *getElecAxis( double ypos=-0.055 ) {
  double mEl = 0.000511;
  TGaxis *axis2 = new TGaxis(5000.0, ypos, 598000.0, ypos, 5000.0*mEl, 598000.0*mEl, 50510, "G");
  axis2->SetTitle("       Electron momentum [GeV] ");
  axis2->SetLabelFont(42);
  axis2->SetTitleFont(42);
  axis2->SetLabelSize(0.045);
  axis2->SetTitleSize(0.035);
  //axis2->SetLabelOffset(0.00005);
  axis2->SetTitleOffset(1.2);
  axis2->SetLabelOffset(-0.008);
  axis2->CenterTitle();
  return axis2;
}

TGaxis *getMuonAxis( double ypos=-0.055 ) {
  double mMu = 0.10599;
  TGaxis *axis1 = new TGaxis(35.0, ypos, 4000.0, ypos, 35.0*mMu, 4000.0*mMu, 50510, "G");
  axis1->SetTitle("Muon momentum [GeV]    ");
  axis1->SetLabelFont(42);
  axis1->SetTitleFont(42);
  axis1->SetLabelSize(0.045);
  axis1->SetTitleSize(0.035);
  axis1->SetTitleOffset(1.2);
  axis1->SetLabelOffset(-0.008);
  axis1->CenterTitle();
  return axis1;
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTypeLabels(TString head, int col=kOrange+1) 
{
  TString strawType = "Xe+Ar Scenario 1  ";
  if (head.Contains("_Xe")) strawType = "Xenon Only  ";
  if (head.Contains("_Ar")) strawType = "Argon Only  ";
  
  TString binType = "All HT bits";
  if (head.Contains("_MBX")) binType = "Middle HT bit";

  TString label = strawType+"   "+binType;
  if (head == "h_fAr") drawText(0.14,0.92,"Fraction of Argon Straws",col);
  else {
    drawText( 0.14, 0.92, strawType, col);
    drawText( 0.14, 0.87,   binType, col);
  }
}

  
void drawMajorLabels(TString hname, int col=kBlack) 
{
    drawText(0.685,0.92,"#bf{#it{ATLAS}} Work In Progress",col);
    TString local = "";
    if (hname.Contains("BRL")) local = "Barrel";
    if (hname.Contains("ECA")) local = "Endcap Type A";
    if (hname.Contains("ECB")) local = "Endcap Type B";
   
    TString side = ", Side A+C";
    if (hname.Contains("_sA")) side = ", Side A";
    if (hname.Contains("_sC")) side = ", Side C";
    if (local != "") local += side;
    drawText(0.685,0.87,local,col);
}


double integrate(TFile *f, TString hn )
{ 
  TH1 *h =  getHisto(f,hn); 
  double sum = h->Integral(); 
  return sum; 
} 


///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString  inFN("outputZee/hist-Zee.root");
  TString inFN2("outputZmm/hist-Zmumu.root"), pdf("plots.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile")  inFN=argv[++i];
    if (arg=="--inputFile2") inFN2=argv[++i];
    if (arg=="-in")  inFN=argv[++i];
    if (arg=="-in1")  inFN=argv[++i];
    if (arg=="-in2") inFN2=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }

  printf("\n  Running plotRecoEff\n");
  printf("  input file Z(ee): %s\n",inFN.Data());
  printf("  input file Z(mm): %s\n",inFN.Data());
  TFile *f = openFile(inFN);   // Z->ee
  TFile *f2 = openFile(inFN2); // Z->mm

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.02); can->SetRightMargin(0.04);
  can->Print(pdf+"[");

  // ----------------------------------
  // Plot global variables
  // ----------------------------------
  //for( TString hists: {"h_averageMu", "h_OccGlobal", "h_avgmu_OG", "h_massZll"} ) {
  /*for( TString hists: {"h_averageMu", "h_OccGlobal", "h_massZll"} ) {
    drawHistoNorm( f, hists     ,"",kRed);
    drawHistoNorm(f2, hists ,"same",kBlue); 
    drawMajorLabels("",kBlack);
    drawText(0.55,0.92,"Z#rightarrowee MC", kRed);
    drawText(0.55,0.87,"Z#rightarrow#mu#mu MC", kBlue);
    can->Print(pdf);
  }

  for (TString hists: {"h_avgmu_OG"} ) {
    drawHisto( f, hists     ,"",kRed);
    drawHisto(f2, hists ,"same",kBlue); 
    drawMajorLabels("",kBlack);
    drawText(0.55,0.92,"Z#rightarrowee MC", kRed);
    drawText(0.55,0.87,"Z#rightarrow#mu#mu MC", kBlue);
    can->Print(pdf);
  }

  
  // ----------------------------------
  // Plot local variables
  // ----------------------------------
  for( TString hists: {"h_OccBRL", "h_OccECA", "h_OccECB"} ) {
    for( TString side: {"_sA", "_sC"} ) {
      drawHistoNorm( f, hists+side ,    "",kRed);
      drawHistoNorm(f2, hists+side ,"same",kBlue);
      drawMajorLabels(hists+side,kBlack);
      drawText(0.55,0.92,"Z#rightarrowee MC", kRed);
      drawText(0.55,0.87,"Z#rightarrow#mu#mu MC", kBlue);
      can->Print(pdf);
    }
  }*/

  // ----------------------------------
  // Fit Turn On Curve
  // ----------------------------------
  double UseMax = 0.40;
  can->SetGridy(1);
  can->SetGridx(1);
  
  for ( TString bit: { "_MBX" } ) {
    for ( TString part: { "_BRL", "_ECA", "_ECB" } ) {
      can->SetLogx(1);
      can->SetBottomMargin(0.18);

      // Need a dedicated function to get combined histo.
      TH1 *onset = drawEffHisto( f, "h_fHT_Xe_OB0"+bit+part+"_GF", "", kBlack);
      TH1 *on2 = drawEffHisto( f2, "h_fHT_Xe_OB0"+bit+part+"_GF", "", kBlack);
      onset->Add(on2);

      //Get Fit
      double gMin = 50.0;
      double gMax = 1000000.0;
      TString fitname = "fit"+part+bit;
      TF1 *fit = new TF1( fitname, func_onset, gMin, gMax, 6);
      fit->SetLineColor(kMagenta);
      //fit->SetParameters(2.0, 3.5, 0.05, 0.25, 3.1, 2.0); //errors of > 2.5%
      fit->SetParameters(2.0, 3.5, 0.05, 0.25, 3.1, 2.0); //errors of > 1.25%
      fit->SetParName(0, "Saturation point");
      fit->SetParName(1, "pHT(dE/dx)");
      fit->SetParName(2, "pHT(TR)");
      fit->SetParName(3, "Onset mean");
      fit->SetParName(4, "Onset width");

      // We fix the lower plateau to be flat (can be omitted)
      fit->FixParameter(0, -1.5);
      onset->Fit(fitname, "MR");

      onset->Draw();
      can->Print(pdf);
    }
  }
  
  can->SetGridy(0);
  can->SetGridx(0);
  
  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f->Close();


}


