#include <iostream>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGraphAsymmErrors.h>

using namespace std; 

////////////////////////// Small and very helpful functions //////////////////////////

void fatal(TString msg) { printf("\nFATAL\n  %s\n\n",msg.Data()); abort(); }


TFile *openFile(TString fn) 
{
  TFile *f = TFile::Open(fn);
  if (f==nullptr) fatal("Cannot open "+fn);
  return f;
}


TH1 *getHisto(TFile *f, TString hn) 
{
  TH1 *h = (TH1*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH2 *getHisto2D(TFile *f, TString hn)
{
  TH2 *h = (TH2*)f->Get(hn);
  if (h==nullptr) fatal("Cannot access histo "+hn+" in file "+f->GetName());
  return h;
}

TH1 *drawHisto(TH1 *h, TString opt, int col=kBlue, int lw=2) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TH1 *h, TString opt, int col=kBlue, int lw=2) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  //h->GetYaxis()->SetRangeUser(0,1.2); 
  h->GetYaxis()->SetRangeUser(0,h->GetMaximum()*1.2);
  //h->GetYaxis()->SetTitle("Efficiency / Bin");
  h->Draw(opt); return h;
}

TH1 *drawHistoNorm(TH1 *h, TString opt, int col=kBlue, int lw=2) 
{
  h->SetLineColor(col); h->SetLineWidth(lw); h->SetStats(0);
  h->GetYaxis()->SetTitle("Fraction of Events / Bin");
  int nbins = h->GetNbinsX();
  h->SetBinContent( 1, h->GetBinContent(0)+h->GetBinContent(1));
  h->SetBinContent( nbins, h->GetBinContent(nbins) + h->GetBinContent(nbins+1) );
  h->Scale( 1. / h->Integral( -(nbins+1), (nbins+1) ) );
  h->SetMaximum( 1.3 * h->GetMaximum() );
  h->Draw(opt); return h;
}

TH2 *drawHisto2D(TH2 *h, TString opt)
{ 
  h->SetStats(0);
  h->Draw(opt); return h;
}

TH1 *drawEffHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=2) 
{
  return drawEffHisto(getHisto(f,hn),opt,col);
}


TH1 *drawHisto(TFile *f, TString hn, TString opt, int col=kBlue, int lw=2) 
{
  return drawHisto(getHisto(f,hn),opt,col);
}

TH1 *drawHistoNorm(TFile *f, TString hn, TString opt, int col=kBlue, int lw=2) 
{
  return drawHistoNorm(getHisto(f,hn),opt,col);
}

TH2 *drawHisto2D(TFile *f, TString hn, TString opt)
{
  return drawHisto2D(getHisto2D(f,hn),opt);
}

void drawText(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.036); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawTextSmall(double x, double y, TString txt, int col=kBlack) 
{
  static TLatex *tex = new TLatex(); tex->SetNDC();
  tex->SetTextFont(42); tex->SetTextSize(0.028); tex->SetTextColor(col);
  tex->DrawLatex(x,y,txt);
}

void drawMajorLabels(TString hname, int col=kBlack) {
    drawText(0.65,0.90,"#bf{#it{ATLAS}} Work In Progress",col);
    TString local = "";
    if (hname.Contains("BRL")) local = "Barrel";
    if (hname.Contains("ECA")) local = "Endcap Wheels A";
    if (hname.Contains("ECB")) local = "Endcap Wheels B";
    
    if (hname.Contains("_sA")) local += ", Side A";
    else if (hname.Contains("_sC")) local += ", Side C";
    else if (local != "") local += ", Sides A+C";
    drawText(0.65,0.85, local, col);
}


double integrate(TFile *f, TString hn )
{ 
  TH1 *h =  getHisto(f,hn); 
  double sum = h->Integral(); 
  return sum; 
} 


///////////////////////// The main plotting function ////////////////////////////////
int main(int argc, char **argv) 
{
  // input file name
  TString inFN("submitDir/hist-Zmumu.root"), pdf("plots.pdf");
  //TString inFN, pdf; 
  for (int i=1;i<argc;++i) 
  {
    TString arg(argv[i]);
    if (arg=="--inputFile") inFN=argv[++i];
    if (arg=="--pdfFile") pdf=argv[++i];
    if (arg=="-in") inFN=argv[++i];
    if (arg=="-pdf") pdf=argv[++i];
  }

  printf("\n  Running plotRecoEff\n");
  printf("  input file: %s\n",inFN.Data());
  TFile *f = openFile(inFN);

  // Create output pdf
  // Set style
  TCanvas *can = new TCanvas();
  can->SetTopMargin(0.04); can->SetRightMargin(0.04);
  can->Print(pdf+"[");

  // ----------------------------------
  // Plot global variables
  // ----------------------------------
  for( TString hists: {"h_averageMu", "h_OccGlobal", "h_massZll"} ) {
    drawHistoNorm(f, hists ,"",kBlue);
    drawMajorLabels("",kBlack);
    can->Print(pdf);
  }
  
  // ----------------------------------
  // Plot local variables
  // ----------------------------------
  for( TString hists: {"h_OccBRL", "h_OccECA", "h_OccECB"} ) {
    for( TString side: {"_sA", "_sC"} ) {
      drawHistoNorm(f, hists+side ,"",kBlue);
      drawMajorLabels(hists+side,kBlack);
      can->Print(pdf);
    }
  }
  
  
  // ----------------------------------
  // Plot Efficiencies 
  // ----------------------------------
  can->SetGridy(1);
  can->SetGridx(1);

  for( TString head: {"h_fHT","h_fHT_MBX","h_fHT_Xe","h_fHT_Xe_MBX","h_fHT_Ar","h_fHT_Ar_MBX","h_fAr"}) {
    for( TString part: {"_BRL","_ECA","_ECB"} ) {
      for( TString hist: {"_SL","_ZR","_TW"}  ) {
        if (hist == "_GF") can->SetLogx(1);
        else               can->SetLogx(0);

        drawEffHisto(f, head+part+hist, "", kBlue);
        drawMajorLabels(part+hist,kBlack); 
        can->Print(pdf);
      }
    }
  }

  //plot 3BX vs. MBX
  for( TString head: {"","_Ar","_Xe"} ) {
    for( TString part: {"_BRL","_ECA","_ECB"} ) {
      for( TString hist: {"_SL","_ZR","_TW"}  ) {
        if (hist == "_GF") can->SetLogx(1);
        else               can->SetLogx(0);

        drawEffHisto(f, "h_fHT"+head+    ""+part+hist,     "", kRed);
        drawEffHisto(f, "h_fHT"+head+"_MBX"+part+hist, "same", kBlue);
        drawMajorLabels(part+hist,kBlack); 
        
        drawText(0.58,0.90,"3BX", kRed);
        drawText(0.58,0.85,"MBX", kBlue);
        can->Print(pdf);
      }
    }
  }
  
  //plot 3BX vs. MBX
  for( TString head: {"","_Ar","_Xe"} ) {
    for( TString bit: { "", "_MBX" } ) {
      for( TString part: {"_BRL","_ECA","_ECB"} ) {
        for( TString hist: {"_SL","_ZR","_TW"}  ) {
          if (hist == "_GF") can->SetLogx(1);
          else               can->SetLogx(0);

          drawEffHisto(f, "h_fHT"+head+"_HO"+bit+part+hist,     "", kRed);
          drawEffHisto(f, "h_fHT"+head+"_LO"+bit+part+hist, "same", kBlue);
          drawMajorLabels(part+hist,kBlack); 
          drawText(0.38,0.90,"Global Occupancy #geq 35%", kRed);
          drawText(0.38,0.85,"Global Occupancy < 35%", kBlue);
          can->Print(pdf);
        }
      }
    }
  }

  can->SetGridy(0);
  can->SetGridx(0);
  //end grids for eff plots

  /*
  // ----------------------------------
  // Draw any 2D plots 
  // ----------------------------------
  can->SetTopMargin(0.04); can->SetRightMargin(0.14);
  for( TString hists: {"h2_true_x_y"} ) {
          drawHisto2D(f, hists ,"colz");
          drawMajorLabels2D(inFN,kRed);
          can->Print(pdf);
  }*/
  
  can->Print(pdf+"]");
  printf("  Produced: %s\n\n",pdf.Data());
  f->Close();


}


