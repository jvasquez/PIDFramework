// 
// Author: jared.vasquez@yale.edu
//

#include "PIDFramework/PIDAnalysis.h"
#include "PIDFramework/RunUtils.h"

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  PIDAnalysis *alg = new PIDAnalysis();

  // Use helper to start the job
  PID::runJob(alg, argc, argv);

  return 0;
}
