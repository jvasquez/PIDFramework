#!/bin/perl
# run this script in a fresh shell after having setup athena with source start_athena.sh
# run on the grid with 
# pathena PileupReweighting/generatePRW_jobOptions.py --inDS="my.input.ds/,etc/,etc/" --outDS="user.myname.whatever/"
# You can hadd all the config files together you get from the grid. Important note: You must not combine the PRW config files of 25ns and 50ns versions of the datasets. Keep the 25ns and 50ns separate, and use the resulting config files with separate instances of the PRW tool

my $debug=0;
my $submit=1;

$ARGC=@ARGV;
if($ARGC!=1) {
    die "usage: ./createPRWFiles.pl <filename>\n";
}

open FILE, $ARGV[0] or die $!;

while (<FILE>) {
    $dataset = $_;
    chop($dataset);
    if ($dataset =~ /^#/) {
	if ($debug>0) { print "Skipping commented line $dataset\n"; }
    } elsif ($dataset eq "") {
	if ($debug>0) { print "Skipping empty line $dataset\n"; }
    } else {
	$dataset =~ m/(\w+).(\w+).(\w+).(\w+).(\w+).(\w+)/;
	my $mctype=$1;
	my $dsid=$2;
	my $sample=$3;
	my $recoormerge=$4;
	my $aodordaod=$5;
	my $fulltag=$6;
	if ($aodordaod ne "AOD") {
	    print "Sample is not AOD: it is $aodordaod. Skipping..\n";
	}
	else {
	    $outds="user.nproklov.$mctype.$dsid.$recoormerge.PRW.$fulltag";
	    $cmd = "pathena PileupReweighting/generatePRW_jobOptions.py --inDS=\"$dataset/\" --outDS=\"$outds/\"";
	    if ($debug>0) { print "Command = $cmd\n"; }
	    if ($submit>0) { system($cmd); }
	}
    }
}
