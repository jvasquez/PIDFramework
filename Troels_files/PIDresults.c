// ------------------------------------------------------------------- //
/*
  ROOT macro for producing TRT PID results from Zee and Zmumu 2012 data.

  Author: Troels C. Petersen (NBI)
  Email:  petersen@nbi.dk
  Date:   Started 07-05-2014

  Straw Layer             [short: SL]: Barrel: 73 -> 73 bins,  EndcapA: 6x16 = 96 bins,  EndcapB: 8x8 = 64 bins
  Z/R position in straw   [short: ZR]: Barrel: 0-720mm -> 36 bins,  Endcaps: 630-1030mm -> 40 bins
  Track-to-Wire distance  [short: TW]: 0.0-2.2mm (trunkating to a maximum of 2.175mm) in 44 bins

*/
// ------------------------------------------------------------------- //


double sqr(double a) {return a*a;}


// ------------------------------------------------------------------- //
void calcROC(const TH1D* &histSig, const TH1D* &histBkg, TGraph* &graphROC,
	     const double &TargetSigEff, double &BkgEffAtSigTarget, double &CutValueAtSigTarget, 
	     const double &TargetBkgEff, double &SigEffAtBkgTarget, double &CutValueAtBkgTarget) {
// ------------------------------------------------------------------- //

  // Loop over histogram bins and calculate vectors of efficiencies:
  // ---------------------------------------------------------------
  vector<double> effSig, effBkg;
  if (histSig->GetNbinsX() == histBkg->GetNbinsX()) {
    int Nbins = histSig->GetNbinsX();
    if (fabs(histSig->GetXaxis()->GetXmax() - histBkg->GetXaxis()->GetXmax()) < 0.0001 &&
	fabs(histSig->GetXaxis()->GetXmin() - histBkg->GetXaxis()->GetXmin()) < 0.0001) {
      double Xmin = histSig->GetXaxis()->GetXmin();
      double Xmax = histSig->GetXaxis()->GetXmax();
      double dX = (Xmax-Xmin)/double(Nbins);
      double AllSig = histSig->GetEntries();
      double AllBkg = histBkg->GetEntries();
      double IntSig = 0.0;
      double IntBkg = 0.0;
      for (int ibin=0; ibin < Nbins+2; ibin++) {
	IntSig += histSig->GetBinContent(ibin) / AllSig;
	effSig.push_back(1.0-IntSig);
	IntBkg += histBkg->GetBinContent(ibin) / AllBkg;
	effBkg.push_back(1.0-IntBkg);
	double binmin = Xmin + (ibin-1) * dX;
	double binmax = Xmin +  ibin    * dX;
	// printf("  bin %3d:    %7.3f - %7.3f    %6.4f  %6.4f \n", ibin, binmin, binmax, IntSig, IntBkg);
      }


      // Make TGraph of vectors:
      // -----------------------
      graphROC = new TGraph(Nbins, &effSig[0], &effBkg[0]);
      

      // Calculate background and signal efficiency at signal and background target:
      // ---------------------------------------------------------------------------
      int ibinBkg = 1;
      while (effSig[ibinBkg] > TargetSigEff  &&  ibinBkg < Nbins+1) {ibinBkg++;}
      double fracSigBin = (TargetSigEff - effSig[ibinBkg-1]) / (effSig[ibinBkg] - effSig[ibinBkg-1]);
      BkgEffAtSigTarget = effBkg[ibinBkg-1] +  fracSigBin * (effBkg[ibinBkg] - effBkg[ibinBkg-1]);
      CutValueAtSigTarget = Xmin + (ibinBkg-1) * dX + fracSigBin * dX;
      // printf("  Bin: %3d    fracBin: %4.2f     BkgEffAtSigTarget: %5.3f    CutValue: %6.3f \n",
      //	     ibinBkg, fracSigBin, BkgEffAtSigTarget, CutValueAtSigTarget);
      
      int ibinSig = 1;
      while (effBkg[ibinSig] > TargetBkgEff  &&  ibinSig < Nbins+1) {ibinSig++;}
      double fracBkgBin = (TargetBkgEff - effBkg[ibinSig-1]) / (effBkg[ibinSig] - effBkg[ibinSig-1]);
      SigEffAtBkgTarget = effSig[ibinSig-1] +  fracBkgBin * (effSig[ibinSig] - effSig[ibinSig-1]);
      CutValueAtBkgTarget = Xmin + (ibinSig-1) * dX + fracBkgBin * dX;
      // printf("  Bin: %3d    fracBin: %4.2f     SigEffAtBkgTarget: %5.3f    CutValue: %6.3f \n",
      //             ibinSig, fracBkgBin, SigEffAtBkgTarget, CutValueAtBkgTarget);

    } else {
      printf("ERROR: Signal and Background histograms have different ranges! \n");
      return;
    }
  } else {
    printf("ERROR: Signal and Background histograms have different binning! \n");
    return;
  }

  return;
}
// ------------------------------------------------------------------- //




// ------------------------------------------------------------------- //
void calcROC2(const TH1D* &histSig, const TH1D* &histBkg, TGraph* &graphROC) {
// ------------------------------------------------------------------- //
// Function is simply a cross check that the above calculation was correct!!!

  // Loop over histogram bins and calculate vectors of efficiencies:
  // ---------------------------------------------------------------
  vector<double> effSig, effBkg;
  if (histSig->GetNbinsX() == histBkg->GetNbinsX()) {
    int Nbins = histSig->GetNbinsX();
    if (fabs(histSig->GetXaxis()->GetXmax() - histBkg->GetXaxis()->GetXmax()) < 0.0001 &&
	fabs(histSig->GetXaxis()->GetXmin() - histBkg->GetXaxis()->GetXmin()) < 0.0001) {

      double intSig = 0.0;
      double intBkg = 0.0;
      for (int ibin=0; ibin<Nbins+2; ibin++) {
	intSig += histSig->GetBinContent(ibin);
	intBkg += histBkg->GetBinContent(ibin);
      }

      double sumSig = 0.0;
      double sumBkg = 0.0;
      effSig.push_back(0.0);
      effBkg.push_back(0.0);
      for (int ibin=Nbins+1; ibin>=0; ibin--) {
	sumSig += histSig->GetBinContent(ibin);
	sumBkg += histBkg->GetBinContent(ibin);
	effSig.push_back(sumSig/intSig);
	effBkg.push_back(sumBkg/intBkg);
	printf("  bin %3d:  %9.3f (%9.3f)   %9.3f (%9.3f) \n", ibin, sumSig, intSig, sumBkg, intBkg);
      }

      // Make TGraph of vectors:
      // -----------------------
      graphROC = new TGraph(int(effSig.size()), &effSig[0], &effBkg[0]);
      
    } else {
      printf("ERROR: Signal and Background histograms have different ranges! \n");
      return;
    }
  } else {
    printf("ERROR: Signal and Background histograms have different binning! \n");
    return;
  }

  return;
}
// ------------------------------------------------------------------- //




// ------------------------------------------------------------------- //
void PIDresults() {
// ------------------------------------------------------------------- //
  gROOT->Reset();
  gStyle->SetOptStat("");
  gStyle->SetOptFit(0);

  char name[80];
  bool verbose = true;
  bool SavePlots = false;

  TFile *fileDataZee   = TFile::Open("GetZmumu.DATA_Zee.Ref13.root");
  TFile *fileDataZmumu = TFile::Open("GetZmumu.DATA_Zmumu.Ref13.root");
  TFile *fileMcZee   = TFile::Open("hist-Zee.root");
  TFile *fileMcZmumu = TFile::Open("hist-Zmumu.root");

  TLatex *text = new TLatex();
  text->SetNDC();
  text->SetTextFont(72);
  text->SetTextColor(1);

  const int Ndata = 2;
  char* name_data[Ndata] = {"Data12", "MC15"};
  const int Nparts = 3;
  char* name_part[Nparts] = {"Barrel", "EndcapA", "EndcapB"};
  const int Ntype = 2;     // 0: Electrons, 1: Muons
  const int Nmeth = 3;     // 0: fHT, 1: LLH, 2: Alternative-LLH (Data: NoOcc, MC: With MC par)
  const int Neta = 5;
  char* name_etapart[Neta] = {"Barrel", "B-EA trans.", "EndcapA", "EA-EB trans.", "EndcapB"};
  const int Nocc = 10;

  // const int Ntypes = 2;
  // double colorindex[Ntypes] = {kRed, kBlue};
  // double markerindex[Ntypes] = {20, 21};
  // double styleindex[Ntypes] = {3, 3};


  // ------------------------------------------------------------------------------ //
  // Reading and preparing histograms:
  // ------------------------------------------------------------------------------ //

  // Define histograms (first: Data/MC, second: Zee and Zmumu, third: fHT and PID):
  TH3D* h3_dist[2][2][3];
  TH1D* h1_eta[2][2][3][5];
  TH1D* h1_etaocc[2][2][3][5][10];

  // Read 3D histograms for Data12:
  int data = 0;  // Data12
  h3_dist[data][0][0] = (TH3D*)fileDataZee->Get("PIDresults_Cut20hits/Hist_fHTvsEtaOcc");
  h3_dist[data][1][0] = (TH3D*)fileDataZmumu->Get("PIDresults_Cut20hits/Hist_fHTvsEtaOcc");
  h3_dist[data][0][1] = (TH3D*)fileDataZee->Get("PIDresults_Cut20hits/Hist_pEL0vsEtaOcc");
  h3_dist[data][1][1] = (TH3D*)fileDataZmumu->Get("PIDresults_Cut20hits/Hist_pEL0vsEtaOcc");
  h3_dist[data][0][2] = (TH3D*)fileDataZee->Get("PIDresults_Cut20hits/Hist_pEL1vsEtaOcc");
  h3_dist[data][1][2] = (TH3D*)fileDataZmumu->Get("PIDresults_Cut20hits/Hist_pEL1vsEtaOcc");
  printf("  Entries for Data:   fHT:  Zee=%8d    Zmumu=%8d      LLH:  Zee=%8d    Zmumu=%8d \n",
	 h3_dist[data][0][0]->GetEntries(), h3_dist[data][1][0]->GetEntries(),
	 h3_dist[data][0][1]->GetEntries(), h3_dist[data][1][1]->GetEntries());

  // Read 3D histograms for MC15:
  data = 1;    // MC15
  h3_dist[data][0][0] = (TH3D*)fileMcZee->Get("h3_fHTMB_OT");
  h3_dist[data][1][0] = (TH3D*)fileMcZmumu->Get("h3_fHTMB_OT");
  h3_dist[data][0][1] = (TH3D*)fileMcZee->Get("h3_LH_OT");
  h3_dist[data][1][1] = (TH3D*)fileMcZmumu->Get("h3_LH_OT");
  h3_dist[data][0][2] = (TH3D*)fileMcZee->Get("h3_LHMC_OT");
  h3_dist[data][1][2] = (TH3D*)fileMcZmumu->Get("h3_LHMC_OT");
  printf("  Entries for MC:   fHT:  Zee=%8d    Zmumu=%8d      LLH:  Zee=%8d    Zmumu=%8d \n",
	 h3_dist[data][0][0]->GetEntries(), h3_dist[data][1][0]->GetEntries(),
	 h3_dist[data][0][1]->GetEntries(), h3_dist[data][1][1]->GetEntries());


  // Project histograms into 1D distributions:
  // ------------------------------------------
  for (int idata=0; idata<Ndata; idata++) {
    for (int itype=0; itype<Ntype; itype++) {
      for (int imeth=0; imeth<Nmeth; imeth++) {
	for (int ieta=0; ieta<Neta; ieta++) {

	  // Integrated over all occupancies:
	  sprintf(name, "h1_data%1d_type%1d_meth%1d_eta%1d", idata, itype, imeth, ieta);
	  h1_eta[idata][itype][imeth][ieta] =
	    (TH1D*)h3_dist[idata][itype][imeth]->ProjectionY(name, ieta+1, ieta+1, 1, 50);
	  
	  // Also as a function of OT (Occupancy-per-Track) in bins of 0.05:
	  for (int iOT=0; iOT<Nocc; iOT++) {
	    sprintf(name, "h1_data%1d_type%1d_meth%1d_eta%1d_occ%02d", idata, itype, imeth, ieta, iOT);
	    h1_etaocc[idata][itype][imeth][ieta][iOT] =
	      (TH1D*)h3_dist[idata][itype][imeth]->ProjectionY(name, ieta+1, ieta+1, iOT*5+1, iOT*5+5);
	  }
	}
      }
    }
  }




  // -------------------------------------------------------------------------- //
  // Distribution:
  // -------------------------------------------------------------------------- //

  int PIDmethod = 1;   // LLH
  int etabin = 0;      // Barrel

  // PID distribution for pEL:
  TCanvas *c1 = new TCanvas("c1", "", 30, 30, 1000, 600);
  c1->SetLogy();

  // Data:
  h1_eta[0][0][PIDmethod][etabin]->GetXaxis()->SetTitle("Electron probability (integrated over all occupancies)");
  h1_eta[0][0][PIDmethod][etabin]->SetMinimum(50.0);
  h1_eta[0][0][PIDmethod][etabin]->SetLineStyle(3);
  h1_eta[0][0][PIDmethod][etabin]->SetLineWidth(2);
  h1_eta[0][0][PIDmethod][etabin]->SetLineColor(kRed);
  h1_eta[0][0][PIDmethod][etabin]->Draw("");
  h1_eta[0][1][PIDmethod][etabin]->SetLineStyle(3);
  h1_eta[0][1][PIDmethod][etabin]->SetLineWidth(2);
  h1_eta[0][1][PIDmethod][etabin]->SetLineColor(kBlue);
  h1_eta[0][1][PIDmethod][etabin]->Draw("same");

  // MC:
  h1_eta[1][0][PIDmethod][etabin]->SetLineWidth(2);
  h1_eta[1][0][PIDmethod][etabin]->SetLineColor(kRed);
  h1_eta[1][0][PIDmethod][etabin]->Draw("same");
  h1_eta[1][1][PIDmethod][etabin]->SetLineWidth(2);
  h1_eta[1][1][PIDmethod][etabin]->SetLineColor(kBlue);
  h1_eta[1][1][PIDmethod][etabin]->Draw("same");

  // Legend:
  TLegend* legend = new TLegend(0.20, 0.55, 0.55, 0.75);
  legend->SetFillColor(0);
  legend->AddEntry(h1_eta[0][0][PIDmethod][etabin], " Data12 Electrons (20+ TRT hits)", "PL");
  legend->AddEntry(h1_eta[0][1][PIDmethod][etabin], " Data12 Muons (20+ TRT hits)", "PL");
  legend->AddEntry(h1_eta[1][0][PIDmethod][etabin], " MC15 Electrons (20+ TRT hits)", "PL");
  legend->AddEntry(h1_eta[1][1][PIDmethod][etabin], " MC15 Muons (20+ TRT hits)", "PL");
  legend->SetLineColor(1);
  legend->Draw();
    
  // Text:
  sprintf(name, "ATLAS TRT - %s", name_etapart[etabin]);
  text->SetTextSize(0.06);
  text->DrawLatex(0.20, 0.83, name);
  text->SetTextSize(0.035);
  text->DrawLatex(0.20, 0.79, "Integrated over all occupancies");

  sprintf(name, "figures/h1_MCandData_PIDdist_etabin%1d.pdf", etabin);
  if (SavePlots) c1->SaveAs(name);



  // --------------------------------------------------------------------- //
  // Calculate ROC curves:
  // --------------------------------------------------------------------- //

  TGraph* graphROC_eta[2][3][5];      // 1: Data/MC, 2: fHT/LLH/aLLH, 3: EtaBin
  double TargetSigEff = 0.90;
  double TargetBkgEff = 0.10;

  TH1D* Hist_Performance[Ndata][Nmeth];
  TH1D* Hist_Improvement[Ndata];
  for (int idata=0; idata<Ndata; idata++) {
    for (int imeth=0; imeth<Nmeth; imeth++) {
      sprintf(name, "Hist_Performance_%s_method%1d", name_data[idata], imeth);
      Hist_Performance[idata][imeth] = new TH1D(name, name, 5, -0.5, 4.5);
    }
    sprintf(name, "Hist_Improvement_%s", name_data[idata]);
    Hist_Improvement[idata] = new TH1D(name, name, 5, -0.5, 4.5);
  }


  double BkgEffAtSigTarget_pEL0 = 1.0;
  for (int idata=0; idata<Ndata; idata++) {
    for (int ieta=0; ieta<Neta; ieta++) {
      for (int imeth=0; imeth<Nmeth; imeth++) {
	double BkgEffAtSigTarget_pEL, CutValueAtSigTarget_pEL;
	double SigEffAtBkgTarget_pEL, CutValueAtBkgTarget_pEL;

	calcROC(h1_eta[idata][0][imeth][ieta], h1_eta[idata][1][imeth][ieta], graphROC_eta[idata][imeth][ieta],
		TargetSigEff, BkgEffAtSigTarget_pEL, CutValueAtSigTarget_pEL,
		TargetBkgEff, SigEffAtBkgTarget_pEL, CutValueAtBkgTarget_pEL);
	if (verbose) {
	  printf("  %1d %1d %1d: Target sig eff %5.3f:     At sig target:   Bkg eff = %5.3f    Cut value = %6.3f \n",
		 idata, imeth, ieta, TargetSigEff, BkgEffAtSigTarget_pEL, CutValueAtSigTarget_pEL);
	}
	
	if (imeth == 0) BkgEffAtSigTarget_pEL0 = BkgEffAtSigTarget_pEL;
	Hist_Performance[idata][imeth]->SetBinContent(ieta+1, BkgEffAtSigTarget_pEL);
      }
      Hist_Improvement[idata]->SetBinContent(ieta+1, BkgEffAtSigTarget_pEL0/BkgEffAtSigTarget_pEL);
    }
  }



  // Plot Performance and Improvement curves:
  TCanvas *c1b[2][2];
  for (int idata=0; idata<Ndata; idata++) {
    sprintf(name, "c1b_perf_%s", name_data[idata]);
    c1b[idata][0] = new TCanvas(name, "", 50+20*idata, 50+20*idata, 800, 500);
    c1b[idata][0]->SetTitle();

    Hist_Performance[idata][0]->SetMinimum(0.0);
    Hist_Performance[idata][0]->SetTitle("Performance of fHT and LLH methods vs. TRT parts in |#eta|");
    Hist_Performance[idata][0]->GetXaxis()->SetTitle("TRT part in |#eta|");
    Hist_Performance[idata][0]->GetYaxis()->SetTitle("Muon efficiency at 90% electron efficiency");
    Hist_Performance[idata][0]->SetLineColor(kRed);
    Hist_Performance[idata][0]->SetLineWidth(2);
    Hist_Performance[idata][0]->Draw("");
    Hist_Performance[idata][1]->SetLineColor(kBlue);
    Hist_Performance[idata][1]->SetLineWidth(2);
    Hist_Performance[idata][1]->Draw("same");

    // Legend:
    TLegend* legend = new TLegend(0.15, 0.15, 0.55, 0.30);
    legend->SetFillColor(0);
    legend->AddEntry(Hist_Performance[idata][0], "Fraction HT hits - fHT", "PL");
    legend->AddEntry(Hist_Performance[idata][1], "Likelihood approach - LLH", "PL");
    legend->SetLineColor(1);
    legend->Draw();

    // Text:
    sprintf(name, "ATLAS TRT - %s", name_data[idata]);
    text->SetTextSize(0.06);
    text->DrawLatex(0.55, 0.83, name);
    
    sprintf(name, "figures/Performance_%s.pdf", name_data[idata]);
    if (SavePlots) c1b[idata][0]->SaveAs(name);
  }




  // ----------------------------------------------------------------------------------------------
  // Plot ROC curves of example - DATA vs. MC:
  // ----------------------------------------------------------------------------------------------

  // etabin = 0;

  double xmin = 0.50;
  double xmax = 0.97;
  double ymin = 0.0;
  // double ymin = graphROC_eta[1][0][etabin]->Eval(xmin);
  double ymax = graphROC_eta[1][0][etabin]->Eval(xmax);
  printf("  xmin = %5.2f   xmax = %5.2f     ymin = %6.4f ymax = %6.4f \n", xmin, xmax, ymin, ymax);
  // if (ymin < 0.001) ymin = 0.001;
    
  TCanvas *c2 = new TCanvas("c2", "", 50, 50, 1200, 800);
  // c2->SetLogy();
  
  TH1F* h2 = c2->DrawFrame(xmin, 0.0, xmax+0.01, ymax);
  // h2->GetXaxis()->CenterTitle();
  h2->SetTitleOffset(1.1,"x");
  h2->SetXTitle("Electron efficiency");
  h2->SetTitleOffset(1.1,"y");
  h2->SetYTitle("Muon efficiency");

  const int Ntypes = 2;
  double colorindex[Ntypes] = {kRed, kBlue};
  double markerindex[Ntypes] = {20, 21};
  double styleindex[Ntypes] = {3, 3};

  // MC plots:
  int i = 0;
  int j = 0;
  graphROC_eta[1][0][etabin]->SetLineWidth(2);
  graphROC_eta[1][0][etabin]->SetLineColor(colorindex[i]);
  graphROC_eta[1][0][etabin]->SetLineStyle(0);
  graphROC_eta[1][0][etabin]->SetMarkerStyle(markerindex[j]);
  graphROC_eta[1][0][etabin]->SetMarkerSize(0.8);
  graphROC_eta[1][0][etabin]->SetMarkerColor(colorindex[i]);
  graphROC_eta[1][0][etabin]->Draw("PL same");
  
  i = 1;
  graphROC_eta[1][1][etabin]->SetLineWidth(2);
  graphROC_eta[1][1][etabin]->SetLineColor(colorindex[i]);
  graphROC_eta[1][1][etabin]->SetLineStyle(0);
  graphROC_eta[1][1][etabin]->SetMarkerStyle(markerindex[j]);
  graphROC_eta[1][1][etabin]->SetMarkerSize(0.8);
  graphROC_eta[1][1][etabin]->SetMarkerColor(colorindex[i]);
  graphROC_eta[1][1][etabin]->Draw("PL same");

  // Data plots:
  i = 0;
  j = 1;
  graphROC_eta[0][0][etabin]->SetLineWidth(2);
  graphROC_eta[0][0][etabin]->SetLineColor(colorindex[i]);
  graphROC_eta[0][0][etabin]->SetLineStyle(3);
  graphROC_eta[0][0][etabin]->SetMarkerStyle(markerindex[j]);
  graphROC_eta[0][0][etabin]->SetMarkerSize(0.8);
  graphROC_eta[0][0][etabin]->SetMarkerColor(colorindex[i]);
  graphROC_eta[0][0][etabin]->Draw("PL same");
  
  i = 1;
  graphROC_eta[0][1][etabin]->SetLineWidth(2);
  graphROC_eta[0][1][etabin]->SetLineColor(colorindex[i]);
  graphROC_eta[0][1][etabin]->SetLineStyle(3);
  graphROC_eta[0][1][etabin]->SetMarkerStyle(markerindex[j]);
  graphROC_eta[0][1][etabin]->SetMarkerSize(0.8);
  graphROC_eta[0][1][etabin]->SetMarkerColor(colorindex[i]);
  graphROC_eta[0][1][etabin]->Draw("PL same");


  // Legend:
  TLegend* legend = new TLegend(0.15, 0.65, 0.60, 0.80);
  legend->SetFillColor(0);
  legend->AddEntry(graphROC_eta[1][0][etabin], "MC15: Fraction HT hits - All hits used", "PL");
  legend->AddEntry(graphROC_eta[1][1][etabin], "MC15: Likelihood PID - Using 2D fit", "PL");
  legend->AddEntry(graphROC_eta[0][0][etabin], "Data12: Fraction HT hits - All hits used", "PL");
  legend->AddEntry(graphROC_eta[0][1][etabin], "Data12: Likelihood PID - Using 2D fit", "PL");
  legend->SetLineColor(1);
  legend->Draw();
    
  // Text:
  sprintf(name, "ATLAS TRT %s", name_etapart[etabin]);
  text->SetTextSize(0.06);
  text->DrawLatex(0.20, 0.83, name);

  sprintf(name, "figures/h1_%s_PID_ROCcurves_etabin%1d.pdf", name_data[data], etabin);
  c2->Update();
  if (SavePlots) c2->SaveAs(name);





  // ----------------------------------------------------------------------------------------------
  // Calculate ROC curves for each bin in OT for all methods, and...
  // Plot separation as a function of OT for different methods.
  // ----------------------------------------------------------------------------------------------

  TGraph* graphROCocc[Ndata][Nmeth][Neta][Nocc];

  double effBkg[Ndata][Nmeth][Neta][Nocc];;
  double occ[Nocc];
  for (int iocc=0; iocc<Nocc; iocc++) occ[iocc] = (double(iocc)+0.5) / double(Nocc);

  for (int idata=0; idata<Ndata; idata++) {
    for (int ieta=0; ieta<Neta; ieta++) {
      for (int imeth=0; imeth<Nmeth; imeth++) {
	for (int iocc=0; iocc<Nocc; iocc++) {
	  double BkgEffAtSigTarget_pEL, CutValueAtSigTarget_pEL;
	  double SigEffAtBkgTarget_pEL, CutValueAtBkgTarget_pEL;

	  if (h1_etaocc[idata][0][imeth][ieta][iocc]->GetEntries() > 100 &&
	      h1_etaocc[idata][1][imeth][ieta][iocc]->GetEntries() > 100) {

	    calcROC(h1_etaocc[idata][0][imeth][ieta][iocc], h1_etaocc[idata][1][imeth][ieta][iocc],
		    graphROCocc[idata][imeth][ieta][iocc],
		    TargetSigEff, BkgEffAtSigTarget_pEL, CutValueAtSigTarget_pEL,
		    TargetBkgEff, SigEffAtBkgTarget_pEL, CutValueAtBkgTarget_pEL);
	    effBkg[idata][imeth][ieta][iocc] = BkgEffAtSigTarget_pEL;

	    if (verbose) {
	      printf("  %1d %1d %1d %2d: Target sig eff %5.3f:     At sig target:   Bkg eff = %5.3f    Cut value = %6.3f \n",
		     idata, imeth, ieta, iocc, TargetSigEff, BkgEffAtSigTarget_pEL, CutValueAtSigTarget_pEL);
	    }
	  } else {
	    if (verbose) printf("    Case of no/low stat: %2d %2d %2d \n", idata, ieta, imeth, iocc);
	    effBkg[idata][imeth][ieta][iocc] = -1.0;
	  }
	
	}
      }
    }
  }

  // Make new graphs of background efficiency at 90% signal efficiency:
  TGraph* graphBkg[Ndata][Nmeth][Neta];
  for (int idata=0; idata<Ndata; idata++) {
    for (int ieta=0; ieta<Neta; ieta++) {
      for (int imeth=0; imeth<Nmeth; imeth++) {
	graphBkg[idata][imeth][ieta] = new TGraph(Nocc, occ, effBkg[idata][imeth][ieta]);
      }
    }
  }


  // Plot illustrative examples of these:
  // ------------------------------------
  TCanvas *c3 = new TCanvas("c3", "", 50, 50, 1000, 750);
  
  TH1F* h3 = c3->DrawFrame(0.0, 0.0, 0.70, 0.45);
  // h3->GetXaxis()->CenterTitle();
  h3->SetTitleOffset(1.1,"x");
  h3->SetXTitle("Occupancy bin");
  h3->SetTitleOffset(1.1,"y");
  h3->SetYTitle("Muon efficiency at 90% electron efficiency");

  int idata = 1;  // We take MC15
  int imeth = 1;  // We take LLH
  int ieta  = 0;  // We take Barrel
  TF1* fitB = new TF1("fitB", "pol1", 0.03, 0.67);
  fitB->SetLineColor(kRed);
  graphBkg[idata][imeth][ieta]->SetLineColor(kRed);
  graphBkg[idata][imeth][ieta]->SetLineWidth(2);
  graphBkg[idata][imeth][ieta]->SetLineStyle(0);
  graphBkg[idata][imeth][ieta]->SetMarkerColor(kRed);
  graphBkg[idata][imeth][ieta]->SetMarkerStyle(20);
  graphBkg[idata][imeth][ieta]->SetMarkerSize(1.2);
  graphBkg[idata][imeth][ieta]->Fit("fitB", "R");
  graphBkg[idata][imeth][ieta]->Draw("same P");

  ieta  = 2;  // We take EndcapA
  TF1* fitEA = new TF1("fitEA", "pol1", 0.03, 0.57);
  fitEA->SetLineColor(kMagenta);
  graphBkg[idata][imeth][ieta]->SetLineColor(kMagenta);
  graphBkg[idata][imeth][ieta]->SetLineWidth(2);
  graphBkg[idata][imeth][ieta]->SetLineStyle(0);
  graphBkg[idata][imeth][ieta]->SetMarkerColor(kMagenta);
  graphBkg[idata][imeth][ieta]->SetMarkerStyle(22);
  graphBkg[idata][imeth][ieta]->SetMarkerSize(1.2);
  graphBkg[idata][imeth][ieta]->Fit("fitEA", "R");
  graphBkg[idata][imeth][ieta]->Draw("same P");

  ieta  = 4;  // We take EndcapB
  TF1* fitEB = new TF1("fitEB", "pol1", 0.03, 0.67);
  fitEB->SetLineColor(kBlue);
  graphBkg[idata][imeth][ieta]->SetLineColor(kBlue);
  graphBkg[idata][imeth][ieta]->SetLineWidth(2);
  graphBkg[idata][imeth][ieta]->SetLineStyle(0);
  graphBkg[idata][imeth][ieta]->SetMarkerColor(kBlue);
  graphBkg[idata][imeth][ieta]->SetMarkerStyle(21);
  graphBkg[idata][imeth][ieta]->SetMarkerSize(1.2);
  graphBkg[idata][imeth][ieta]->Fit("fitEB", "R");
  graphBkg[idata][imeth][ieta]->Draw("same P");

  // Legend:
  TLegend* legend = new TLegend(0.15, 0.65, 0.60, 0.80);
  legend->SetFillColor(0);
  legend->SetLineColor(1);
  legend->AddEntry(graphBkg[idata][imeth][0], " Barrel", "P");
  legend->AddEntry(graphBkg[idata][imeth][1], " Endcap A-wheels", "P");
  legend->AddEntry(graphBkg[idata][imeth][2], " Endcap B-wheels", "P");
  legend->Draw();
    
  // Text:
  sprintf(name, "ATLAS TRT - MC15");
  text->SetTextSize(0.06);
  text->DrawLatex(0.20, 0.83, name);

  c3->Update();
  if (SavePlots) c3->SaveAs("figures/PIDperformanceVsAveOccBinned.pdf");



  // ----------------------------------------------------------------------------------------------
  // Plot ROC curves of example for MC (and one data) - various occupancies:
  // ----------------------------------------------------------------------------------------------

  data = 1;   // We choose MC
  // etabin = 0;
  int occbin0 = 0;
  int occbin1 = 3;
  int occbin2 = 6;

  double xmin = 0.50;
  double xmax = 0.97;
  double ymin = 0.0;
  // double ymin = graphROCocc[data][0][etabin][1]->Eval(xmin);
  double ymax = graphROCocc[data][0][etabin][1]->Eval(xmax);
  // printf("  xmin = %5.2f   xmax = %5.2f     ymin = %6.4f ymax = %6.4f \n", xmin, xmax, ymin, ymax);
  // if (ymin < 0.001) ymin = 0.001;
    
  TCanvas *c4 = new TCanvas("c4", "", 50, 50, 1200, 800);
  // c4->SetLogy();
  
  TH1F* h4 = c4->DrawFrame(xmin, 0.0, xmax+0.01, ymax);
  // h4->GetXaxis()->CenterTitle();
  h4->SetTitleOffset(1.1,"x");
  h4->SetXTitle("Electron efficiency");
  h4->SetTitleOffset(1.1,"y");
  h4->SetYTitle("Muon efficiency");


  // MC15: Low occupancy:
  graphROCocc[data][0][etabin][occbin0]->SetLineWidth(2);
  graphROCocc[data][0][etabin][occbin0]->SetLineColor(kBlue-2);
  graphROCocc[data][0][etabin][occbin0]->SetLineStyle(3);
  graphROCocc[data][0][etabin][occbin0]->SetMarkerStyle(24);
  graphROCocc[data][0][etabin][occbin0]->SetMarkerSize(0.8);
  graphROCocc[data][0][etabin][occbin0]->SetMarkerColor(kBlue-2);
  graphROCocc[data][0][etabin][occbin0]->Draw("PL same");
  
  graphROCocc[data][1][etabin][occbin0]->SetLineWidth(2);
  graphROCocc[data][1][etabin][occbin0]->SetLineColor(kBlue);
  graphROCocc[data][1][etabin][occbin0]->SetLineStyle(0);
  graphROCocc[data][1][etabin][occbin0]->SetMarkerStyle(20);
  graphROCocc[data][1][etabin][occbin0]->SetMarkerSize(1.0);
  graphROCocc[data][1][etabin][occbin0]->SetMarkerColor(kBlue);
  graphROCocc[data][1][etabin][occbin0]->Draw("PL same");

  // MC15: High occupancy:
  graphROCocc[data][0][etabin][occbin2]->SetLineWidth(2);
  graphROCocc[data][0][etabin][occbin2]->SetLineColor(kRed-2);
  graphROCocc[data][0][etabin][occbin2]->SetLineStyle(3);
  graphROCocc[data][0][etabin][occbin2]->SetMarkerStyle(25);
  graphROCocc[data][0][etabin][occbin2]->SetMarkerSize(0.8);
  graphROCocc[data][0][etabin][occbin2]->SetMarkerColor(kRed-2);
  graphROCocc[data][0][etabin][occbin2]->Draw("PL same");
  
  graphROCocc[data][1][etabin][occbin2]->SetLineWidth(2);
  graphROCocc[data][1][etabin][occbin2]->SetLineColor(kRed);
  graphROCocc[data][1][etabin][occbin2]->SetLineStyle(0);
  graphROCocc[data][1][etabin][occbin2]->SetMarkerStyle(21);
  graphROCocc[data][1][etabin][occbin2]->SetMarkerSize(1.0);
  graphROCocc[data][1][etabin][occbin2]->SetMarkerColor(kRed);
  graphROCocc[data][1][etabin][occbin2]->Draw("PL same");

  // Data12 (LLH only): Medium occupancy (too low stat for low occupancy):
  graphROCocc[0][1][etabin][1]->SetLineWidth(2);
  graphROCocc[0][1][etabin][1]->SetLineColor(kMagenta);
  graphROCocc[0][1][etabin][1]->SetLineStyle(0);
  graphROCocc[0][1][etabin][1]->SetMarkerStyle(22);
  graphROCocc[0][1][etabin][1]->SetMarkerSize(0.8);
  graphROCocc[0][1][etabin][1]->SetMarkerColor(kMagenta);
  graphROCocc[0][1][etabin][1]->Draw("PL same");

  // Legend:
  TLegend* legend = new TLegend(0.15, 0.60, 0.60, 0.80);
  legend->SetFillColor(0);
  legend->AddEntry(graphROCocc[data][0][etabin][occbin0], "MC15: fHT at low occupancy (0.0-0.1)", "PL");
  legend->AddEntry(graphROCocc[data][1][etabin][occbin0], "MC15: LLH at low occupancy (0.0-0.1)", "PL");
  legend->AddEntry(graphROCocc[data][0][etabin][occbin2], "MC15: fHT at high occupancy (0.6-0.7)", "PL");
  legend->AddEntry(graphROCocc[data][1][etabin][occbin2], "MC15: LLH at high occupancy (0.6-0.7)", "PL");
  legend->AddEntry(graphROCocc[0][1][etabin][1], "Data12: LLH at low occupancy (0.1-0.2)", "PL");
  legend->SetLineColor(1);
  legend->Draw();
    
  // Text:
  sprintf(name, "ATLAS TRT %s", name_etapart[etabin]);
  text->SetTextSize(0.06);
  text->DrawLatex(0.20, 0.83, name);

  sprintf(name, "figures/h1_%s_ROCcurves_CompareOccupancies_etabin%1d.pdf", name_data[data], etabin);
  c4->Update();
  if (SavePlots) c4->SaveAs(name);



  // ----------------------------------------------------------------------------------------------
  // Plot ROC curves for MC - Variation in variables it is based on (from data vs. MC):
  // ----------------------------------------------------------------------------------------------

  // etabin = 0;

  double xmin = 0.50;
  double xmax = 0.97;
  double ymin = 0.0;
  double ymax = graphROC_eta[1][1][etabin]->Eval(xmax);
  // printf("  xmin = %5.2f   xmax = %5.2f     ymin = %6.4f ymax = %6.4f \n", xmin, xmax, ymin, ymax);
    
  TCanvas *c5 = new TCanvas("c5", "", 50, 50, 1200, 800);
  // c5->SetLogy();
  
  TH1F* h5 = c5->DrawFrame(xmin, 0.0, xmax+0.01, ymax);
  // h5->GetXaxis()->CenterTitle();
  h5->SetTitleOffset(1.1,"x");
  h5->SetXTitle("Electron efficiency");
  h5->SetTitleOffset(1.1,"y");
  h5->SetYTitle("Muon efficiency");

  // MC fHT:
  graphROC_eta[1][0][etabin]->SetLineWidth(2);
  graphROC_eta[1][0][etabin]->SetLineColor(kRed);
  graphROC_eta[1][0][etabin]->SetLineStyle(3);
  graphROC_eta[1][0][etabin]->SetMarkerStyle(26);
  graphROC_eta[1][0][etabin]->SetMarkerSize(1.0);
  graphROC_eta[1][0][etabin]->SetMarkerColor(kRed);
  graphROC_eta[1][0][etabin]->Draw("PL same");

  // MC LLH with MC onset parameters:
  graphROC_eta[1][1][etabin]->SetLineWidth(2);
  graphROC_eta[1][1][etabin]->SetLineColor(kBlue);
  graphROC_eta[1][1][etabin]->SetLineStyle(0);
  graphROC_eta[1][1][etabin]->SetMarkerStyle(24);
  graphROC_eta[1][1][etabin]->SetMarkerSize(1.0);
  graphROC_eta[1][1][etabin]->SetMarkerColor(kBlue);
  graphROC_eta[1][1][etabin]->Draw("PL same");

  // MC LLH with data onset parameters:
  graphROC_eta[1][2][etabin]->SetLineWidth(2);
  graphROC_eta[1][2][etabin]->SetLineColor(kMagenta);
  graphROC_eta[1][2][etabin]->SetLineStyle(0);
  graphROC_eta[1][2][etabin]->SetMarkerStyle(25);
  graphROC_eta[1][2][etabin]->SetMarkerSize(1.0);
  graphROC_eta[1][2][etabin]->SetMarkerColor(kMagenta);
  graphROC_eta[1][2][etabin]->Draw("PL same");
  
  // Legend:
  TLegend* legend = new TLegend(0.15, 0.65, 0.60, 0.80);
  legend->SetFillColor(0);
  legend->AddEntry(graphROC_eta[1][0][etabin], "MC15 fHT (Independent of parameters)", "PL");
  legend->AddEntry(graphROC_eta[1][2][etabin], "MC15 LLH: Onset parameters from Data12", "PL");
  legend->AddEntry(graphROC_eta[1][1][etabin], "MC15 LLH: Onset parameters from MC15", "PL");
  legend->SetLineColor(1);
  legend->Draw();
    
  // Text:
  sprintf(name, "ATLAS TRT %s", name_etapart[etabin]);
  text->SetTextSize(0.06);
  text->DrawLatex(0.20, 0.83, name);

  sprintf(name, "figures/StabilityStudyMC_ROCcurves_etabin%1d.pdf", etabin);
  c5->Update();
  if (SavePlots) c5->SaveAs(name);



}
