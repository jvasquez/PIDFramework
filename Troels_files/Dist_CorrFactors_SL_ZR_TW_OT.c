// ------------------------------------------------------------------- //
/*

  ROOT macro for producing TRT plots used in PID from Zee and Zmumu 2012 data.

  Author: Troels C. Petersen (NBI)
  Email:  petersen@nbi.dk
  Date:   Started 02-07-2014

  Run by: >root -l Dist_CorrFactors_SL_ZR_TW_OT.c
*/
// ------------------------------------------------------------------- //


double sqr(double a) {return a*a;}




// New and much improved onset function (1D):
// ------------------------------------------------------------------- //
double func_onset2(double *x, double *par) {
// ------------------------------------------------------------------- //
  if (x[0] < 0.0) return 0.0;

  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par[4])/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par[4])/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par[1] - par[4])/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par[1]);

  if (log10(x[0])      < par[0]) return pHT_dEdx;
  else if (log10(x[0]) > par[1]) return pHT_HG;
  else                           return pHT_TR;
}



// ------------------------------------------------------------------- //
void Dist_CorrFactors_SL_ZR_TW_OT() {
// ------------------------------------------------------------------- //
  gROOT->Reset();
  gStyle->SetOptStat("");
  gStyle->SetOptFit(1111);
  gStyle->SetStatY(0.55);                
  gStyle->SetStatX(0.96);                
  gStyle->SetStatW(0.20);                
  gStyle->SetStatH(0.12);

  char name[80], name2[80], nameLT[80], nameHT[80];
  bool SavePlots = false;
  bool UseMiddleHTbin = true;

  double SystError = 0.0025;    // Don't trust very small errors! (0.005 gives reasonable Chi2 values).

  int Npart = 3;
  int NpartMin = 0;

  int Ndata = 2;
  char* dataname[2] = {"Data12", "MC15"};
  int Ngas = 1;
  char* gasname[1] = {"Xenon"};
  int Ntype = 2;
  char* typename[2] = {"Electrons", "NonElecs"};
  char* typename2[2] = {"Zee", "Zmm"};
  int Npart = 3;
  char* partname[3] = {"Barrel", "EndcapA", "EndcapB"};
  char* partnameMC[3]   = {"BRL", "ECA", "ECB"};
  char* partnameData[3] = {"B",   "EA",  "EB"};

  int Nvar = 4;
  char* varname[4]     = {"SL", "ZR", "TW", "OR"};
  char* varnameData[4] = {"SL", "ZRpos", "Rtrk", "Occ"};
  int nbinsvar[3][4]  = {{  73,    36,   44,   50}, {  96,     40,   44,   50}, {  64,     40,   44,   50}};
  double minvar[3][4] = {{-0.5,   0.0,  0.0,  0.0}, {-0.5,  630.0,  0.0,  0.0}, {-0.5,  630.0,  0.0,  0.0}};
  double maxvar[3][4] = {{72.5, 720.0,  2.2,  1.0}, {95.5, 1030.0,  2.2,  1.0}, {63.5, 1030.0,  2.2,  1.0}};

  TLatex *text = new TLatex();
  text->SetNDC();
  text->SetTextFont(72);
  text->SetTextColor(1);



  // ------------------------------------------------------------------------------ //
  // Read all histograms/profiles:
  // ------------------------------------------------------------------------------ //

  // Define histograms (first index: Zee and Zmumu, second index: B, EA, EB, third index: SL, ZR, TW, OC:
  TH1D* h1[2][3][4][3];       // (fourth index: Data, MC, Data/MC)
  TH1D* h1_temp[2][3][4][2];  // (fourth index: nLT, nHT)


  // Read histograms and produce fHT ratios for data:
  TFile *fileDataZee   = TFile::Open("GetZmumu.DATA_Zee.Ref10.root");
  TFile *fileDataZmumu = TFile::Open("GetZmumu.DATA_Zmumu.Ref10.root");
  // NOTE: "Ref13" has the correct number of bins in ZR for endcap (40) and not 50.
  //       However, MC also has 50, and needs to be corrected before one can change.
  //       For the output, at manual (ugly) fix has been put in, so that 40 bins comes out.

  
  for (int ipart=0; ipart < Npart; ipart++) {
    for (int ivar=0; ivar < Nvar; ivar++) {
      
      // Electrons:
      sprintf(name, "MultiDimHists/Hist%s_Zee_nLT_%s", partnameData[ipart], varnameData[ivar]);
      h1_temp[0][ipart][ivar][0] = (TH1D*)fileDataZee->Get(name);
      if (UseMiddleHTbin) sprintf(name, "MultiDimHists/Hist%s_Zee_nHTMB_%s", partnameData[ipart], varnameData[ivar]);
      else                sprintf(name, "MultiDimHists/Hist%s_Zee_nHT_%s", partnameData[ipart], varnameData[ivar]);
      h1_temp[0][ipart][ivar][1] = (TH1D*)fileDataZee->Get(name);
      sprintf(name, "Hist%s_Zee_fHT_%s", partnameData[ipart], varnameData[ivar]);
      h1[0][ipart][ivar][0] = (TH1D*)h1_temp[0][ipart][ivar][1]->Clone(name);
      h1[0][ipart][ivar][0]->Divide(h1_temp[0][ipart][ivar][0]);

      // Muons:
      sprintf(name, "MultiDimHists/Hist%s_Zmm_nLT_%s", partnameData[ipart], varnameData[ivar]);
      h1_temp[1][ipart][ivar][0] = (TH1D*)fileDataZmumu->Get(name);
      if (UseMiddleHTbin) sprintf(name, "MultiDimHists/Hist%s_Zmm_nHTMB_%s", partnameData[ipart], varnameData[ivar]);
      else                sprintf(name, "MultiDimHists/Hist%s_Zmm_nHT_%s", partnameData[ipart], varnameData[ivar]);
      h1_temp[1][ipart][ivar][1] = (TH1D*)fileDataZmumu->Get(name);
      sprintf(name, "Hist%s_Zmm_fHT_%s", partname[ipart], varnameData[ivar]);
      h1[1][ipart][ivar][0] = (TH1D*)h1_temp[1][ipart][ivar][1]->Clone(name);
      h1[1][ipart][ivar][0]->Divide(h1_temp[1][ipart][ivar][0]);

      printf("  DATA: Part% 1d, var %1d:   EL entries: %9d     MU entries: %9d \n", ipart, ivar,
      	     h1_temp[0][ipart][ivar][0][0]->GetEntries(), h1_temp[1][ipart][ivar][0][0]->GetEntries());
    }
  }


  // Read histograms and produce fHT ratios for MC:
  TFile *fileMcZee   = TFile::Open("hist-Zee.root");
  TFile *fileMcZmumu = TFile::Open("hist-Zmumu.root");

  for (int ipart=0; ipart < Npart; ipart++) {
    for (int ivar=0; ivar < Nvar; ivar++) {
      
      // Electrons:
      if (!UseMiddleHTbin) {
	sprintf(name, "h_fHT_Xe_%s_%s", partnameMC[ipart], varname[ivar]);
	sprintf(name2, "hEL_fHT_Xe_%s_%s", partnameMC[ipart], varname[ivar]);
      } else {
	sprintf(name, "h_fHT_Xe_MBX_%s_%s", partnameMC[ipart], varname[ivar]);     // Middle bin only!
	sprintf(name2, "hEL_fHT_Xe_MBX_%s_%s", partnameMC[ipart], varname[ivar]);
      }
      h1[0][ipart][ivar][1] = ((TProfile*)fileMcZee->Get(name))->ProjectionX(name2);
	
      // Muons:
      if (!UseMiddleHTbin) {
	sprintf(name, "h_fHT_Xe_%s_%s", partnameMC[ipart], varname[ivar]);
	sprintf(name2, "hMU_fHT_Xe_%s_%s", partnameMC[ipart], varname[ivar]);
      } else {
	sprintf(name, "h_fHT_Xe_MBX_%s_%s", partnameMC[ipart], varname[ivar]);     // Middle bin only!
	sprintf(name2, "hMU_fHT_Xe_MBX_%s_%s", partnameMC[ipart], varname[ivar]);
      }
      h1[1][ipart][ivar][1] = ((TProfile*)fileMcZmumu->Get(name))->ProjectionX(name2);

      printf("  MC:   Part% 1d, var %1d:   EL entries: %9d     MU entries: %9d \n", ipart, ivar,
	     h1[0][ipart][ivar][1]->GetEntries(), h1[1][ipart][ivar][1]->GetEntries());
    }
  }

  printf("  Successfully read histograms! \n\n");



  // -------------------------------------------------------------------------- //
  // Correction distributions (both for data12 and MC15):
  // -------------------------------------------------------------------------- //

  printf("\n\n");
  TCanvas *canvas4dist[2][3][4];
  TF1* f_ave[2][3][4][2];

  for (int itype=0; itype < Ntype; itype++) {
    for (int ipart=0; ipart < Npart; ipart++) {
      for (int ivar=0; ivar < Nvar; ivar++) {

	// Make canvas for plots:
	sprintf(name, "Canvas_%s_%s_%s", typename[itype], partname[ipart], varname[ivar]);
	canvas4dist[itype][ipart][ivar] = new TCanvas(name, name, 50+200*itype+20*ivar, 50+30*ipart, 800, 600);
	// canvas4dist[itype][ipart]->Divide(4,1);  // We want to produce separate plots!
	// canvas4dist[itype][ipart]->cd(ivar+1);

	// Get the ratio before touching each distribution:
	sprintf(name, "Hist_%s_%s_%s", typename[itype], partname[ipart], varname[ivar]);
	h1[itype][ipart][ivar][2] = (TH1D*)h1[itype][ipart][ivar][0]->Clone(name);
	h1[itype][ipart][ivar][2]->Divide(h1[itype][ipart][ivar][1]);

	for (int idata=0; idata < Ndata; idata++) {
	  h1[itype][ipart][ivar][idata]->SetMinimum(0.0);

	  sprintf(name, "f_ave_%s_%s_%s_%s", typename[itype], partname[ipart], varname[ivar], dataname[idata]);
	  if (ivar < 3) f_ave[itype][ipart][ivar][idata] = new TF1(name, "[0]", minvar[ipart][ivar], maxvar[ipart][ivar]);
	  else          f_ave[itype][ipart][ivar][idata] = new TF1(name, "[0] * (1.0 + [1]*x + [2]*x*x)", minvar[ipart][ivar], maxvar[ipart][ivar]);
	  h1[itype][ipart][ivar][idata]->GetYaxis()->SetTitleOffset(1.3);
	  h1[itype][ipart][ivar][idata]->Fit(name, "RQ");
	}

	// Draw correction distributions of data and MC in same canvas:
	TPad* pad_dist = new TPad("pad_dist", "pad_dist", 0.0, 0.35, 1.0, 1.0);
	pad_dist->SetLeftMargin(1.1);
	pad_dist->SetBottomMargin(0.0);
	pad_dist->Draw();
	pad_dist->cd();

	f_ave[itype][ipart][ivar][0]->SetLineColor(kRed);
	h1[itype][ipart][ivar][0]->SetLineColor(kRed);
	h1[itype][ipart][ivar][0]->SetLineWidth(2);
	f_ave[itype][ipart][ivar][1]->SetLineColor(kBlue);
	h1[itype][ipart][ivar][1]->SetLineColor(kBlue);
	h1[itype][ipart][ivar][1]->SetLineWidth(2);

	int drawfirst = (f_ave[itype][ipart][ivar][0]->GetParameter(0) > f_ave[itype][ipart][ivar][0]->GetParameter(0)) ? 0 : 1;
	h1[itype][ipart][ivar][drawfirst]->Draw("");
	h1[itype][ipart][ivar][1-drawfirst]->Draw("same");

	// Plot ratio data/MC in the lower plot:
	canvas4dist[itype][ipart][ivar]->cd();
	TPad* pad_ratio = new TPad("pad_ratio", "pad_ratio", 0.0, 0.0, 1.0, 0.35);
	pad_ratio->SetLeftMargin(1.1);
	pad_ratio->SetTopMargin(0.00);
	pad_ratio->SetBottomMargin(0.27);
	pad_ratio->Draw("same");
	pad_ratio->cd();

	// Calculate ratio:
	h1[itype][ipart][ivar][2]->GetYaxis()->SetRangeUser(0.0, 2.0);
	h1[itype][ipart][ivar][2]->SetLineColor(kMagenta);
	h1[itype][ipart][ivar][2]->SetLineWidth(2);
	h1[itype][ipart][ivar][2]->SetTitle("");
	h1[itype][ipart][ivar][2]->Draw();

	double min = h1[itype][ipart][ivar][0]->GetXaxis()->GetBinLowEdge(1);
	double max = h1[itype][ipart][ivar][0]->GetXaxis()->GetBinUpEdge(h1[itype][ipart][ivar][0]->GetNbinsX());
	TLine* unitline = new TLine(min, 1.0, max, 1.0);
	unitline->SetLineStyle(3);
	unitline->Draw();
      }

      // Save plots:
      sprintf(name, "figures/CorrDist_%s_%s_%s", typename[itype], partname[ipart], varname[ivar]);
      if (SavePlots) canvas4dist[itype][ipart][ivar]->SaveAs(name);
    }
  }




  // Write result out nicely (and also in files):
  // -------------------------------------------------------------------------- //
  int NbinCorr[3][4] = {{0, 0, 0, 0}, {0, -10, 0, 0}, {0, -10, 0, 0}};
  double var_min[3][4] = {{-0.5,   0.0, 0.0, 0.0}, {-0.5,  630.0, 0.0, 0.0}, {-0.5,  630.0, 0.0, 0.0}};
  double var_max[3][4] = {{72.5, 720.0, 2.2, 1.0}, {-0.5, 1030.0, 2.2, 1.0}, {-0.5, 1030.0, 2.2, 1.0}};

  // In c++ array format:
  for (int idata=0; idata < Ndata; idata++) {
    // printf("\n\nFollowing results are for %s:\n", dataname[idata]);
    sprintf(name, "CorrectionFactors_cppformat_%s.txt", dataname[idata]);
    FILE* datafile = fopen(name, "w");

    for (int itype=0; itype < Ntype; itype++) {
      for (int ipart=0; ipart < Npart; ipart++) {
	for (int ivar=0; ivar < Nvar; ivar++) {
	  int Nbins = h1[itype][ipart][ivar][idata]->GetNbinsX() + NbinCorr[ipart][ivar];
	  fprintf(datafile, "  double CpHT_%s_%s_%s[%d] = {", typename[itype], partname[ipart], varname[ivar], Nbins);
	  for (int ibin=0; ibin < Nbins; ibin++) {
	    double ratio = h1[itype][ipart][ivar][idata]->GetBinContent(ibin+1) /
	      f_ave[itype][ipart][ivar][idata]->GetParameter(0);
	    if (ratio < 0.001) ratio = 1.0;
	    if (ibin == 0) fprintf(datafile, " %5.3f", ratio);
	    else           fprintf(datafile, ", %5.3f", ratio);
	  }
	  fprintf(datafile, "}; \n");
	}
      }
    }
    fclose(datafile);
  }


  // In text mode with ranges and bins:
  FILE* datafile = fopen("data_calib.txt", "w");
  for (int idata=0; idata < Ndata; idata++) {
    // printf("\n\nFollowing results are for %s:\n", dataname[idata]);
    sprintf(name, "CorrectionFactors_HRformat_%s.txt", dataname[idata]);
    FILE* datafile = fopen(name, "w");

    for (int itype=0; itype < Ntype; itype++) {
      for (int ipart=0; ipart < Npart; ipart++) {
	for (int ivar=0; ivar < Nvar; ivar++) {
	  int Nbins = h1[itype][ipart][ivar][idata]->GetNbinsX() + NbinCorr[ipart][ivar];
	  fprintf(datafile, "  %9s  %4s  %7s  %8s  %2s   %2d %5.1f %6.1f   ", typename[itype], typename2[itype],
		  gasname[0], partname[ipart], varname[ivar], Nbins, var_min[ipart][ivar], var_max[ipart][ivar]);
	  for (int ibin=0; ibin < Nbins; ibin++) {
	    double ratio = h1[itype][ipart][ivar][idata]->GetBinContent(ibin+1) /
	      f_ave[itype][ipart][ivar][idata]->GetParameter(0);
	    if (ratio < 0.001) ratio = 1.0;
	    fprintf(datafile, " %5.3f", ratio);
	  }
	  fprintf(datafile, "\n");
	}
      }
    }
    fclose(datafile);
  }

  break;












  // -------------------------------------------------------------------------- //
  // Check impact of occupancy on correction distributions (NOTE: MC only!):
  // -------------------------------------------------------------------------- //

  // Read relevant histograms/profiles:
  // ------------------------------------------------------------------------------ //
  // Define histograms
  // (first index: Zee and Zmumu, second index: B, EA, EB, third index: SL, ZR, TW, fourth index: LO, HO, HO/LO
  TH1D* h1occ[2][3][3][3];

  int Nocc = 2;
  char* occname[2] = {"LO", "HO"};

  // Read histograms and produce fHT ratios:
  for (int ipart=0; ipart < Npart; ipart++) {
    for (int ivar=0; ivar < Nvar-1; ivar++) {
      for (int iocc=0; iocc < Nocc; iocc++) {

	// Electrons:
	if (!UseMiddleHTbin) {
	  sprintf(name, "h_fHT_Xe_%s_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);
	  sprintf(name2, "hEL_fHT_Xe_%s_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);
	} else {
	  sprintf(name, "h_fHT_Xe_%s_MBX_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);     // Middle bin only!
	  sprintf(name2, "hEL_fHT_Xe_%s_MBX_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);
	}
	h1occ[0][ipart][ivar][iocc] = ((TProfile*)fileMcZee->Get(name))->ProjectionX(name2);

	// Muons:
	if (!UseMiddleHTbin) {
	  sprintf(name, "h_fHT_Xe_%s_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);
	  sprintf(name2, "hMU_fHT_Xe_%s_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);
	} else {
	  sprintf(name, "h_fHT_Xe_%s_MBX_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);     // Middle bin only!
	  sprintf(name2, "hMU_fHT_Xe_%s_MBX_%s_%s", occname[iocc], partnameMC[ipart], varname[ivar]);
	}
	h1occ[1][ipart][ivar][iocc] = ((TProfile*)fileMcZmumu->Get(name))->ProjectionX(name2);
	
	printf("  Part% 1d, var %1d, occ %1d:   EL entries: %9d     MU entries: %9d \n", ipart, ivar, iocc,
	       h1occ[0][ipart][ivar][iocc]->GetEntries(), h1occ[1][ipart][ivar][iocc]->GetEntries());
      }
    }
  }

  printf("  Successfully read occupancy histograms! \n\n");




  // Make relevant plot (comparing Low and High Occupancy (LO and HO)):
  // ------------------------------------------------------------------------------ //
  TCanvas *canvasB_el1D_LHO[2][3];      // First index: electron, muons,   Second index: part

  for (int itype=0; itype < Ntype; itype++) {
    for (int ipart=0; ipart < Npart; ipart++) {
      sprintf(name, "canvasB_el1D_LHO_%s_%s", typename[itype], partname[ipart]); 
      canvasB_el1D_LHO[itype][ipart] = new TCanvas(name, name, 50+20*itype, 150+30*ipart, 1200, 600);
      canvasB_el1D_LHO[itype][ipart]->Divide(3,1);

      // For each canvas, loop over the three variables of interest:
      for (int ivar=0; ivar < 3; ivar++) {
	canvasB_el1D_LHO[itype][ipart]->cd(ivar+1);


	// Plot distributions in upper plot:
	TPad* pad_dist = new TPad("pad_dist", "pad_dist", 0.0, 0.35, 1.0, 1.0);
	pad_dist->SetLeftMargin(1.1);
	pad_dist->SetBottomMargin(0.0);
	pad_dist->Draw();
	pad_dist->cd();

	h1occ[itype][ipart][ivar][1]->GetYaxis()->SetTitleSize(0.04);
	h1occ[itype][ipart][ivar][1]->GetYaxis()->SetTitleOffset(1.3);

	h1occ[itype][ipart][ivar][1]->SetMinimum(0.0);
	h1occ[itype][ipart][ivar][1]->SetLineColor(kRed);
	h1occ[itype][ipart][ivar][1]->SetLineWidth(2);
	h1occ[itype][ipart][ivar][1]->Draw("e");
	
	h1occ[itype][ipart][ivar][0]->SetLineColor(kBlue);
	h1occ[itype][ipart][ivar][0]->SetLineWidth(2);
	h1occ[itype][ipart][ivar][0]->Draw("e same");

	TLegend* legend_dist = new TLegend(0.15, 0.05, 0.60, 0.15);
	legend_dist->SetFillColor(0);
	legend_dist->AddEntry(h1occ[itype][ipart][ivar][0], " Occupancy < 0.2 ", "LP");
	legend_dist->AddEntry(h1occ[itype][ipart][ivar][1], " Occupancy > 0.4 ", "LP");
	legend_dist->SetLineColor(1);
	legend_dist->Draw();


	// Plot ratio in lower plot:
	canvasB_el1D_LHO[itype][ipart]->cd(ivar+1);
	TPad* pad_ratio = new TPad("pad_ratio", "pad_ratio", 0.0, 0.0, 1.0, 0.35);
	pad_ratio->SetLeftMargin(1.1);
	pad_ratio->SetTopMargin(0.00);
	pad_ratio->SetBottomMargin(0.27);
	pad_ratio->Draw("same");
	pad_ratio->cd();

	// Calculate ratio:
	sprintf(name, "h1occ_ratio_%s_%s_%s", typename[itype], partname[ipart], varname[ivar]); 
	h1occ[itype][ipart][ivar][2] = (TH1D*)h1occ[itype][ipart][ivar][1]->Clone(name);
	h1occ[itype][ipart][ivar][2]->Divide(h1occ[itype][ipart][ivar][0]);

	// Plot:
	TF1* fit_ratio = new TF1("fit_ratio", "pol0", 0.0, 1100.0);
	fit_ratio->SetLineColor(1);
	h1occ[itype][ipart][ivar][2]->Fit("fit_ratio", "Q");
	printf("  Fit %1d %1d %1d:   Chi2/Ndof = %6.1f / %2d   Prob = %6.4f    ratio = %5.3f +- %5.3f \n", itype, ipart, ivar,
	       fit_ratio->GetChisquare(), fit_ratio->GetNDF(), fit_ratio->GetProb(),
	       fit_ratio->GetParameter(0), fit_ratio->GetParError(0));

	h1occ[itype][ipart][ivar][2]->GetXaxis()->SetTitleSize(0.08);
	h1occ[itype][ipart][ivar][2]->GetYaxis()->SetTitleSize(0.06);
	h1occ[itype][ipart][ivar][2]->GetYaxis()->SetTitleOffset(0.6);
	h1occ[itype][ipart][ivar][2]->GetYaxis()->CenterTitle();
	h1occ[itype][ipart][ivar][2]->GetYaxis()->SetTitle("Ratio High/Low occupancy");
	h1occ[itype][ipart][ivar][2]->SetLineColor(kMagenta);
	h1occ[itype][ipart][ivar][2]->SetMaximum(2.5);
	h1occ[itype][ipart][ivar][2]->SetMinimum(0.0);
	h1occ[itype][ipart][ivar][2]->Draw();
      }
      
      sprintf(name, "canvasB_el1D_LHO_%s_%s.pdf", typename[itype], partname[ipart]); 
      if (SavePlots) canvasB_el1D_LHO[itype][ipart]->SaveAs(name);
    }
  }



}
