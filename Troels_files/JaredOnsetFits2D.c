// ------------------------------------------------------------------- //
/*

  ROOT macro for producing TRT onset plots used in PID from Zee and Zmumu.
  These are the first fits in 2D, including depency on both gamma and occupancy.

  Author: Troels C. Petersen (NBI)
  Email:  petersen@nbi.dk
  Date:   Started 30-04-2015

*/
// ------------------------------------------------------------------- //


double sqr(double a) {return a*a;}



// ------------------------------------------------------------------- //
double func_onset(double *x, double *par) {
// ------------------------------------------------------------------- //
  if (x[0] < 0.0) return 0.0;

  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par[4])/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par[4])/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par[1] - par[4])/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par[1]);

  if (log10(x[0])      < par[0]) return pHT_dEdx;
  else if (log10(x[0]) > par[1]) return pHT_HG;
  else                           return pHT_TR;
}




// This function is the "bare" multiplication of the onset and Anatoli
// formula!
// ------------------------------------------------------------------- //
double func_OnsetOcc(double *x, double *par) {
// ------------------------------------------------------------------- //
  if (x[0] < 0.0) return 0.0;

  // The onset function describes the gamma dependency at occupancy = 0:
  // Parameters:
  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  // -------------------------------------------------------------------
  // TR onset part (main part):
  double exp_term = exp(-(log10(x[0]) - par[4])/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par[4])/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par[1] - par[4])/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par[1]);

  double pHT_OccZero = pHT_TR;
  if      (log10(x[0]) < par[0]) pHT_OccZero = pHT_dEdx;
  else if (log10(x[0]) > par[1]) pHT_OccZero = pHT_HG;


  // The occupancy dependency is included through the Anatoli-Petersen formula:
  // --------------------------------------------------------------------------
  double occ_factor = 1.0 + par[6]*(x[1]/1.0) + par[7]*sqr(x[1]/1.0);     // Quadratic, and unit at occ=0
  double pHT = pHT_OccZero + (1.0 - pHT_OccZero + par[2]) * (pHT_OccZero*occ_factor - pHT_OccZero);
  // double pHT = pHT_OccZero + (1.0 - pHT_OccZero) * (pHT_OccZero*occ_factor - pHT_OccZero);

  // One can discuss if continuity/orderlyness is really needed, and hence if the "par[2]" in the first
  // parenthesis of the last line is needed. Fitting without it does not change anything significantly!


  return pHT;
}




// This function is the "bare" multiplication of the onset and Anatoli
// formula PLUS two extra parameters for cross terms. Only one is actually
// used in the fits (-> the point of onset changes slowly with occupancy!).
// ------------------------------------------------------------------- //
double func_OnsetOcc2(double *x, double *par) {
// ------------------------------------------------------------------- //
  if (x[0] < 0.0) return 0.0;

  // The onset function describes the gamma dependency at occupancy = 0:
  // Parameters:
  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  // -------------------------------------------------------------------
  // TR onset part (main part):
  double par4 = par[4] + par[9]*x[1];
  double exp_term = exp(-(log10(x[0]) - par4)/par[5]);
  double pHT_TR   = par[2] + par[3]/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par[2] + par[3]/(1.0 + exp_term0);
  double beta0 = par[3] / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double par1 = par[1] + par[8]*x[1];
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par[2] + par[3]/(1.0 + exp_term1);
  double beta1 = par[3] / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par1);

  double pHT_OccZero = pHT_TR;
  if      (log10(x[0]) < par[0]) pHT_OccZero = pHT_dEdx;
  else if (log10(x[0]) > par1  ) pHT_OccZero = pHT_HG;

  // The occupancy dependency is included through the Anatoli-Petersen formula:
  // --------------------------------------------------------------------------
  double occ_factor = par[10] + par[6]*(x[1]) + par[7]*sqr(x[1]);     // Quadratic, and unit at occ=0
  double pHT = pHT_OccZero * occ_factor; 
  // double pHT = pHT_OccZero + (1.0 - pHT_OccZero) * (pHT_OccZero*occ_factor - pHT_OccZero);

  // One can discuss if continuity/orderlyness is really needed, and hence if the "par[2]" in the first
  // parenthesis of the last line is needed. Fitting without it does not change anything significantly!

  return pHT;
}

double func_OnsetOcc4(double *x, double *par) {
// ------------------------------------------------------------------- //
  if (x[0] < 0.0) return 0.0;

  // The onset function describes the gamma dependency at occupancy = 0:
  // Parameters:
  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  // -------------------------------------------------------------------
  // TR onset part (main part):
  double par2 = par[2] +  par[8]*x[1] +  par[9]*sqr(x[1]);
  double par3 = par[3] + par[10]*x[1] + par[11]*sqr(x[1]);
  
  double par1 = par[1] + par[6]*x[1];
  double par4 = par[4] + par[7]*x[1];
  
  double exp_term = exp(-(log10(x[0]) - par4)/par[5]);
  double pHT_TR   = par2 + par3/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par2 + par3/(1.0 + exp_term0);
  double beta0 = par3 / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par2 + par3/(1.0 + exp_term1);
  double beta1 = par3 / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par1);

  double pHT = pHT_TR;
  if      (log10(x[0]) < par[0]) pHT = pHT_dEdx;
  else if (log10(x[0]) > par1  ) pHT = pHT_HG;

  return pHT;
}

double func_OnsetOcc5(double *x, double *par) {
// ------------------------------------------------------------------- //
  if (x[0] < 0.0) return 0.0;

  // The onset function describes the gamma dependency at occupancy = 0:
  // Parameters:
  //   0: Lower limit, 1: Upper limit, 2: Lower plateau pHT value,
  //   3: Rise in pHT, 4: Mean of onset, 5: Width of onset
  // -------------------------------------------------------------------
  // TR onset part (main part):
  double par2 = par[2];  double par3 = par[3];
  double par4 = par[4] + par[7]*x[1];
  double par1 = par[1] + par[6]*x[1];
  double exp_term = exp(-(log10(x[0]) - par4)/par[5]);
  double pHT_TR   = par2 + par3/(1.0 + exp_term);

  // dE/dx part (linear at low gamma):
  double exp_term0 = exp(-(par[0] - par4)/par[5]);
  double alpha0 = par2 + par3/(1.0 + exp_term0);
  double beta0 = par3 / sqr(1.0 + exp_term0) * exp_term0 / par[5];
  double pHT_dEdx = alpha0 + beta0*(log10(x[0]) - par[0]);

  // High-gamma part (linear at high gamma):
  double exp_term1 = exp(-(par1 - par4)/par[5]);
  double alpha1 = par2 + par3/(1.0 + exp_term1);
  double beta1 = par3 / sqr(1.0 + exp_term1) * exp_term1 / par[5];
  double pHT_HG   = alpha1 + beta1*(log10(x[0]) - par1);

  double pHT_zeroOcc = pHT_TR;
  if      (log10(x[0]) < par[0]) pHT_zeroOcc = pHT_dEdx;
  else if (log10(x[0]) > par1  ) pHT_zeroOcc = pHT_HG;

  double OccFit = par[2] + par[8]*x[1] + par[9]*sqr(x[1]);
  double pHT = pHT_zeroOcc + (1-pHT_zeroOcc)*(OccFit-par2);

  return pHT;
}

// This function is the "bare" multiplication of the onset and Anatoli




// ------------------------------------------------------------------- //

// ------------------------------------------------------------------- //
/*

  Data/Part:           Comment on parameter setting:
  -----------------------------------------------------------------
  Barrel: 
    Data2012 (Xenon):  All floating works. p0=1.0 so fixed at that (also fitting MC). p5 is 0.2091 so fix this in MC.
                       1.0000  3.3337    0.0344  0.0966  3.0307  0.2091    1.9154 -0.6995    1.1417  0.1602
    MC15 (Xenon):      All floating converges, but with p5 = 0.0221 (not correct!). Fixing p5 at 0.2091, and it works.
                       1.0000  3.2719    0.0356  0.0729  3.0528  0.2091    2.4756  0.0780    0.9226 -0.1316
    MC15 (Argon):      All floating and with p5 fixed no converging. Fixing p5 and p9 makes it work with good result.
                       1.0000  3.0366    0.0431  0.0193  2.9661  0.2091    2.2433  2.9084    1.1598 -0.1316  (p5 and p9 fixed)

  EndcapA:            
    Data2012 (Xenon):  All floating works, but setting p0=1.0 changes chi2 by 0.3, so I choose this!
                       1.0000  3.8789    0.0514  0.1605  3.3309  0.2120    0.6325 -0.3678    0.8001  0.0014
    MC15 (Xenon):      1.0000  3.3552    0.0540  0.0917  3.0720  0.1213    1.9578 -0.2688    0.5508  0.0378
    MC15 (Argon):      1.0000  2.8916    0.0443  0.0160  2.7890  0.1208    2.1245  1.7321    0.8988  0.4714

  EndcapB:           
    Data2012 (Xenon):  1.0000  3.7685    0.0565  0.2121  3.2380  0.1983   -0.5497  0.6158   -0.4307 -0.3574
    MC15 (Xenon):      1.0000  3.3557    0.0651  0.1242  3.0808  0.1221    1.0976  0.4845    0.3026  0.0184
    MC15 (Argon):      1.0000  2.8916    0.0543  0.0200  2.7890  0.1208    2.1245  1.7321    0.8988  0.4714  // Guesses as backup!

  -----------------------------------------------------------------

  These constants are for Data12 in Xenon:
  Barrel   gamma  Xenon    1.0000  3.3337  0.0344  0.0966  3.0307  0.2091  1.9154 -0.6995  1.1417  0.1602
  EndcapA  gamma  Xenon    1.0000  3.8789  0.0514  0.1605  3.3309  0.2120  0.6325 -0.3678  0.8001  0.0014
  EndcapB  gamma  Xenon    1.0000  3.7685  0.0565  0.2121  3.2380  0.1983 -0.5497  0.6158 -0.4307 -0.3574
  double pHT_Barrel_Xenon_GOpar[10]  = { 1.0000, 3.3337, 0.0344, 0.0966, 3.0307, 0.2091, 1.9154,-0.6995, 1.1417, 0.1602};
  double pHT_EndcapA_Xenon_GOpar[10] = { 1.0000, 3.8789, 0.0514, 0.1605, 3.3309, 0.2120, 0.6325,-0.3678, 0.8001, 0.0014};
  double pHT_EndcapB_Xenon_GOpar[10] = { 1.0000, 3.7685, 0.0565, 0.2121, 3.2380, 0.1983,-0.5497, 0.6158,-0.4307,-0.3574};

  These constants are for MC15 in Xenon:
  Barrel   gamma  Xenon    1.0000  3.2596  0.0354  0.0720  3.0459  0.2091  2.5133  0.0883  0.9462 -0.1138
  EndcapA  gamma  Xenon    1.0000  3.3484  0.0539  0.0906  3.0692  0.1205  1.9875 -0.2680  0.5556  0.0438
  EndcapB  gamma  Xenon    1.0000  3.3533  0.0651  0.1239  3.0792  0.1221  1.0931  0.4910  0.3015  0.0193
  double pHT_Barrel_Xenon_GOpar[10]  = { 1.0000, 3.2596, 0.0354, 0.0720, 3.0459, 0.2091, 2.5133, 0.0883, 0.9462,-0.1138};
  double pHT_EndcapA_Xenon_GOpar[10] = { 1.0000, 3.3484, 0.0539, 0.0906, 3.0692, 0.1205, 1.9875,-0.2680, 0.5556, 0.0438};
  double pHT_EndcapB_Xenon_GOpar[10] = { 1.0000, 3.3533, 0.0651, 0.1239, 3.0792, 0.1221, 1.0931, 0.4910, 0.3015, 0.0193};

  These constants are for MC15 in Argon:
  Barrel   gamma  Argon    1.0000  3.0382  0.0430  0.0194  2.9665  0.2091  2.2764  2.8751  1.1721 -0.1316
  EndcapA  gamma  Argon    1.0000  2.9091  0.0441  0.0177  2.8021  0.1374  2.1101  1.8041  1.0226  0.5081
  EndcapB  gamma  Argon    1.0000  2.8916  0.0543  0.0200  2.7890  0.1208  2.1245  1.7321  0.8988  0.4714  // Guesses as backup!
  double pHT_Barrel_Argon_GOpar[10]  = { 1.0000, 3.0382, 0.0430, 0.0194, 2.9665, 0.2091, 2.2764, 2.8751, 1.1721,-0.1316};
  double pHT_EndcapA_Argon_GOpar[10] = { 1.0000, 2.9091, 0.0441, 0.0177, 2.8021, 0.1374, 2.1101, 1.8041, 1.0226, 0.5081};
  double pHT_EndcapB_Argon_GOpar[10] = { 1.0000, 2.8916, 0.0543, 0.0200, 2.7890, 0.1208, 2.1245, 1.7321, 0.8988, 0.4714};



 */
// ------------------------------------------------------------------- //



// ------------------------------------------------------------------- //
void JaredOnsetFits2D() {
// ------------------------------------------------------------------- //
  gROOT->Reset();
  gStyle->SetOptStat("");
  gStyle->SetOptFit(0);
  gStyle->SetStatY(0.55);                
  gStyle->SetStatX(0.96);                
  gStyle->SetStatW(0.20);                
  gStyle->SetStatH(0.12);

  char name[80], nameLT[80], nameHT[80], nameLTmu[80], nameHTmu[80];
  bool SavePlots = false;
  bool isData = false;
  bool isArgon = true;
  if (isData) isArgon = false;        // No argon in 2012 data!
  int idata = (isData) ? 0 : 1;       // Index of data (0: Data, 1: MC)
  int igas = (isArgon) ? 1 : 0;       // Index of gas (0: Xenon, 1: Argon)

  TLatex *text = new TLatex();
  text->SetNDC();
  text->SetTextFont(72);
  text->SetTextColor(1);





  // ------------------------------------------------------------------------------ //
  // Monte Carlo (2015) - Reading data and preparing histograms:
  // ------------------------------------------------------------------------------ //

  double SystError = 0.005;    // Don't trust very small errors! (0.005 gives reasonable Chi2 values).
                               // Also, one should fit the whole range with about equal weight!

  int Npart = 3;
  if (isArgon) Npart = 2;             // No argon in EndcapB wheels
  int NpartMin = 0;
  char* name_part[3] = {"Barrel", "EndcapA", "EndcapB"};
  char* name_part_file[3] = {"BRL", "ECA", "ECB"};
  int Ntype = 2;
  char* name_type[2] = {"Zee", "Zmm"};
  int Ndata = 2;
  char* name_data[2] = {"Data12", "MC15"};
  int Ngas = 2;
  char* name_gas[2] = {"Xenon", "Argon"};
  
  // Define histograms:
  //   First index - type:        Zee, Zmumu
  //   Second index - part:       Barrel, EndcapA, EndcapB
  //   Third index - quantity:    nLT, nHT, fHT
  TH2D* h2_onset[2][3][3];
  

  // Read histograms (first for MC, then for data):
  // ----------------------------------------------
  if (isData) {
    TFile *fileZee   = TFile::Open("GetZmumu.DATA_Zee.Ref13.root");
    TFile *fileZmumu = TFile::Open("GetZmumu.DATA_Zmumu.Ref13.root");

    for (int ipart=0; ipart < Npart; ipart++) {

      // Xenon case (using Middle HT bit only):
      sprintf(nameLT, "HTonset/Hist2D_Zee_%s_nLT_gammaOcc", name_part[ipart]);
      sprintf(nameHT, "HTonset/Hist2D_Zee_%s_nHTMB_gammaOcc", name_part[ipart]);
      // Here the name for muons is different:
      sprintf(nameLTmu, "HTonset/Hist2D_Zmm_%s_nLT_gammaOcc", name_part[ipart]);
      sprintf(nameHTmu, "HTonset/Hist2D_Zmm_%s_nHTMB_gammaOcc", name_part[ipart]);

      h2_onset[0][ipart][0] = (TH2D*)fileZee->Get(nameLT);
      h2_onset[0][ipart][1] = (TH2D*)fileZee->Get(nameHT);
      h2_onset[1][ipart][0] = (TH2D*)fileZmumu->Get(nameLTmu);
      h2_onset[1][ipart][1] = (TH2D*)fileZmumu->Get(nameHTmu);
    }

  } else {
    TFile *fileZee   = TFile::Open("hist-Zee.root");
    TFile *fileZmumu = TFile::Open("hist-Zmumu.root");

    for (int ipart=0; ipart < Npart; ipart++) {

      // Xenon case (using Middle HT bit only):
      if (!isArgon) {
        sprintf(nameLT, "h2_nLT_Xe_%s_GF_OT", name_part_file[ipart]);
        sprintf(nameHT, "h2_nHTMB_Xe_%s_GF_OT", name_part_file[ipart]);
      } else {
        // Argon case:
        sprintf(nameLT, "h2_nLT_Ar_%s_GF_OT", name_part_file[ipart]);
        sprintf(nameHT, "h2_nHTMB_Ar_%s_GF_OT", name_part_file[ipart]);
      }

      h2_onset[0][ipart][0] = (TH2D*)fileZee->Get(nameLT);
      h2_onset[0][ipart][1] = (TH2D*)fileZee->Get(nameHT);
      h2_onset[1][ipart][0] = (TH2D*)fileZmumu->Get(nameLT);
      h2_onset[1][ipart][1] = (TH2D*)fileZmumu->Get(nameHT);
    }
  }
    
    
  for (int ipart=0; ipart < Npart; ipart++) {
    printf("  Zee:    nLT entries = %8d    nHT entries = %7d \n",
	   h2_onset[0][ipart][0]->GetEntries(), h2_onset[0][ipart][1]->GetEntries());
    printf("  Zmumu:  nLT entries = %8d    nHT entries = %7d \n",
	   h2_onset[1][ipart][0]->GetEntries(), h2_onset[1][ipart][1]->GetEntries());
  }


  // Divide histograms to get HT probabilities:
  for (int itype=0; itype < Ntype; itype++) {
    for (int ipart=0; ipart < Npart; ipart++) {
      sprintf(name, "Hist_%s_%s_fHT_gamma", name_type[itype], name_part[itype]);
      h2_onset[itype][ipart][2] = (TH2D*)h2_onset[itype][ipart][1]->Clone(name);
      // h2_onset[itype][ipart][2]->Sumw2();
      h2_onset[itype][ipart][2]->Divide(h2_onset[itype][ipart][0]);
    }
  }




  // -------------------------------------------------------------------------- //
  // Combine Zee and Zmumu and add systematic uncertainties:
  // -------------------------------------------------------------------------- //

  TH2D* h2_ocomb[3];

  for (int ipart=0; ipart < Npart; ipart++) {

    sprintf(name, "HistComb_%s_fHT_gamma", name_part[ipart]);
    h2_ocomb[ipart] = (TH2D*)h2_onset[0][ipart][2]->Clone(name);

    // Reset histogram:
    for (int ibinx=1; ibinx < h2_ocomb[ipart]->GetNbinsX()+1; ibinx++) {
      for (int ibiny=1; ibiny < h2_ocomb[ipart]->GetNbinsY()+1; ibiny++) {
	h2_ocomb[ipart]->SetBinContent(ibinx, ibiny, 0.0);
	h2_ocomb[ipart]->SetBinError(ibinx, ibiny, 0.0);
      }
    }

    // Fill histogram:
    for (int itype=0; itype < Ntype; itype++) {
      for (int ibinx=1; ibinx < h2_onset[itype][ipart][2]->GetNbinsX()+1; ibinx++) {
	for (int ibiny=1; ibiny < h2_onset[itype][ipart][2]->GetNbinsY()+1; ibiny++) {

	  // Get rid of the worst data points (too large errors!):
	  double fHT  = h2_onset[itype][ipart][2]->GetBinContent(ibinx, ibiny);
	  double efHT = h2_onset[itype][ipart][2]->GetBinError(ibinx, ibiny);
	  if (efHT < 0.05 && efHT > 0.00001) {
	    h2_ocomb[ipart]->SetBinContent(ibinx, ibiny, fHT);
	    h2_ocomb[ipart]->SetBinError(ibinx, ibiny, sqrt(sqr(efHT) + sqr(SystError)));
	  }

	}
      }
    }
  }




  // ------------------------------------------------------------------------------ //
  // Testing the Anatoli-constraint:
  // ------------------------------------------------------------------------------ //

  // Fit ranges:
  double gMin[3]   = {     50.0,      75.0,     100.0};
  double gMax[3]   = {1000000.0, 1000000.0, 1000000.0};
  double occMin[3] = {      0.0,       0.0,       0.0};
  double occMax[3] = {     0.75,      0.75,      0.75};


  // Fit the ONSET curve for low and high occupancy separately:
  // ----------------------------------------------------------

  // Relevant histograms and functions:
  TCanvas* c2_illu[3];
  TH1D* h1_lowocc[3];
  TF1* f1_lowocc[3];
  TH1D* h1_highocc[3];
  TF1* f1_highocc[3];

  for (int ipart=NpartMin; ipart < Npart; ipart++) {
    sprintf(name, "c2_illu_%s", name_part[ipart]);
    c2_illu[ipart] = new TCanvas(name, "", 50+40*ipart, 50+10*ipart, 1000, 700);
    c2_illu[ipart]->SetRightMargin(0.14);
    c2_illu[ipart]->SetLogx();

    // Produce 1D histogram for low and high occupancy:
    int NbinsYmin_low = 1;
    int NbinsYmax_low = 7;
    sprintf(name, "h1_lowocc_%s", name_part[ipart]);
    h1_lowocc[ipart] = h2_ocomb[ipart]->ProjectionX(name, 0, 0);  // Just to give it the right dimensions!

    int NbinsYmin_high = 20;
    int NbinsYmax_high = 30;
    sprintf(name, "h1_highocc_%s", name_part[ipart]);
    h1_highocc[ipart] = h2_ocomb[ipart]->ProjectionX(name, 0, 0);  // Just to give it the right dimensions!

    for (int ig=1; ig < h2_ocomb[ipart]->GetNbinsX()+1; ig++) {

      // Low occupancy:
      h1_lowocc[ipart]->SetBinContent(ig, 0.0);
      h1_lowocc[ipart]->SetBinError(ig, 0.0);
      double sum0 = 0.0;
      double sum1 = 0.0;
      for (int io=NbinsYmin_low; io < NbinsYmax_low+1; io++) {
	double pHT = h2_ocomb[ipart]->GetBinContent(ig,io);
	double epHT = h2_ocomb[ipart]->GetBinError(ig,io);
	if (pHT > 0.0 && epHT > 0.0) {
	  sum0 += 1.0 / sqr(epHT);
	  sum1 += pHT / sqr(epHT);
	}
      }
      if (sum0 > 0.0) {
	double pHTave = sum1 / sum0;
	double epHTave = sqrt(1.0 / sum0);
	if (epHTave < 0.03) {
	  h1_lowocc[ipart]->SetBinContent(ig, pHTave);
	  h1_lowocc[ipart]->SetBinError(ig, epHTave);
	}
      }

      // High occupancy:
      h1_highocc[ipart]->SetBinContent(ig, 0.0);
      h1_highocc[ipart]->SetBinError(ig, 0.0);
      double sum0 = 0.0;
      double sum1 = 0.0;
      for (int io=NbinsYmin_high; io < NbinsYmax_high+1; io++) {
	double pHT = h2_ocomb[ipart]->GetBinContent(ig,io);
	double epHT = h2_ocomb[ipart]->GetBinError(ig,io);
	if (pHT > 0.0 && epHT > 0.0) {
	  sum0 += 1.0 / sqr(epHT);
	  sum1 += pHT / sqr(epHT);
	}
      }
      if (sum0 > 0.0) {
	double pHTave = sum1 / sum0;
	double epHTave = sqrt(1.0 / sum0);
	if (epHTave < 0.03) {
	  h1_highocc[ipart]->SetBinContent(ig, pHTave);
	  h1_highocc[ipart]->SetBinError(ig, epHTave);
	}
      }
    }


    // Fit to onset in gamma:
    sprintf(name, "f1_lowocc_%s", name_part[ipart]);
    f1_lowocc[ipart] = new TF1(name, func_onset, gMin[ipart], gMax[ipart], 6);
    if (!isArgon) f1_lowocc[ipart]->SetParameters(-1.5, 3.8, 0.05, 0.14, 3.6, 0.20);
    else          f1_lowocc[ipart]->SetParameters(-1.5, 3.8-0.5*ipart, 0.05, 0.04, 3.0, 0.25);
    f1_lowocc[ipart]->FixParameter(0, -1.5);
    if (ipart == 0) f1_lowocc[ipart]->FixParameter(5, 0.19);
    if (ipart == 1 && (isData || isArgon)) f1_lowocc[ipart]->FixParameter(5, 0.20);       // May be removed with (more) time/data!
    h1_lowocc[ipart]->Fit(name, "R");

    sprintf(name, "f1_highocc_%s", name_part[ipart]);
    f1_highocc[ipart] = new TF1(name, func_onset, gMin[ipart], gMax[ipart], 6);
    if (!isArgon) f1_highocc[ipart]->SetParameters(-1.5, 3.8, 0.10, 0.16, 3.6, 0.25);
    else          f1_highocc[ipart]->SetParameters(-1.5, 3.8-0.5*ipart, 0.10, 0.05, 3.0, 0.25);
    f1_highocc[ipart]->FixParameter(0, -1.5);
    if (ipart == 0) f1_highocc[ipart]->FixParameter(5, 0.19);
    if (ipart == 1 && (isData || isArgon)) f1_highocc[ipart]->FixParameter(5, 0.20);       // May be removed with (more) time/data!
    h1_highocc[ipart]->Fit(name, "R");

    // Plot:
    h1_lowocc[ipart]->SetTitle("");
    h1_lowocc[ipart]->SetLineWidth(2);
    h1_lowocc[ipart]->GetXaxis()->SetTitle("Lorentz gamma factor - #gamma");
    h1_lowocc[ipart]->GetXaxis()->SetTitleOffset(1.0);
    h1_lowocc[ipart]->GetXaxis()->CenterTitle();

    h1_highocc[ipart]->SetTitle("");
    h1_highocc[ipart]->SetLineWidth(2);
    h1_highocc[ipart]->GetXaxis()->SetTitle("Lorentz gamma factor - #gamma");
    h1_highocc[ipart]->GetXaxis()->SetTitleOffset(1.0);
    h1_highocc[ipart]->GetXaxis()->CenterTitle();

    h1_highocc[ipart]->Draw("e");
    h1_lowocc[ipart]->Draw("e same");

    // Write text:
    sprintf(name, "ATLAS TRT %s", name_part[ipart]);
    text->SetTextSize(0.05);
    text->DrawLatex(0.15, 0.85, name);

    sprintf(name, "Systematic: %6.4f", SystError);
    text->SetTextSize(0.03);
    text->DrawLatex(0.12, 0.12, name);
    
    sprintf(name, "figures/pHTonset_LowHighOccOnset_ZeeZmumu_%s_%s.pdf", name_part[ipart], name_data[idata]);
    if (SavePlots) c2_illu[ipart]->SaveAs(name);

    printf("  %s:   %6.4f  %6.4f  %6.4f  %6.4f  %6.4f  %6.4f \n", name_part[ipart],
	   f1_lowocc[ipart]->GetParameter(0), f1_lowocc[ipart]->GetParameter(1),
	   f1_lowocc[ipart]->GetParameter(2), f1_lowocc[ipart]->GetParameter(3),
	   f1_lowocc[ipart]->GetParameter(4), f1_lowocc[ipart]->GetParameter(5));

    printf("  %s:   %6.4f  %6.4f  %6.4f  %6.4f  %6.4f  %6.4f \n", name_part[ipart],
	   f1_highocc[ipart]->GetParameter(0), f1_highocc[ipart]->GetParameter(1),
	   f1_highocc[ipart]->GetParameter(2), f1_highocc[ipart]->GetParameter(3),
	   f1_highocc[ipart]->GetParameter(4), f1_highocc[ipart]->GetParameter(5));
  }





  // Fit the occupancy dependency for non-electrons (gamma < 500,          i.e. xbin <= 17)
  //                                  and electrons (5000 < gamma < 60000, i.e. 33 <= xbin < 45):
  // --------------------------------------------------------------------------------------------
  TCanvas* c3_illu[3];
  TH1D* h1_lowgamma[3];
  TF1* f1_lowgamma[3];
  TH1D* h1_highgamma[3];
  TF1* f1_highgamma[3];
  TF1* f1_highgamma_pred[3];

  for (int ipart=NpartMin; ipart < Npart; ipart++) {
    sprintf(name, "c3_illu_%s", name_part[ipart]);
    c3_illu[ipart] = new TCanvas(name, "", 50+40*ipart, 100+10*ipart, 1000, 700);
    c3_illu[ipart]->SetRightMargin(0.14);

    // Histogram:
    int NbinsXmin_low =  1;
    int NbinsXmax_low = 17;
    sprintf(name, "h1_lowgamma_%s", name_part[ipart]);
    h1_lowgamma[ipart] = h2_ocomb[ipart]->ProjectionY(name, 0, 0);  // Just to give it the right dimensions!

    int NbinsXmin_high = 33;
    int NbinsXmax_high = 45;
    sprintf(name, "h1_highgamma_%s", name_part[ipart]);
    h1_highgamma[ipart] = h2_ocomb[ipart]->ProjectionY(name, 0, 0);  // Just to give it the right dimensions!

    // Low gamma:
    for (int io=1; io < h2_ocomb[ipart]->GetNbinsY()+1; io++) {
      h1_lowgamma[ipart]->SetBinContent(io, 0.0);
      h1_lowgamma[ipart]->SetBinError(io, 0.0);
      double sum0 = 0.0;
      double sum1 = 0.0;
      for (int ig=NbinsXmin_low; ig < NbinsXmax_low; ig++) {
        double pHT = h2_ocomb[ipart]->GetBinContent(ig,io);
        double epHT = h2_ocomb[ipart]->GetBinError(ig,io);
        if (pHT > 0.0 && epHT > 0.0) {
        // printf("  %2d %2d:   %6.4f +- %6.4f \n", ig, io, pHT, epHT);
        sum0 += 1.0 / sqr(epHT);
        sum1 += pHT / sqr(epHT);
	}
      }
      if (sum0 > 0.0) {
	double pHTave = sum1 / sum0;
	double epHTave = sqrt(1.0 / sum0);
	if (epHTave < 0.03) {
	  h1_lowgamma[ipart]->SetBinContent(io, pHTave);
	  h1_lowgamma[ipart]->SetBinError(io, epHTave);
	}
      }
    }

    // High gamma:
    for (int io=1; io < h2_ocomb[ipart]->GetNbinsY()+1; io++) {
      h1_highgamma[ipart]->SetBinContent(io, 0.0);
      h1_highgamma[ipart]->SetBinError(io, 0.0);
      double sum0 = 0.0;
      double sum1 = 0.0;
      for (int ig=NbinsXmin_high; ig < NbinsXmax_high; ig++) {
	double pHT = h2_ocomb[ipart]->GetBinContent(ig,io);
	double epHT = h2_ocomb[ipart]->GetBinError(ig,io);
	if (pHT > 0.0 && epHT > 0.0) {
	  // printf("  %2d %2d:   %6.4f +- %6.4f \n", ig, io, pHT, epHT);
	  sum0 += 1.0 / sqr(epHT);
	  sum1 += pHT / sqr(epHT);
	}
      }
      if (sum0 > 0.0) {
	double pHTave = sum1 / sum0;
	double epHTave = sqrt(1.0 / sum0);
	if (epHTave < 0.03) {
	  h1_highgamma[ipart]->SetBinContent(io, pHTave);
	  h1_highgamma[ipart]->SetBinError(io, epHTave);
	}
      }
    }



    // Fit to occupancy dependency for low gamma:
    sprintf(name, "f1_lowgamma_%s", name_part[ipart]);
    f1_lowgamma[ipart] = new TF1(name, "[0] * (1.0 + [1]*x + [2]*x*x)", 0.0, 70.0);
    f1_lowgamma[ipart]->SetLineColor(kGray+1);
    f1_lowgamma[ipart]->SetParameters(0.05, 0.1, 0.01);
    h1_lowgamma[ipart]->Fit(name, "R");

    // Fit to occupancy dependency for high gamma:
    sprintf(name, "f1_highgamma_%s", name_part[ipart]);
    f1_highgamma[ipart] = new TF1(name, "[0] * (1.0 + [1]*x + [2]*x*x)", 0.0, 70.0);
    f1_highgamma[ipart]->SetLineColor(kGray+1);
    f1_highgamma[ipart]->SetParameters(0.32, 0.01, 0.001);
    h1_highgamma[ipart]->Fit(name, "R");

    // Plot histogram for low gamma:
    h1_lowgamma[ipart]->SetTitle("");
    h1_lowgamma[ipart]->SetLineWidth(2);
    h1_lowgamma[ipart]->SetLineColor(kBlue);
    h1_lowgamma[ipart]->GetXaxis()->SetTitle("Occupancy");
    h1_lowgamma[ipart]->GetXaxis()->SetTitleOffset(1.0);
    h1_lowgamma[ipart]->GetXaxis()->CenterTitle();

    // Plot histogram for high gamma:
    h1_highgamma[ipart]->SetTitle("");
    h1_highgamma[ipart]->SetLineWidth(2);
    h1_highgamma[ipart]->SetLineColor(kRed);
    h1_highgamma[ipart]->GetXaxis()->SetTitle("Occupancy");
    h1_highgamma[ipart]->GetXaxis()->SetTitleOffset(1.0);
    h1_highgamma[ipart]->GetXaxis()->CenterTitle();
    h1_highgamma[ipart]->Draw("e");
    h1_lowgamma[ipart]->Draw("e same");

    // Plot prediction from Anatoli constraint on top:
    sprintf(name, "f1_highgamma_pred_%s", name_part[ipart]);
    f1_highgamma_pred[ipart] = new TF1(name, "[0] + (1.0 - [0]) * ( [1]*(1.0 + [2]*x + [3]*x*x) - [1])", 0.0, 70.0);
    f1_highgamma_pred[ipart]->SetLineColor(kBlack);
    f1_highgamma_pred[ipart]->FixParameter(0, f1_highgamma[ipart]->GetParameter(0));
    f1_highgamma_pred[ipart]->FixParameter(1, f1_lowgamma[ipart]->GetParameter(0));
    f1_highgamma_pred[ipart]->FixParameter(2, f1_lowgamma[ipart]->GetParameter(1));
    f1_highgamma_pred[ipart]->FixParameter(3, f1_lowgamma[ipart]->GetParameter(2));
    f1_highgamma_pred[ipart]->Draw("same");

    // Write text:
    sprintf(name, "ATLAS TRT %s", name_part[ipart]);
    text->SetTextSize(0.05);
    text->DrawLatex(0.15, 0.85, name);

    sprintf(name, "Systematic: %6.4f", SystError);
    text->SetTextSize(0.03);
    text->DrawLatex(0.12, 0.12, name);
    
    sprintf(name, "figures/pHTonset_LowHighGammaOccFit_ZeeZmumu_%s_%s.pdf", name_part[ipart], name_data[idata]);
    if (SavePlots) c3_illu[ipart]->SaveAs(name);
  }



  // -------------------------------------------------------------------------- //
  // Fit and plot 2D histograms:
  // -------------------------------------------------------------------------- //

  TCanvas* c_onset[3];
  TF2* fit_onset[3];

  for (int ipart=NpartMin; ipart < Npart; ipart++) {
    sprintf(name, "c_onset_%s", name_part[ipart]);
    c_onset[ipart] = new TCanvas(name, "", 50+40*ipart, 150+10*ipart, 1000, 700);
    c_onset[ipart]->SetRightMargin(0.14);
    c_onset[ipart]->SetLogx();

    // 2D fit to onset with gamma and increase with occupancy - and corrections to onset with occupancy:
    // -------------------------------------------------------------------------------------------------
    sprintf(name, "fit_onset_%s", name_part[ipart]);
    fit_onset[ipart] = new TF2(name, func_OnsetOcc5, gMin[ipart], gMax[ipart], occMin[ipart], occMax[ipart], 10);
    fit_onset[ipart]->SetNpx(10000);

    // Xenon:
    if (!isArgon) {
      if (isData) {
        if (ipart == 0) fit_onset[ipart]->SetParameters(1.8, 3.8, 0.04, 0.12, 3.0, 0.22, 1.0, 0.1, 0.0, 0.0);
        else            fit_onset[ipart]->SetParameters(1.8, 3.7, 0.05, 0.14, 3.0, 0.18, 1.0, 0.1, 0.0, 0.0);
      } else {
        if (ipart == 0) fit_onset[ipart]->SetParameters(1.8, 3.8, 0.04, 0.14, 3.0, 0.22, 1.0, 0.1, 0.0, 0.0);
        else            fit_onset[ipart]->SetParameters(1.8, 3.7, 0.06, 0.16, 3.0, 0.18, 1.0, 0.1, 0.0, 0.0);
      }

    // Argon:
    } else {
      if (ipart == 0) fit_onset[ipart]->SetParameters(1.8, 3.5, 0.03, 0.04, 2.85, 0.22, 1.0, 0.1, 0.0, 0.0);
      else            fit_onset[ipart]->SetParameters(1.8, 3.6, 0.05, 0.08, 3.05, 0.18, 1.0, 0.1, 0.0, 0.0);
    }

    double gamMin = 1.0;
    fit_onset[ipart]->SetParLimits(  1, gamMin, 6.0 );
    
    fit_onset[ipart]->SetParLimits(  2, 0.0, 1.0 );
    fit_onset[ipart]->SetParLimits(  3, 0.0, 1.0 );
   
    fit_onset[ipart]->SetParLimits(  6, 0.0, 1.0 );
    fit_onset[ipart]->SetParLimits(  7, 0.0, 1.0 );
    
    fit_onset[ipart]->SetParLimits(  8, 0.0, 1.0 );
    fit_onset[ipart]->SetParLimits(  9, 0.0, 1.0 );
   
    //Fix Parameters
    fit_onset[ipart]->FixParameter( 0, 1.0 );
    fit_onset[ipart]->FixParameter( 6, 0.0 );
    
    TFitResultPtr r = h2_ocomb[ipart]->Fit(name, "RS");

    printf("  %s:  %7.4f %7.4f   %7.4f %7.4f %7.4f %7.4f   %7.4f %7.4f   %7.4f %7.4f \n", name_part[ipart],
	   fit_onset[ipart]->GetParameter(0),  fit_onset[ipart]->GetParameter(1),
	   fit_onset[ipart]->GetParameter(2),  fit_onset[ipart]->GetParameter(3),
	   fit_onset[ipart]->GetParameter(4),  fit_onset[ipart]->GetParameter(5),
	   fit_onset[ipart]->GetParameter(6),  fit_onset[ipart]->GetParameter(7),
	   fit_onset[ipart]->GetParameter(8),  fit_onset[ipart]->GetParameter(9),
	   fit_onset[ipart]->GetParameter(10), fit_onset[ipart]->GetParameter(11));

    printf("  Chi2: %6.1f  Ndof: %5d  Prob: %6.4f     \n\n",
	   fit_onset[ipart]->GetChisquare(), fit_onset[ipart]->GetNDF(), fit_onset[ipart]->GetProb());


    // ---------------------------------------------------
    // Get correlation matrix:
    // ---------------------------------------------------
    int Nvars = 12;
    /*for (int i=1; i<Nvars; i++) {
      printf("  var %2d: ", i);
      for (int j=1; j<Nvars; j++) {
        double corr = r->Correlation(i,j);
        if (fabs(corr) > 0.50) printf(" %7.4f", corr);
        else                   printf("    -   ");
      }
      printf("\n");
    }*/

    // Make plot:
    h2_ocomb[ipart]->SetTitle("");
    h2_ocomb[ipart]->GetXaxis()->SetTitle("Lorentz gamma factor - #gamma");
    h2_ocomb[ipart]->GetXaxis()->SetTitleOffset(1.0);
    h2_ocomb[ipart]->GetXaxis()->CenterTitle();
    h2_ocomb[ipart]->GetYaxis()->SetTitle("Occupancy (regional)");
    h2_ocomb[ipart]->GetYaxis()->SetTitleOffset(0.9);
    h2_ocomb[ipart]->GetYaxis()->CenterTitle();
    h2_ocomb[ipart]->GetZaxis()->SetTitle("High-Threshold probability");
    h2_ocomb[ipart]->GetZaxis()->SetTitleOffset(1.1);
    h2_ocomb[ipart]->GetZaxis()->CenterTitle();
    h2_ocomb[ipart]->GetXaxis()->SetRangeUser(0.8*gMin[ipart], 1.2*gMax[ipart]);
    h2_ocomb[ipart]->GetYaxis()->SetRangeUser(occMin[ipart], occMax[ipart]+0.05);
    h2_ocomb[ipart]->Draw("colz");

    // Draw fit:
    fit_onset[ipart]->SetLineColor(2);
    fit_onset[ipart]->SetLineWidth(2);
    fit_onset[ipart]->Draw("same");

    // Write text:
    sprintf(name, "%s", name_data[idata]);
    text->SetTextSize(0.04);
    text->DrawLatex(0.15, 0.91, name);

    sprintf(name, "ATLAS TRT %s", name_part[ipart]);
    text->SetTextSize(0.05);
    text->DrawLatex(0.15, 0.85, name);

    sprintf(name, "Systematic: %6.4f", SystError);
    text->SetTextSize(0.03);
    text->DrawLatex(0.52, 0.84, name);

    sprintf(name, "Prob(Chi2=%5.1f, Ndof=%3d) = %6.4f",
	    fit_onset[ipart]->GetChisquare(), fit_onset[ipart]->GetNDF(), fit_onset[ipart]->GetProb());
    text->SetTextSize(0.03);
    text->DrawLatex(0.52, 0.87, name);

    sprintf(name, "figures/pHTonset2D_ZeeZmumuMC_%s_XeArSamples_%s.pdf", name_part[ipart], name_data[idata]);
    if (SavePlots) c_onset[ipart]->SaveAs(name);
  }



  printf("\n  These constants are for %s in %s:\n", name_data[idata], name_gas[igas]);

  // Print parameters for text/python file:
  for (int ipart=0; ipart < Npart; ipart++) {
    printf("  %6s  %7s  gamma occ_track  %5s   %2d  %7.1f %7.1f   %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f %7.4f \n",
	   name_data[idata], name_part[ipart], name_gas[igas], Nvars, gMin[ipart], gMax[ipart],
	   fit_onset[ipart]->GetParameter(0), fit_onset[ipart]->GetParameter(1),
	   fit_onset[ipart]->GetParameter(2), fit_onset[ipart]->GetParameter(3),
	   fit_onset[ipart]->GetParameter(4), fit_onset[ipart]->GetParameter(5),
	   fit_onset[ipart]->GetParameter(6), fit_onset[ipart]->GetParameter(7),
	   fit_onset[ipart]->GetParameter(8), fit_onset[ipart]->GetParameter(9),
	   fit_onset[ipart]->GetParameter(10), fit_onset[ipart]->GetParameter(11));
  }

  // Print parameters for Athena:
  for (int ipart=0; ipart < Npart; ipart++) {
    printf("  {%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f}, // %s  Prob: %6.4f \n",
	   fit_onset[ipart]->GetParameter(0), fit_onset[ipart]->GetParameter(1),
	   fit_onset[ipart]->GetParameter(2), fit_onset[ipart]->GetParameter(3),
	   fit_onset[ipart]->GetParameter(4), fit_onset[ipart]->GetParameter(5),
	   fit_onset[ipart]->GetParameter(6), fit_onset[ipart]->GetParameter(7),
	   fit_onset[ipart]->GetParameter(8), fit_onset[ipart]->GetParameter(9),
     name_part[ipart], fit_onset[ipart]->GetProb() );
  }

  // Print for this code!!!
  for (int ipart=0; ipart < Npart; ipart++) {
    printf("  {%6.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f},     // Chi2:%6.1f  Ndof:%4d  Prob: %6.4f \n",
	   fit_onset[ipart]->GetParameter(0), fit_onset[ipart]->GetParameter(1),
	   fit_onset[ipart]->GetParameter(2), fit_onset[ipart]->GetParameter(3),
	   fit_onset[ipart]->GetParameter(4), fit_onset[ipart]->GetParameter(5),
	   fit_onset[ipart]->GetChisquare(), fit_onset[ipart]->GetNDF(), fit_onset[ipart]->GetProb());
  }




  // -------------------------------------------------------------------------- //
  // Onset distributions at zero occupancy from 2D fits (only for Xenon):
  // -------------------------------------------------------------------------- //

  if (isArgon) break;

  // Result of earlier runnings (0: data/MC, 1: B/EA/EC, 2: Variable):
  double fit_onset_results[2][3][6] = {{{1.0000, 3.7955, 0.0424, 0.1373, 3.2247, 0.2400},     // Chi2: 427.0  Ndof: 563  Prob: 1.0000
					{1.0000, 4.0962, 0.0546, 0.1711, 3.3826, 0.2235},     // Chi2: 449.3  Ndof: 527  Prob: 0.9938
					{1.0000, 3.6137, 0.0556, 0.1843, 3.1762, 0.1824}},    // Chi2: 398.9  Ndof: 464  Prob: 0.9869
				       {{1.0000, 3.7630, 0.0444, 0.1089, 3.2937, 0.2544},     // Chi2:1078.5  Ndof: 915  Prob: 0.0001
					{1.0000, 3.5293, 0.0618, 0.1048, 3.1558, 0.1297},     // Chi2: 923.7  Ndof: 858  Prob: 0.0591
					{1.0000, 3.4769, 0.0695, 0.1375, 3.1296, 0.1318}}};   // Chi2:1215.0  Ndof: 869  Prob: 0.0000

  /*
    When fixing both par8 and par9 to zero:
    {1.0000, 3.8109, 0.0424, 0.1328, 3.1195, 0.2336},     // Chi2: 538.7  Ndof: 564  Prob: 0.7715
    {1.0000, 4.1032, 0.0535, 0.1669, 3.3386, 0.2223},     // Chi2: 467.8  Ndof: 528  Prob: 0.9718
    {1.0000, 3.6116, 0.0544, 0.1787, 3.1508, 0.1814},     // Chi2: 431.9  Ndof: 465  Prob: 0.8621

    When allowing par9 to float (non-zero) in data:
    {1.0000, 3.7955, 0.0424, 0.1373, 3.2247, 0.2400},     // Chi2: 427.0  Ndof: 563  Prob: 1.0000
    {1.0000, 4.0962, 0.0546, 0.1711, 3.3826, 0.2235},     // Chi2: 449.3  Ndof: 527  Prob: 0.9938
    {1.0000, 3.6137, 0.0556, 0.1843, 3.1762, 0.1824},     // Chi2: 398.9  Ndof: 464  Prob: 0.9869

    When allowing both par8 and par9 to float (non-zero) in data:
    {1.0000, 3.3544, 0.0347, 0.0976, 3.0421, 0.2109},     // Chi2: 404.8  Ndof: 562  Prob: 1.0000
    {1.0000, 3.8817, 0.0511, 0.1561, 3.3214, 0.2160},     // Chi2: 444.5  Ndof: 526  Prob: 0.9958
    {1.0000, 3.7471, 0.0594, 0.2049, 3.2390, 0.1893},     // Chi2: 392.4  Ndof: 463  Prob: 0.9924

    Despite almost 5sigma for par8, I decide that it is because of the lacking data at occ=0, and let it remain fixed at 0.
    -----------------------------------------------------------------------------------------------------------------------
    In MC with both fixed, I get:
    {1.0000, 3.7149, 0.0515, 0.1130, 3.0490, 0.2231},     // Chi2:1933.9  Ndof: 916  Prob: 0.0000
    {1.0000, 3.5886, 0.0660, 0.1187, 3.0976, 0.1330},     // Chi2:1280.9  Ndof: 859  Prob: 0.0000
    {1.0000, 3.4844, 0.0740, 0.1442, 3.0837, 0.1276},     // Chi2:1684.7  Ndof: 870  Prob: 0.0000

    With only par8 fixed, I get:
    {1.0000, 3.7630, 0.0444, 0.1089, 3.2937, 0.2544},     // Chi2:1078.5  Ndof: 915  Prob: 0.0001
    {1.0000, 3.5293, 0.0618, 0.1048, 3.1558, 0.1297},     // Chi2: 923.7  Ndof: 858  Prob: 0.0591
    {1.0000, 3.4769, 0.0695, 0.1375, 3.1296, 0.1318},     // Chi2:1215.0  Ndof: 869  Prob: 0.0000

    With none fixed, I get:
    {1.0000, 2.9399, 0.0355, 0.0443, 2.7922, 0.1202},     // Chi2: 959.0  Ndof: 914  Prob: 0.1464  OK, this fit is terribly unphysical!
    {1.0000, 3.3321, 0.0523, 0.0850, 3.0506, 0.1215},     // Chi2: 827.8  Ndof: 857  Prob: 0.7574
    {1.0000, 3.3778, 0.0644, 0.1222, 3.0791, 0.1268},     // Chi2:1181.0  Ndof: 868  Prob: 0.0000
  */


  // Update from current running:
  for (int ipart=NpartMin; ipart < Npart; ipart++) {
    for (int ivar=0; ivar<6; ivar++) {
      // printf("  The %2d %2d %2d variable was changed from %6.4f to %6.4f \n",
      //   idata, ipart, ivar, fit_onset_results[idata][ipart][ivar], fit_onset[ipart]->GetParameter(ivar));
      fit_onset_results[idata][ipart][ivar] = fit_onset[ipart]->GetParameter(ivar);
    }
  }


  // Plot visual factors:
  double ypos = -0.040;
  double mPi = 0.13957;
  double mMu = 0.10599;
  double mEl = 0.000511;
  double factor[3] = {0.5, 0.75, 1.0};    // Correction factors regarding point of change of axis!

  TCanvas* c_onset_occ0[3];
  TH1F* h1_onset_occ0[3];
  TF1* f1_onset_occ0[2][3];


  for (int ipart=NpartMin; ipart < Npart; ipart++) {
    sprintf(name, "c_onset_occ0_%s", name_part[ipart]);
    c_onset_occ0[ipart] = new TCanvas(name, "", 150, 50+40*ipart, 1000, 700);
    c_onset_occ0[ipart]->SetLogx();
    c_onset_occ0[ipart]->SetTopMargin(0.05);
    c_onset_occ0[ipart]->SetBottomMargin(0.20);
    c_onset_occ0[ipart]->SetRightMargin(0.05);
    h1_onset_occ0[ipart] = c_onset_occ0[ipart]->DrawFrame(gMin[ipart], 0.0, gMax[ipart], 0.25+0.05*ipart);
    h1_onset_occ0[ipart]->SetTitle("");
    h1_onset_occ0[ipart]->GetXaxis()->SetTitle("Lorentz gamma factor - #gamma");
    h1_onset_occ0[ipart]->GetXaxis()->SetTitleOffset(-0.6);
    h1_onset_occ0[ipart]->GetXaxis()->CenterTitle();
    h1_onset_occ0[ipart]->GetYaxis()->SetTitle("High-Threshold probability");
    h1_onset_occ0[ipart]->GetYaxis()->SetTitleOffset(1.0);
    h1_onset_occ0[ipart]->GetYaxis()->CenterTitle();

    for (int idata=0; idata < Ndata; idata++) {
      
      // New onset function for data and MC:
      sprintf(name, "f1_onset_occ0_%s_%s", name_data[idata], name_part[ipart]);
      f1_onset_occ0[idata][ipart] = new TF1(name, func_onset, gMin[ipart], gMax[ipart], 6);
      f1_onset_occ0[idata][ipart]->SetLineColor(2+2*idata);
      f1_onset_occ0[idata][ipart]->SetLineWidth(2);
      for (int ivar=0; ivar<6; ivar++) {
        f1_onset_occ0[idata][ipart]->SetParameter(ivar, fit_onset_results[idata][ipart][ivar]);
      }
      f1_onset_occ0[idata][ipart]->Draw("P same");
    }

    // Text:
    sprintf(name, "ATLAS TRT %s", name_part[ipart]);
    text->SetTextSize(0.05);
    text->DrawLatex(0.15, 0.89, name);

    // Legend:
    TLegend* leg = new TLegend(0.15, 0.75, 0.48, 0.86);
    leg->AddEntry(f1_onset_occ0[0][ipart], "  Data 2012", "PL");
    leg->AddEntry(f1_onset_occ0[1][ipart], "  MC 2015", "PL");
    leg->SetFillColor(0);
    leg->SetLineColor(0);
    leg->Draw("same");


    // Axis below plot showing muon and electron momentum (instead of gamma):
    // Muon momentum axis:
    TGaxis *axis0 = new TGaxis(gMin[ipart], ypos, 10000.0*factor[ipart], ypos, gMin[ipart]*mMu, 10000.0*factor[ipart]*mMu, 50510, "G");
    axis0->SetTitle("Muon momentum (GeV)");
    axis0->SetLabelFont(42);
    axis0->SetTitleFont(42);
    axis0->SetLabelSize(0.04);
    axis0->SetTitleSize(0.035);
    axis0->SetTitleOffset(1.25);
    axis0->CenterTitle();
    axis0->Draw();

    // Electron momentum axis:
    TGaxis *axis2 = new TGaxis(15000.0*factor[ipart], ypos, gMax[ipart], ypos, 15000.0*factor[ipart]*mEl, gMax[ipart]*mEl, 50510, "G");
    axis2->SetTitle("Electron momentum (GeV)");
    axis2->SetLabelFont(42);
    axis2->SetTitleFont(42);
    axis2->SetLabelSize(0.04);
    axis2->SetTitleSize(0.035);
    axis2->SetTitleOffset(1.25);
    axis2->CenterTitle();
    axis2->Draw();

    sprintf(name, "figures/pHTonset_DataVsMC_ZeeZmumu_%s.pdf", name_part[ipart]);
    if (SavePlots) c_onset_occ0[ipart]->SaveAs(name);
  }

}
